/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  animator export plugin - save the board (or a layer) in an animator(1) script
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "data.h"

#define ERROR "route-rnd animator error: "

#define FONT_SCALE 3

#define VIEWBOX 1
#define SV(a)  ((a))
#define SVX(x) SV(x)
#define SVY(y) SV(y)

static double translucent = 0.8;

typedef struct wr_ctx_s {
	rtrnd_t *ctx;
	FILE *f;
	char *fn;
} wr_ctx_t;

static void svg_wr_head(wr_ctx_t *wctx)
{
	g2d_box_t bb;

	rtrnd_board_bbox(&bb, wctx->ctx->board);
	fprintf(wctx->f, "<?xml version=\"1.0\"?>\n");
	fprintf(wctx->f, "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.0\"");
#ifdef VIEWBOX
	{
		double mx, my;
		mx = (bb.p2.x - bb.p1.x) * 0.15;
		my = (bb.p2.y - bb.p1.y) * 0.15;
		fprintf(wctx->f, " viewBox=\"%f %f %f %f\"", SVX(bb.p1.x-mx), SVY(bb.p1.y-my), SVX(bb.p2.x+mx), SVY(bb.p2.y+my));
	}
#endif
	fprintf(wctx->f, ">\n");
}

static void svg_wr_foot(wr_ctx_t *wctx)
{
	fprintf(wctx->f, "</svg>");
}

static void svg_wr_via(wr_ctx_t *wctx, rtrnd_via_t *via)
{
	fprintf(wctx->f, "	<circle cx='%f' cy='%f' r='%f' stroke-width='0.1' stroke='#777777' fill='#111111'/>\n",
		SVX(via->x), SVY(via->y), SV((double)via->dia/2));
}

static void svg_wr_layer(wr_ctx_t *wctx, rtrnd_layer_t *layer, int draw_vias)
{
	rtrnd_rtree_it_t it;
	rtrnd_any_obj_t *obj;
	rtp_vertex_t *v;

	fprintf(wctx->f, "<g id='%s' opacity='%f'>\n", layer->name, translucent);
	for(obj = rtrnd_rtree_all_first(&it, &layer->objs); obj != NULL; obj = rtrnd_rtree_all_next(&it)) {
		switch(obj->hdr.type) {
			case RTRND_LINE:
				fprintf(wctx->f, "	<line x1='%f' y1='%f' x2='%f' y2='%f' stroke-width='%f' stroke='%s' stroke-linecap='round'/>\n",
					SVX(obj->line.cline.p1.x), SVY(obj->line.cline.p1.y),
					SVX(obj->line.cline.p2.x), SVY(obj->line.cline.p2.y),
					SV(obj->line.thickness), layer->color);
				break;
			case RTRND_TEXT:
				fprintf(wctx->f, "	<text x='%f' y='%f' font-size='%f' stroke-width='%f' stroke='%s' fill='%s'>%s</text>\n",
					SVX(obj->text.x), SVY(obj->text.y), SV(obj->text.size*FONT_SCALE), SV(obj->text.size*FONT_SCALE/50.0),
					layer->color, layer->color, obj->text.hdr.oid);
				break;
			case RTRND_POLY:
				fprintf(wctx->f, "	<polygon points='");
				for(v = gdl_first(&obj->poly.rtpoly.lst); v != NULL; v = gdl_next(&obj->poly.rtpoly.lst, v))
					fprintf(wctx->f, " %f,%f", SVX(v->x), SVY(v->y));
				fprintf(wctx->f, "' style='fill:%s'/>\n", layer->color);
				break;
			case RTRND_ARC:
				{
					double sa = obj->arc.carc.start;
					double da = obj->arc.carc.delta;
					double ea = sa + da;
					double r = obj->arc.carc.r;
					double diff = 0, x1, y1, x2, y2;
					int large = (fabs(da) > M_PI);
					int sweep = !(da > 0.0);

					if (fabs(da) <= 0.001) { da = 0.001; diff=0.001; }
					rtrnd_arc_xy(&obj->arc, sa, &x2, &y2);
					rtrnd_arc_xy(&obj->arc, ea, &x1, &y1);

					x1 += diff; y1 += diff;

					fprintf(wctx->f, "<path d=\"M %f %f A %f %f 0 %d %d %f %f\" stroke-width=\"%f\" stroke=\"%s\" stroke-linecap=\"round\" fill=\"none\"/> <!-- %f;%f r=%f ang=%f;%f -->\n",
						SVX(x1), SVY(y1), SV(r), SV(r), large, sweep, SVX(x2), SVY(y2),
						SV(obj->arc.thickness), layer->color,
						obj->arc.carc.c.x, obj->arc.carc.c.y, obj->arc.carc.r, sa, da);
				}

			default: /* can't be on layer */
				break;
		}
	}

	if (draw_vias) {
		rtrnd_via_t *via;
		for(via = rtrnd_rtree_all_first(&it, &wctx->ctx->board->vias); via != NULL; via = rtrnd_rtree_all_next(&it))
			if (rtrnd_via_touches_layer(layer, via))
				svg_wr_via(wctx, via);
	}

	fprintf(wctx->f, "</g>\n");
}

static int svg_export(rtrnd_t *ctx, const char *basename, rtrnd_layer_t *layer, vtp0_t *annots)
{
	wr_ctx_t wctx;
	int res = 0, len = strlen(basename), n;

	wctx.fn = malloc(len + 32);
	memcpy(wctx.fn, basename, len);
	strcpy(wctx.fn + len, ".svg");
	wctx.ctx = ctx;
	wctx.f = fopen(wctx.fn, "w");
	if (wctx.f == NULL) {
		fprintf(stderr, ERROR "can't open '%s' for write\n", wctx.fn);
		goto err;
	}

	svg_wr_head(&wctx);
	if (layer == NULL) {
		if ((ctx->board != NULL) && (ctx->board->layers.used > 0)) {
			rtrnd_via_t *via;
			rtrnd_rtree_it_t it;

			for(n = ctx->board->layers.used-1; n >=0 ; n--)
				svg_wr_layer(&wctx, ctx->board->layers.array[n], 0);
			fprintf(wctx.f, "<g id='vias' opacity='%f'>\n", translucent);
			for(via = rtrnd_rtree_all_first(&it, &ctx->board->vias); via != NULL; via = rtrnd_rtree_all_next(&it))
				svg_wr_via(&wctx, via);
			fprintf(wctx.f, "</g>");
		}
		else
			fprintf(wctx.f, "<!-- (empty board - no layers!) -->\n");
	}
	else
		svg_wr_layer(&wctx, layer, 1);

	if (annots != NULL)
		for(n = 0; n < annots->used; n++)
			svg_wr_layer(&wctx, annots->array[n], 0);
	svg_wr_foot(&wctx);

	err:;
	free(wctx.fn);
	if (wctx.f != NULL)
		fclose(wctx.f);
	return res;
}


static const rtrnd_export_t exp_svg = {
	"svg",
	svg_export
};

void export_svg_init(void)
{
	vtp0_append(&rtrnd_all_export, (void *)&exp_svg);
}
