extern void io_tedax_init(void);
extern void export_animator_init(void);
extern void export_svg_init(void);
extern void rt_horver_init(void);
extern void rt_topo_init(void);
/*extern void rt_hace_init(void);*/

void rtrnd_buildin_init(void)
{
	io_tedax_init();
	export_animator_init();
	export_svg_init();
	rt_horver_init();
	rt_topo_init();
/*	rt_hace_init();*/
}
