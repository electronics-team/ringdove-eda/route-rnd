/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2020,2021 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include <genvector/vtd0.h>
#include <libcdtr/debug.h>

#include "geo.h"
#include "gengeo2d/cline.h"

void rt_topo_trbs_cdt_init(rtrnd_t *ctx, rtrnd_layer_t *ly, trbs_t *trbs)
{
	rtrnd_any_obj_t *obj;
	rtrnd_via_t *via;
	rtrnd_rtree_it_t it;
	point_t *p1, *p2;
	cdt_t *cdt = &trbs->cdt;
	rtp_vertex_t *v;
	edge_t *e;

	memset(trbs, 0, sizeof(trbs_t));

	cdt_init(cdt, ctx->board->hdr.bbox.x1, ctx->board->hdr.bbox.y1, ctx->board->hdr.bbox.x2, ctx->board->hdr.bbox.y2);

	for(via = rtrnd_rtree_all_first(&it, &ctx->board->vias); via != NULL; via = rtrnd_rtree_all_next(&it)) {
		p1 = trbs_insert_point(trbs, via->x, via->y, (rtrnd_any_obj_t *)via->hdr.net, NULL);
	}

	for(obj = rtrnd_rtree_all_first(&it, &ly->objs); obj != NULL; obj = rtrnd_rtree_all_next(&it)) {
		switch(obj->hdr.type) {
			case RTRND_LINE:
				p1 = trbs_insert_point(trbs, obj->line.cline.p1.x, obj->line.cline.p1.y, obj, NULL);
				p2 = trbs_insert_point(trbs, obj->line.cline.p2.x, obj->line.cline.p2.y, obj, NULL);
				if (p1 != p2) {
					e = cdt_insert_constrained_edge(cdt, p1, p2);
					e->data = obj;
				}
				break;
			case RTRND_POLY:
				v = gdl_last(&obj->poly.rtpoly.lst);
				p2 = trbs_insert_point(trbs, v->x, v->y, obj, NULL);
				for(v = gdl_first(&obj->poly.rtpoly.lst); v != NULL; v = gdl_next(&obj->poly.rtpoly.lst, v)) {
					p1 = trbs_insert_point(trbs, v->x, v->y, obj, NULL);
					e = cdt_insert_constrained_edge(cdt, p1, p2);
					e->data = obj;
					p2 = p1;
				}
				break;
			default:
#warning handle all other types
				;
		}
	}
}

void rt_topo_trbs_cdt_draw(rtrnd_t *ctx, rtrnd_layer_t *ly_out, cdt_t *cdt)
{
	VTEDGE_FOREACH(edge, &cdt->edges)
		rtrnd_line_new(ly_out, NULL, NULL,
			edge->endp[0]->pos.x, edge->endp[0]->pos.y, edge->endp[1]->pos.x, edge->endp[1]->pos.y, \
			edge->is_constrained ? 0.1 : 0.01, 0);
	VTEDGE_FOREACH_END();
	VTPOINT_FOREACH(pt, &cdt->points)
		char tmp[64];
		sprintf(tmp, "P%ld", ((trbs_point_t *)pt->data)->uid);
		rtrnd_text_new(ly_out, pt->pos.x, pt->pos.y, tmp, 0.5);
	VTPOINT_FOREACH_END();
}

static int trbs_cdt_insert_perp(trbs_t *trbs, edge_t *e, triangle_t *t, double *xo, double *yo, point_t **opp)
{
	int n;
	point_t *p = NULL;
	g2d_offs_t o;
	g2d_vect_t pt, npt;
	g2d_cline_t line;
	rtrnd_any_obj_t *eo = e->data;
	trbs_point_t *tp;

	if (t == NULL)
		return 0;

	/* find the opposite point */
	for(n = 0; n < 3; n++) {
		if ((t->p[n] != e->endp[0]) && (t->p[n] != e->endp[1])) {
			p = t->p[n];
			break;
		}
	}
	assert(p != NULL);

	*opp = p;
	tp = p->data;

	/* do not insert bottleneck within a polygon (all three points of the triangle are the same obj) */
	if ((tp->net != NULL) && (tp->net == ((trbs_point_t *)e->endp[0]->data)->net) && (tp->net == ((trbs_point_t *)e->endp[1]->data)->net))
		return 0;
	printf("  ins obj: %p %p %p\n", tp->net, ((trbs_point_t *)e->endp[0]->data)->net, ((trbs_point_t *)e->endp[1]->data)->net);

	/* if the edge and the point both belong to the same net, they are probably
	   a terminal - avoid adding a lot of internal edges */
	if ((eo != NULL) && (tp->obj != NULL) && (eo->hdr.net != NULL) && (tp->obj->hdr.net == eo->hdr.net))
		return 0;

	printf("  opposite: P%ld (%f;%f)\n", ((trbs_point_t *)p->data)->uid, p->pos.x, p->pos.y);

	pt.x = p->pos.x; pt.y = p->pos.y;
	line.p1.x = e->endp[0]->pos.x; line.p1.y = e->endp[0]->pos.y;
	line.p2.x = e->endp[1]->pos.x; line.p2.y = e->endp[1]->pos.y;
	o = g2d_project_pt_cline(pt, &line);
	if ((o < 0) || (o > 1))
		return 0;
	npt = g2d_cline_offs(&line, o);

	*xo = npt.x; *yo = npt.y;
	return 1;
}

/* Return how much edge capacity gets shorter at endpoint p due to the object
   and clearance there */
static void trbs_edge_cap_endp(trbs_t *trbs, edge_t *e, point_t *p, double *copper, double *clearance)
{
	double cop = 0, clr = 0;
	trbs_point_t *tp = p->data;


	if ((tp == NULL) || (tp->obj == NULL)) {
		*copper = *clearance = 0;
		return;
	}

#warning TODO: iterate over a list of endoint objects, e.g. a terminal is a via+poly
	switch(tp->obj->hdr.type) {
		case RTRND_VIA:
			cop = tp->obj->via.dia / 2.0;
			clr = tp->obj->via.clearance;
			break;

#warning TODO: these for arc and line ignore mid-line non-perpendicular case where  we lose more room
		case RTRND_LINE:
			cop = tp->obj->line.thickness / 2.0;
			clr = tp->obj->line.clearance;
			break;

		case RTRND_ARC:
			cop = tp->obj->arc.thickness / 2.0;
			clr = tp->obj->arc.clearance;
			break;

		case RTRND_POLY:
#warning TODO: calculate poly vs. line crossing
printf("  poly\n");
/*
				c2.p1.x = n->x; c1.p2.y = n->y;
				c2.p2.x = nn->x; c2.p2.y = nn->y;
				vp = gdl_last(&obj->poly.rtpoly.lst);
				for(v = gdl_first(&obj->poly.rtpoly.lst); v != NULL; v = gdl_next(&obj->poly.rtpoly.lst, v)) {
					c1.p1.x = v->x; c1.p1.y = v->y;
					c1.p2.x = vp->x; c1.p2.y = vp->y;
					if (g2d_isc_cline_cline(&c1, &c2))
*/
			break;

		default:
			break;
	}

	*copper = TRBS_MAX(cop, 0);
	*clearance = TRBS_MAX(clr, 0);
}

static void trbs_init_cdt_edges(trbs_t *trbs, rtrnd_layer_t *ly_dbg)
{
	long n, n2;
	vtd0_t vc = {0};
	vtp0_t vp = {0};

	/* name all points already exist */
	VTPOINT_FOREACH(pt, &trbs->cdt.points)
		trbs_insert_point(trbs, pt->pos.x, pt->pos.y, NULL, NULL);
	VTPOINT_FOREACH_END();

#if 1
	printf("*** NARROW:\n");
	/* insert a new edge at the narrowest point between a constrained edge
	   and the opposite point; having such an edge means control over how
	   many lines may pass through there. The first loop only collects points
	   to draw constrained edges to, so the scenario is not changing... */
	for(n = 0; n < trbs->cdt.edges.used; n++) {
		double x, y;
		point_t *opp;
		edge_t *e = trbs->cdt.edges.array[n];

		if (!e->is_constrained)
			continue;
		printf(" edge P%ld..P%ld\n", ((trbs_point_t *)e->endp[0]->data)->uid, ((trbs_point_t *)e->endp[1]->data)->uid);

		if (trbs_cdt_insert_perp(trbs, e, e->adj_t[0], &x, &y, &opp) > 0) {
			vtp0_append(&vp, opp);
			vtp0_append(&vp, e);
			vtd0_append(&vc, x);
			vtd0_append(&vc, y);
		}
		if (trbs_cdt_insert_perp(trbs, e, e->adj_t[1], &x, &y, &opp) > 0) {
			vtp0_append(&vp, opp);
			vtp0_append(&vp, e);
			vtd0_append(&vc, x);
			vtd0_append(&vc, y);
		}
	}

	/* ... this second loop is creating the new points and actual constrained
	   edges as mapped in the first loop above */
	for(n = n2 = 0; n < vp.used; n += 2, n2 += 2) {
		double x = vc.array[n2], y = vc.array[n2+1];
		point_t *opp = vp.array[n], *newp;
		edge_t *e = vp.array[n+1];
		rtrnd_any_obj_t *p_obj;

		rtrnd_line_new(ly_dbg, NULL, NULL, x, y, opp->pos.x, opp->pos.y, 0.2, 0);
		if (((trbs_point_t *)e->endp[0]->data)->obj == ((trbs_point_t *)e->endp[1]->data)->obj)
			p_obj = ((trbs_point_t *)e->endp[0]->data)->obj;
		else
			p_obj = NULL;

		newp = trbs_insert_point(trbs, x, y, p_obj, NULL);
		if (newp != NULL) {
			printf("  insert:   P%ld (%f;%f) obj=%p\n", ((trbs_point_t *)newp->data)->uid, newp->pos.x, newp->pos.y, p_obj);
			{
				FILE *f = fopen("insert.cdt", "w");
				cdt_fdump(f, &trbs->cdt);
				fprintf(f, "ins_point p1 %.16f %.16f\n", opp->pos.x, opp->pos.y);
				fprintf(f, "ins_point p2 %.16f %.16f\n", newp->pos.x, newp->pos.y);
				fprintf(f, "ins_cedge p1 p2\n");
				fclose(f);
			}
			cdt_insert_constrained_edge(&trbs->cdt, opp, newp);
		}
		else
			printf("  FAILED insert:   (%f;%f)\n",  x, y);
	}
#endif

	/* set up edges and edge sentinels */
	for(n = 0; n < trbs->cdt.edges.used; n++) {
		edge_t *e = trbs->cdt.edges.array[n];
		trbs_cross_t *cr0;
		trbs_edge_t *tedge;
		rtrnd_any_obj_t *eobj = e->data;

		e->data = uall_stacks_alloc(&trbs->tedges);
		memset(e->data, 0, sizeof(trbs_edge_t));

		cr0 = trbs_cross_new(trbs, NULL, e, NULL);
		tedge = e->data;
		gdl_append(&tedge->crosses, cr0, link_edge);

		tedge->obj = eobj;
		if (eobj != NULL)
			tedge->blocked = 1;


		/* base capacity of an edge is its length */
		tedge->cap = trbs_dist(e->endp[0]->pos.x, e->endp[0]->pos.y, e->endp[1]->pos.x, e->endp[1]->pos.y);
printf(" EDGECAP1: %f\n", tedge->cap);
		trbs_edge_cap_endp(trbs, e, e->endp[0], &tedge->pdia_cop[0], &tedge->pdia_clr[0]);
		trbs_edge_cap_endp(trbs, e, e->endp[1], &tedge->pdia_cop[1], &tedge->pdia_clr[1]);
		tedge->cap -= (tedge->pdia_cop[0]/2 + tedge->pdia_cop[1]/2 + tedge->pdia_clr[0] + tedge->pdia_clr[1]);
		tedge->cap_orig = tedge->cap;
printf(" EDGECAP2: %f\n", tedge->cap);

		trbs_cross_new(trbs, NULL, e, cr0);
	}
}
