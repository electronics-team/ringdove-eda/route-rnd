/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020 and 2021)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */


#include <libusearch/a_star_api.h>

static int trace_astar = 0;

static void trbs_cr_coord(trbs_t *trbs, trbs_cross_t *cr, double *x, double *y)
{
	if (cr == trbs->target) {
		*x = trbs->target->pos.x;
		*y = trbs->target->pos.y;
	}
	else {
		/* temporary: assume edge center */
		*x = (cr->edge->endp[0]->pos.x + cr->edge->endp[1]->pos.x)/2;
		*y = (cr->edge->endp[0]->pos.y + cr->edge->endp[1]->pos.y)/2;
	}
}

static long trbs_ast_heuristic(usrch_a_star_t *ctx, void *node)
{
	trbs_t *trbs = ctx->user_data;
	trbs_cross_t *cr = node;
	double x, y;

	if (cr == trbs->target)
		return 0;

	trbs_cr_coord(trbs, cr, &x, &y);
	return trbs_dist_heur(x, y, trbs->target->pos.x, trbs->target->pos.y);
}

static long trbs_ast_cost(usrch_a_star_t *ctx, void *from, void *to)
{
	trbs_t *trbs = ctx->user_data;
	trbs_cross_t *cr1 = from, *cr2 = to;
	double x1, y1, x2, y2;

	trbs_cr_coord(trbs, cr1, &x1, &y1);
	trbs_cr_coord(trbs, cr2, &x2, &y2);

	return trbs_dist_heur(x1, y1, x2, y2);
}

static vtp0_t trbs_ast_nv;
static void *trbs_ast_neighbor_pre(usrch_a_star_t *ctx, void *curr)
{
	double thick;
	trbs_cross_t *cr = curr;
	trbs_t *trbs = ctx->user_data;

#warning TODO: per net wire thickness
	thick = rt_topo_cfg.wire_thick + rt_topo_cfg.wire_clr/2;

	if (trace_astar) printf("A* from: cr%ld\n", cr->uid);

	trbs_ast_nv.used = 0;
	if (cr->edge->adj_t[0] != NULL)
		trbs_next_edge_from_edge(trbs, &trbs_ast_nv, cr->edge->adj_t[0], cr, thick);
	if (cr->edge->adj_t[1] != NULL)
		trbs_next_edge_from_edge(trbs, &trbs_ast_nv, cr->edge->adj_t[1], cr, thick);
	return &trbs_ast_nv;
}

static void *trbs_ast_neighbor(usrch_a_star_t *ctx, void *curr, void *nctx)
{
	vtp0_t *v = nctx;

	if (v->used == 0)
		return NULL;

	v->used--;
	if (trace_astar) printf(" a* cr%ld\n", ((trbs_cross_t *)(v->array[v->used]))->uid);
	return v->array[v->used];
}

static void trbs_ast_set_mark(usrch_a_star_t *ctx, void *node, usrch_a_star_node_t *mark)
{
	trbs_t *trbs = ctx->user_data;
	trbs_cross_t *cr = node;


	if (cr == trbs->target) 
		trbs->target_mark = mark;
	else
		cr->mark = mark;
}

static usrch_a_star_node_t *trbs_ast_get_mark(usrch_a_star_t *ctx, void *node)
{
	trbs_t *trbs = ctx->user_data;
	trbs_cross_t *cr = node;

	if (cr == trbs->target) 
		return trbs->target_mark;

	return cr->mark;
}

static int rt_topo_trbs_route_net(trbs_t *trbs, trbs_2net_t *ttn)
{
	vtp0_t next = {0};
	usrch_res_t sres;
	usrch_a_star_t ast = {0};
	usrch_a_star_node_t *it;
	trbs_cross_t *cr, *ncr;
	long ncrs;

	trbs->collision = NULL;

	ast.heuristic = trbs_ast_heuristic;
	ast.cost = trbs_ast_cost;
	ast.neighbor_pre = trbs_ast_neighbor_pre;
	ast.neighbor = trbs_ast_neighbor;
	ast.set_mark = trbs_ast_set_mark;
	ast.get_mark = trbs_ast_get_mark;
	ast.user_data = trbs;

	trbs->routing_net = ttn->net;
	trbs->target = ast.target = ttn->end;

	printf(" route_net: from P%ld to P%ld\n",
		((trbs_point_t *)ttn->start->data)->uid,
		((trbs_point_t *)ttn->end->data)->uid);

	trbs_next_edge_from_point(trbs, &next, ttn->start);
	if (next.used == 0) {
		printf("  Can't start: no visible edges from start point\n");
		usrch_a_star_uninit(&ast);
		trbs->routing_net = NULL;
		return -1;
	}

	sres = usrch_a_star_start_arr(&ast, next.array, next.used);
	if (sres != USRCH_RES_SUCCESS) {
		printf("  a* start fail 1\n");
		usrch_a_star_uninit(&ast);
		trbs->routing_net = NULL;
		return -1;
	}

	while((sres = usrch_a_star_iter(&ast)) == USRCH_RES_CONTINUE) ;

	if (sres != USRCH_RES_FOUND) {
		printf("  a* search fail 2\n");
		usrch_a_star_uninit(&ast);
		trbs->routing_net = NULL;
		return -1;
	}

	ttn->routed = 1;

	/* reconstruct the path */
	ncrs = 0;
	cr = usrch_a_star_path_first(&ast, &it); /* ignore the first pointer, which is the target point */
	for(cr = usrch_a_star_path_next(&ast, &it); cr != NULL; cr = usrch_a_star_path_next(&ast, &it)) {
		trbs_edge_t *te;
		ncr = trbs_cross_new(trbs, ttn, cr->edge, cr);
		gdl_insert(&ttn->route, ncr, link_route);
		printf("  above %ld: %ld\n", cr->uid, ncr->uid);

		te = cr->edge->data;
#warning TODO: per net wire thickness
		te->cap -= rt_topo_cfg.wire_thick + rt_topo_cfg.wire_clr/2;

		ncrs++;
	}

	/* corner case: there's no cr because the route is connecting two points
	   directly on an edge; the edge needs to be marked as blocked but the
	   block needs to be removed on a clean */
	if (ncrs == 0) {
		edge_t *edge = NULL;
		long n;
		for(n = 0; n < trbs->cdt.edges.used; n++) {
			edge_t *e = trbs->cdt.edges.array[n];
			if ((e->endp[0] == ttn->start) && (e->endp[1] == ttn->end)) {
				edge = e;
				break;
			}
			if ((e->endp[1] == ttn->start) && (e->endp[0] == ttn->end)) {
				edge = e;
				break;
			}
		}
		if (edge != NULL) {
			trbs_edge_t *te = edge->data;
			te->blocked = 1;
			te->pp_blk = 1;
		}
	}

	usrch_a_star_uninit(&ast);
	trbs->routing_net = NULL;

	return 0;
}

