/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020 and 2021)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

/* LAA step III: optimize the assignment graphs; result is a layer assignment.
   Included from laa.c */

typedef struct {
	rtrnd_t *ctx;
	rtrnd_rtree_t *r2net;

	/* cache */
	vtp0_t hull;
	vtve0_t all_pts;
} laa3_t;

static void laa_br_init_2net(rtrnd_t *ctx, rt_topo_laa_2net_t *tn)
{
	int n, lid, start = -1, end = -1;

	/* allocate current assignment */
	tn->asg = malloc(tn->br.used);
	memset(tn->asg, -1, tn->br.used);

	/* simple case: there's a common layer in first and last node, use that
	   layer all the way long */
	for(lid = 0; lid < ctx->board->layers.used; lid++) {
		long lb = (1 << lid);
		if ((tn->br.array[0].pt_layers & lb) && (tn->br.array[tn->br.used-1].pt_layers & lb)) {
			memset(tn->asg, lid, tn->br.used);
			return;
		}
	}

	/* no common layer - figure start and end layer */
	for(lid = 0; (lid < ctx->board->layers.used) && ((start < 0) || (end < 0)); lid++) {
		long lb = (1 << lid);
		if ((start < 0) && (tn->br.array[0].pt_layers & lb))
			start = lid;
		if ((end < 0) && (tn->br.array[tn->br.used-1].pt_layers & lb))
			end = lid;
	}

	assert(start >= 0);
	assert(end >= 0);

	/* go on start layer until the first via then on dest layer */
	tn->asg[0] = start;
	for(n = 1; n < tn->br.used; n++)
		tn->asg[n] = end;
}

static void laa_br_init_net(rtrnd_t *ctx, rtrnd_net_t *net)
{
	rt_topo_2nets_t *tns = NETDATA_LAA(net);
	rt_topo_laa_2net_t *tn;

	for(tn = tns->head; tn != NULL; tn = tn->next)
		laa_br_init_2net(ctx, tn);
}

static void laa_br_get_ends_on(const rt_topo_laa_2net_t *tn, int bridx, g2d_cline_t *dst, int indices[2], int lid)
{
	int start, end;

	if (lid < 0)
		lid = tn->asg[bridx]; 	/* default lid is the assigned lid */

	/* find the first branch that has an outgoing line on a different layer, backward */
	for(start = bridx; start >= 0; start--)
		if (tn->asg[start] != lid)
			break;
	start++; /* and then return to the one that first used our layer */

	/* find the first branch that has an outgoing line on a different layer, forward */
	for(end = bridx; end < tn->br.used; end++)
		if (tn->asg[end] != lid)
			break;
	if (end == tn->br.used)
		end--;

	dst->p1.x = tn->br.array[start].x; dst->p1.y = tn->br.array[start].y;
	dst->p2.x = tn->br.array[end].x;   dst->p2.y = tn->br.array[end].y;
	indices[0] = start;
	indices[1] = end;
}

#include "laa3_detour.c"

typedef struct {
	vtve0_t pts;          /* of rtrnd_2branch_t * */
	unsigned make_pts:1;  /* whether to fill in pts */
	const struct rt_topo_laa_2net_s *tn_detouring; /* the 2net hat is making the detour (shorter detour) */
} detour_t;

static double laa_cross_cost(laa3_t *laa3, const rt_topo_laa_2net_t *tn, const rtrnd_crossing_t *cr, detour_t *det)
{
	const rt_topo_laa_2net_t *tn1 = tn, *tn2 = cr->cn;
	int bridx1 = cr->bridx, bridx2 = cr->cn_bridx;
	double res;
	vtve0_t *pts;

	assert(tn1 != NULL);
	assert(tn2 != NULL);
	assert(bridx1 >= 0);
	assert(bridx2 >= 0);

	/* if they are not on the same layer, there is no crossing so the cost is 0 */
	if (tn1->asg[bridx1] != tn2->asg[bridx2])
		return 0;

	if (det == NULL)
		return 1;

	pts = det->make_pts ? &det->pts : &laa3->all_pts;
	pts->used = 0;
	det->tn_detouring = NULL;

	res = HUGE_VAL;
	if (laa_detour_len(laa3, tn1, bridx1, tn2, bridx2, pts, &res, det->make_pts))
		det->tn_detouring = tn1;
	if (laa_detour_len(laa3, tn2, bridx2, tn1, bridx1, pts, &res, det->make_pts))
		det->tn_detouring = tn2;

	/* corner case: since vias size is 0, a detour can end up being 0 long:
	
	        B
	        |
	   A----B----A
	
	In this case net A will go around the bottom B pin, but that will still be
	0 detour because that pin is in-line with A pins. In this case we need to
	return a non-zero value so that the crossing has a cost */
	if (res == 0)
		res = 0.0001;

	return res;
}

static void laa_br_print_net(laa3_t *laa3, rtrnd_net_t *net)
{
	rt_topo_2nets_t *tns = NETDATA_LAA(net);
	rt_topo_laa_2net_t *tn;
	printf(" net %s\n", net->hdr.oid);
	for(tn = tns->head; tn != NULL; tn = tn->next) {
		int lid, n;
		printf("  2net between %.2f;%2f and %.2f;%2f\n", tn->x1, tn->y1, tn->x2, tn->y2);
		for(lid = 0; lid < laa3->ctx->board->layers.used; lid++) {
			long lb = 1 << lid;
			printf("    ");
			for(n = 0; n < tn->br.used; n++) {
				int ahere = (tn->br.array[n].pt_layers & lb);
				int athere = (n < tn->br.used-1) ? (tn->br.array[n+1].pt_layers & lb) : 0;
				const char *conn;
				if (n >= tn->br.used-1)
					conn = "";
				else if ((tn->asg != NULL) && (tn->asg[n] == lid)) {
					if (tn->br.array[n].cridx >= 0) {
						int cridx = tn->br.array[n].cridx;
						if (laa_cross_cost(laa3, tn, &tn->cross.array[cridx], NULL) == 0)
							conn = "=x="; /* no real crossing */
						else
							conn = "=X="; /* they are both on the same layer -> real crossing */
					}
					else
						conn = "===";
				}
				else if (ahere && athere)
					conn = "---";
				else
					conn = "...";
				printf("%c%s",
					ahere ? '+' : '.',
					conn
					);
			}
			printf("\n");
		}
	}
}

static void laa_br_draw_net(laa3_t *laa3, rtrnd_net_t *net, vtp0_t *annot)
{
	rt_topo_2nets_t *tns = NETDATA_LAA(net);
	rt_topo_laa_2net_t *tn;
	detour_t det = {0};

	det.make_pts = 1;


	for(tn = tns->head; tn != NULL; tn = tn->next) {
		int n, lid, ci;

		for(n = 0; n < tn->br.used-1; n++) {
			double x1, y1, x2, y2;
			lid = tn->asg[n];
			if (lid < 0) continue;
			x1 = tn->br.array[n].x; y1 = tn->br.array[n].y;
			x2 = tn->br.array[n+1].x; y2 = tn->br.array[n+1].y;

			rtrnd_line_new(annot->array[lid], NULL, NULL, x1, y1, x2, y2, 0.2, 0);
		}
		/* draw detours in thin */
		for(ci = 0; ci < tn->cross.used; ci++) {
			double cc = laa_cross_cost(laa3, tn, &tn->cross.array[ci], &det);
			if (cc > 0) {
				long l;
				printf("CROSSING at %.2f;%.2f: detour len=%ld %.2f\n",
					tn->cross.array[ci].x, tn->cross.array[ci].y,
					det.pts.used, cc);
				for(l = 1; l < det.pts.used; l++)
					rtrnd_line_new(annot->array[lid], NULL, NULL,
						det.pts.array[l-1].x, det.pts.array[l-1].y,
						det.pts.array[l].x,   det.pts.array[l].y,
						0.05, 0);

				rtrnd_line_new(annot->array[lid], NULL, NULL,
					det.pts.array[l-1].x, det.pts.array[l-1].y,
					det.pts.array[0].x,   det.pts.array[0].y,
					0.05, 0);
			}
		}
	}
}

#include "laa3_solve.c"
#include "laa3_render.c"

/* Main entry point, deals with the whole board */
static int laa_solve(rtrnd_t *ctx, vtp0_t *annot, rtrnd_rtree_t *r2net, rt_topo_laa2rbs_t *dst)
{
	htsp_entry_t *e;
	int n, i, q, res = 0;
	laa3_t laa3 = {0};

	laa3.ctx = ctx;
	laa3.r2net = r2net;

	/* set up annotation layers */
	for(n = 0; n < ctx->board->layers.used; n++) {
		rtrnd_layer_t *src = ctx->board->layers.array[n];
		rtrnd_layer_t *dst = calloc(sizeof(rtrnd_layer_t), 1);

		rtrnd_layer_init(dst, src->name);
		strcpy(dst->color, src->color);
		vtp0_append(annot, dst);
		/* modify the color */
		for(i = 0; i < 3; i++) {
			char *c = &dst->color[1+i*2];
			if ((*c >= '0') && (*c <= '9'))
				q = *c - '0';
			else if ((*c >= 'a') && (*c <= 'f'))
				q = *c - 'a' + 10;
			else if ((*c >= 'A') && (*c <= 'F'))
				q = *c - 'A' + 10;
			q += 3;
			if (q > 15)
				q = 15;
			if (q < 10)
				*c = q + '0';
			else
				*c = q - 10 + 'a';
		}
	}

	/* create the initial allocation */
	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e))
		laa_br_init_net(ctx, e->value);

	if (laa_test_hook[3] != NULL) laa_test_hook[3](ctx, annot, r2net);

	res |= laa3_solve(&laa3);

	if (res == 0)
		laa3_render(&laa3, dst);

	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		laa_br_print_net(&laa3, e->value);
		laa_br_draw_net(&laa3, e->value, annot);
	}

	vtp0_uninit(&laa3.hull);
	vtve0_uninit(&laa3.all_pts);
	return res;
}
