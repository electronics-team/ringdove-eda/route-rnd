/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

/* LAA manual tester */

#include <stdlib.h>
#include <ctype.h>
#include <genht/htsi.h>
#include <genht/hash.h>
#include "geo.h"
#include <gengeo2d/cline.h>
#include "route-rnd.h"
#include "compat_misc.h"
#include "../laa.h"

FILE *testf;
htsi_t test_hooks;

static void test_load(const char *testfn)
{
	char line[128], *s, *end;

	if (testfn == NULL) {
		fprintf(stderr, "Error: missing argument -t testfn\n");
		exit(1);
	}

	testf = fopen(testfn, "r");
	if (testf == NULL) {
		fprintf(stderr, "Error: can't open %s for read\n", testfn);
		exit(1);
	}

	htsi_init(&test_hooks, strhash, strkeyeq);
	while((s = fgets(line, sizeof(line), testf)) != NULL) {
		while(isspace(*s)) s++;
		if ((strncmp(s, "hook", 4) == 0) && isspace(s[4])) {
			char *hook = s+5;
			while(isspace(*hook)) hook++;
			end = strpbrk(hook, "\r\n");
			if (end != NULL) *end = '\0';
			htsi_insert(&test_hooks, rnd_strdup(hook), ftell(testf));
		}
	}
}

#define test_run(hookname, stmt) \
do { \
	long __l__ = htsi_get(&test_hooks, hookname); \
	char line[128], *s, *cmd, *arg; \
	if (__l__ <= 0) break; \
	fseek(testf, __l__, SEEK_SET); \
	while((s = fgets(line, sizeof(line), testf)) != NULL) { \
		while(isspace(*s)) s++; \
		if ((strncmp(s, "hook", 4) == 0) && isspace(s[4])) break; \
		cmd = s; \
		arg = strpbrk(cmd, " \t\r\n"); \
		if (arg != NULL) { \
			char *end; \
			*arg = '\0'; \
			arg++; \
			while(isspace(*arg)) arg++; \
			end = strpbrk(arg, "\r\n"); \
			if (end != NULL) *end = '\0'; \
		} \
		stmt; \
	} \
} while(0)

static void cmd_brlayer(rtrnd_t *ctx, rtrnd_rtree_t *r2net, double x, double y, int ly)
{
	rt_topo_laa_2net_t *tn;
	rtrnd_rtree_it_t it;
	g2d_vect_t pt;
	rtrnd_rtree_box_t bbox;

	bbox.x1 = x - 0.001; bbox.y1 = y - 0.001;
	bbox.x2 = x + 0.001; bbox.y2 = y + 0.001;
	pt.x = x; pt.y = y;

printf("size=%ld\n", r2net->size);

	for(tn = rtrnd_rtree_first(&it, r2net, &bbox); tn != NULL; tn = rtrnd_rtree_next(&it)) {
		g2d_cline_t cl;
		g2d_offs_t offs;
printf("tn\n");
		cl.p1.x = tn->x1; cl.p1.y = tn->y1;
		cl.p2.x = tn->x2; cl.p2.y = tn->y2;
		offs = g2d_offs_cline_pt(&cl, pt);
		if ((offs >= 0) && (offs <= 1)) {
			int n;
			for(n = 0; n < tn->br.used-1; n++) {
				rtrnd_2branch_t *br = &tn->br.array[n], *brn = &tn->br.array[n+1];
				if ((offs >= br->offs) && (offs < brn->offs)) {
					br->edge_ly_fixed = ly;
					tn->asg[n] = ly;
					return;
				}
			}
		}
	}
	fprintf(stderr, "Test error: can't find a two-net at %f;%f\n", x, y);
	exit(1);
}

static void manual_asg(rtrnd_t *ctx, vtp0_t *annot, rtrnd_rtree_t *r2net)
{
	printf("----------------MANUAL ASG\n");

	test_run("laa3", {
		if (strcmp(cmd, "brlayer") == 0) {
			double x;
			double y;
			int ly;
			if (sscanf(arg, "%lf %lf %d\n", &x, &y, &ly) != 3) {
				fprintf(stderr, "Error: invalid brlayer args '%s'\n", arg);
				exit(1);
			}
			cmd_brlayer(ctx, r2net, x, y, ly);
		}
	});
}


int main(int argc, char *argv[])
{
	int n;
	const char *testfn = NULL;

	for(n = 1; n < argc; n++) {
		char *cmd = argv[n], *arg = argv[n+1];
		if (*cmd == '\0') continue; /* ignore empty args, useful for the tester */
		if (*cmd == '-') {
			while(*cmd == '-') cmd++;
			switch(*cmd) {
				case 't': testfn = arg; argv[n] = argv[n+1] = ""; n++; break;
			}
		}
	}

	test_load(testfn);

	printf("Tester!\n");
	laa_test_hook[3] = manual_asg;
	return route_rnd_main(argc, argv);
}
