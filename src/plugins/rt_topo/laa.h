#ifndef RT_TOPO_LAA_H
#define RT_TOPO_LAA_H
#include "data.h"
#include "rtree.h"
#include "vt2br.h"
#include "vtcr.h"
#include "rt_topo.h"

typedef struct rt_topo_laa_2net_s rt_topo_laa_2net_t;

struct rt_topo_laa_2net_s {
	rtrnd_rtree_box_t bbox;
	vt2br_t br;        /* via site allocation, branches for the assignment graph */
	char *asg;         /* current layer assignment over br */
	rtrnd_net_t *net;
	rt_topo_laa_2net_t *next;
	vtcr_t cross;      /* crossings with other 2nets */

	/* cached costs */
	double cost_detour;
	double cost_vialayer;            /* total via and layer usage cost */

	/* temporary fields for building the rtree */
	double x1, y1, x2, y2;   /* endpoint coords */
	long p1ly, p2ly, ely;    /* endpoint and edge layers */
	unsigned p1fx:1, p2fx:1, efx:1; /* whether endpoints and edge is fixed */
};

/* Layer assignment for a net, stored in NETDATA_LAA */
typedef struct rt_topo_2nets_s {
	rtrnd_net_t *net;
	rt_topo_laa_2net_t *head; /* singly linked list of 2nets */
} rt_topo_2nets_t;

/* Calculat the laa-2nets (steiner tree) and potential vias and final
   layer assigment of nets */
int rt_topo_laa(rtrnd_t *ctx, rt_topo_laa2rbs_t *dst);


typedef void (*laa_test_hook_t)(rtrnd_t *ctx, vtp0_t *annot, rtrnd_rtree_t *r2net);

extern laa_test_hook_t laa_test_hook[5];

#endif
