/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2020,2021 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020 and 2021)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#define TRBS_MIN(a, b) (((a) < (b)) ? (a) : (b))
#define TRBS_MAX(a, b) (((a) > (b)) ? (a) : (b))

static long trbs_dist_heur(double x1, double y1, double x2, double y2)
{
	double dx = x2 - x1, dy = y2 - y1, d = dx*dx+dy*dy;

	return d == 0 ? d : floor(sqrt(d) * 1000);
}

static long trbs_dist(double x1, double y1, double x2, double y2)
{
	double dx = x2 - x1, dy = y2 - y1, d = dx*dx+dy*dy;

	return d == 0 ? d : floor(sqrt(d));
}

point_t *trbs_insert_point(trbs_t *trbs, coord_t x, coord_t y, rtrnd_any_obj_t *obj, rtrnd_net_t *net)
{
	point_t *pt = cdt_insert_point(&trbs->cdt, x, y);
	trbs_point_t *udt;

	if (pt == NULL)
		return NULL;

	udt = pt->data;
	if ((udt != NULL) && (udt->obj != obj) && (obj != NULL)) {
#warning TODO need a list of objects
		fprintf(stderr, "trbs_insert_point(): object collision\n");
	}

	if (udt == NULL) {
		udt = pt->data = calloc(sizeof(trbs_point_t), 1);
		udt->uid = trbs->pt_uid++;
	}

	if (obj != NULL) {
		udt->obj = obj;
		if (net == NULL)
			net = obj->hdr.net;
	}

	if (net != NULL)
		udt->net = net;

	return pt;
}

