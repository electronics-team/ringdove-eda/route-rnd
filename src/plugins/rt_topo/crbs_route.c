/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2020,2021 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020 and 2021)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "crbs.h"
#define HT(x) htad_ ## x
#include <genht/ht.c>
#undef HT

#include <libusearch/a_star_api.h>
#include <libgrbs/route.h>

static int crbs_grbs_draw(crbs_t *crbs, char *fn);

static int crbs_trace_ast = 1;
static int hop_svg = 0; /* whether to write 3..4 svgs per next_hop call */

static int cnt_grbs = 0;

static long crbs_ast_heuristic(usrch_a_star_t *ctx, void *node)
{
	crbs_t *crbs = ctx->user_data;
	grbs_detached_addr_t *from = node;
	double tx, ty;
	long dist;

	tx = from->pt->x + cos(from->new_sa) * from->new_r;
	ty = from->pt->y + sin(from->new_sa) * from->new_r;

	dist = crbs_dist_heur(crbs->target->pt->x, crbs->target->pt->y, tx, ty);

 /* prefer concave over convex assuming the puller will get it shorter */
	if ((from->type & 0x0F) == ADDR_ARC_VCONCAVE)
		dist = dist * 2 / 3;

	return dist;
}

static long crbs_ast_cost(usrch_a_star_t *ctx, void *from_, void *to_)
{
	crbs_t *crbs = ctx->user_data;
	grbs_detached_addr_t *from = from_, *to = to_;
	crbs_2net_t *ctn;
	crbs_point_t *cto;
	double ex, ey, tx, ty, tune = 1;
	long cost;
	int from_dir = 0, to_dir = 0;

	switch(from->type & 0x0F) {
		case ADDR_ARC_VCONCAVE:
			tune = 2.0/3.0; /* prefer concave over convex assuming the puller will get it shorter */
		case ADDR_ARC_CONVEX:
			ex = from->pt->x + cos(from->new_sa + from->new_da) * from->new_r;
			ey = from->pt->y + sin(from->new_sa + from->new_da) * from->new_r;
			cost = floor(from->new_r * from->new_da * DIST_HEUR_MULT * tune);
			from_dir = from->new_adir;
			break;

		case ADDR_POINT:
			ex = from->pt->x;
			ey = from->pt->y;
			cost = 0;
			break;
	}

	switch(to->type & 0x0F) {
		case ADDR_ARC_VCONCAVE:
			tune = tune * (2.0/3.0);
		case ADDR_ARC_CONVEX:
			to_dir = to->new_adir;
		case ADDR_POINT:
			break;
	}

	tx = to->pt->x + cos(to->new_sa) * to->new_r;
	ty = to->pt->y + sin(to->new_sa) * to->new_r;
	cost += crbs_dist_heur(ex, ey, tx, ty);

	/* in an arc-arc path prefer not switching between cw and ccw by increasing
	   the path cost by 1% on a switch - trying to keep the situation convex;
	   this is supposed to help in situations when there are two equally cheap
	   paths without blocking switches where they are reasonable */
	if ((from_dir != 0) && (to_dir != 0) && (from_dir != to_dir))
		cost *= 1.01;


	/* If we are about to go around a point of our own net, give a penalty for
	   anything but vconcave; this helps avoiding a dogleg around square pin
	   corner before aiming at the center */
	cto = to->pt->user_data;
	ctn = crbs->routing_tn->user_data;
	if (cto->net == ctn->net) {
		switch(to->type & 0x0F) {
			case ADDR_ARC_CONVEX:
				cost *= 3;
				break;
			case ADDR_ARC_VCONCAVE:
			case ADDR_POINT:
				break;
		}
	}

	return cost;
}

typedef struct {
	vtp0_t pts;    /* of (crbs_point_t *) */
	int pt_idx;    /* current idx on pts */
	int grbs_idx;  /* how we try to go around that point */
} crbs_ast_t;

static void *crbs_ast_neighbor_pre(usrch_a_star_t *ctx, void *curr)
{
	crbs_t *crbs = ctx->user_data;
	static crbs_ast_t a;
	int target_idx = -1;
	grbs_detached_addr_t *from_addr = curr;
	crbs_point_t *from = from_addr->pt->user_data;

	a.pt_idx = 0;
	a.grbs_idx = 0;
	a.pts.used = 0;

	/* collect next hops on a list */
	EDGELIST_FOREACH(e, (from->cpt->adj_edges)) {
		point_t *to_pt;
		crbs_point_t *pt;

		if (e->endp[0] == from->cpt) to_pt = e->endp[1];
		else if (e->endp[1] == from->cpt) to_pt = e->endp[0];
		else continue; /* shouldn't happen: neither end of the edge is 'from' */

		pt = to_pt->data;

		if (pt != NULL) {
			vtp0_append(&a.pts, pt);
			if (pt->gpt == crbs->target->pt)
				target_idx = a.pts.used - 1;
		}
	}
	EDGELIST_FOREACH_END();

	/* optimization: move target to the front of the list so that we evaluate
	   a finishing path before anything else */
	if (target_idx > 0) {
		void *tmp;

		tmp = a.pts.array[target_idx];
		a.pts.array[target_idx] = a.pts.array[0];
		a.pts.array[0] = tmp;
	}

	return &a;
}

/*
static void print_det(grbs_detached_addr_t *det)
{
	printf("type=%d ", det->type);
	if (det->arc != NULL) printf("arc=%ld ", det->arc->uid);
	if (det->pt != NULL) printf("P%ld ", det->pt->uid);
	printf("u=%ld new=(r%f %f %f|%d)", det->user_long, det->new_r, det->new_sa, det->new_da, det->new_adir);
}


static void print_detkey(grbs_addr_key_t key)
{
	printf("ang=%d o%d conc=%d %s P%d", key.ang, key.orbit, key.is_concave, key.is_ccw ? "ccw" : "cw", key.pt_uid);
}
*/

/* toa must be an arc */
static int crbs_is_spiral(usrch_a_star_t *ctx, grbs_detached_addr_t *froma, grbs_addr_t *toa)
{
	usrch_a_star_node_t *it;
	grbs_detached_addr_t *da;

#warning TODO: we should probably allow real spirals and deny only on the same orbit (some maze setup may have legit spiral)

	for(da = usrch_a_star_path_first_from(ctx, froma, &it); da != NULL; da = usrch_a_star_path_next(ctx, &it)) {
		if (da->pt == toa->obj.arc->parent_pt)
			return 1;
	}

	return 0;
}

static int hop_cnt = 0;

/* Attempt to create a grbs path segment from curr to pt:adir. If fails, return
   NULL. If succeeds, remove the path segment from the grbs context, add a
   detached address of it to the hash and return it. */
static grbs_detached_addr_t *crbs_next_hop(usrch_a_star_t *ctx, grbs_detached_addr_t *curr, crbs_point_t *pt, grbs_arc_dir_t adir)
{
	crbs_t *crbs = ctx->user_data;
	grbs_addr_t *froma, *toa;
	grbs_2net_t *gtn = crbs->routing_tn;
	grbs_detached_addr_t *res = NULL, dtmp[3];
	char tmp[128];

	hop_cnt++;
	if (crbs_trace_ast) printf(" {%d} ", hop_cnt);

	if (hop_svg) {
		sprintf(tmp, "3_GRBS_routed_%d_h%d_a.svg", cnt_grbs, hop_cnt);
		crbs_grbs_draw(crbs, tmp);
	}

 /* Extra paranoia (expensive): previous hop_next ended in detach or cleanup so
    it should have removed all ->new */
	assert(grbs_count_new(&crbs->grbs) == 0);

	froma = grbs_reattach_addr(&crbs->grbs, curr);
	if (froma == NULL) {
		printf(" Neigh: can't reattach curr\n");
		return NULL;
	}

	if (hop_svg) {
		sprintf(tmp, "3_GRBS_routed_%d_h%d_b.svg", cnt_grbs, hop_cnt);
		crbs_grbs_draw(crbs, tmp);
	}

	if ((froma->type != ADDR_POINT) && (froma->obj.arc != NULL)) { assert(froma->obj.arc->link_point.parent != NULL); }

	toa = grbs_path_next(&crbs->grbs, gtn, froma, pt->gpt, adir);
	if (toa != NULL) {
		grbs_addr_key_t key;
		htad_entry_t *e;

		if (hop_svg) {
			sprintf(tmp, "3_GRBS_routed_%d_h%d_c.svg", cnt_grbs, hop_cnt);
			crbs_grbs_draw(crbs, tmp);
		}

		if ((froma->type & 0x0f) == ADDR_ARC_CONVEX) {
			if (fabs(froma->obj.arc->new_da) > M_PI) {
/*				printf("   INVALID arc (too long): %f at P%ld\n", froma->obj.arc->new_da, froma->obj.arc->parent_pt == NULL ? -1 : froma->obj.arc->parent_pt->uid);*/
				grbs_path_cleanup_addr(&crbs->grbs, toa);
				grbs_path_cleanup_addr(&crbs->grbs, froma);
				return NULL;
			}
		}

		if ((adir == GRBS_ADIR_CONVEX_CCW) || (adir == GRBS_ADIR_CONVEX_CW)) {
			if (crbs_is_spiral(ctx, curr, toa)) {
				printf("   INVALID spiral: P%ld\n", froma->obj.arc->parent_pt == NULL ? -1 : froma->obj.arc->parent_pt->uid);
				grbs_path_cleanup_addr(&crbs->grbs, toa);
				grbs_path_cleanup_addr(&crbs->grbs, froma);
				return NULL;
			}
		}


		grbs_detach_addr(&crbs->grbs, dtmp, toa);
		key = grbs_det_addr_to_key(dtmp);
		e = htad_getentry(&crbs->addrs, key);
		if (e == NULL) {
			htad_value_t val = {0};

			val.valid = 1;
#warning TODO: allocate this with ualloc stacks
			res = val.det = calloc(sizeof(grbs_detached_addr_t), 3);
			memcpy(val.det, &dtmp, sizeof(dtmp));
			val.det->user_long = adir;
/*			printf("\nDET SET: key: "); print_detkey(key); printf(" | det: ");print_det(res); printf("\n");*/
			htad_set(&crbs->addrs, key, val);
		}
		else {
			res = e->value.det;
/*			printf("\nDET S2T: key: "); print_detkey(grbs_det_addr_to_key(res)); printf(" | det: ");print_det(res); printf("\n");*/
		}

		grbs_path_cleanup_addr(&crbs->grbs, toa);
	}

	if (hop_svg) {
		sprintf(tmp, "3_GRBS_routed_%d_h%d_d.svg", cnt_grbs, hop_cnt);
		crbs_grbs_draw(crbs, tmp);
	}

	grbs_path_cleanup_addr(&crbs->grbs, froma);
	return res;
}

static void *crbs_ast_neighbor(usrch_a_star_t *ctx, void *curr_, void *nctx)
{
	crbs_t *crbs = ctx->user_data;
	crbs_ast_t *a = nctx;
	grbs_detached_addr_t *next, *curr = curr_;
	const char *from_type;



	for(;;) {
		crbs_point_t *pt;

		if (a->grbs_idx >= 3) {
			a->grbs_idx = 0;
			a->pt_idx++;
		}

		if (a->pt_idx >= a->pts.used)
			break;

		pt = a->pts.array[a->pt_idx];

		switch(curr->type & 0x0F) {
			case ADDR_ARC_CONVEX:   from_type = (curr->new_adir > 0) ? "convex cw" : "convex ccw"; break;
			case ADDR_ARC_VCONCAVE: from_type = "vconcave"; break;
			case ADDR_POINT:        from_type = "incident"; break;
			default:                from_type = "UNKNOWN"; break;
		}

/*printf("Neigh: %d / %d (dir: %d)\n", a->pt_idx, a->pts.used, a->grbs_idx);*/

		if (pt == crbs->target->pt->user_data) { /* quick lane for reaching the target */
			if (crbs_trace_ast) printf(" try from %s P%ld to TARGET P%ld", from_type, curr->pt->uid, pt->gpt->uid);
			next = crbs_next_hop(ctx, curr, pt, GRBS_ADIR_INC);
			if (next != NULL) {
				a->pt_idx = a->pts.used; /* don't consider detours from the current node if target node can be reached directly */
				if (crbs_trace_ast) printf(" -> ok\n");
				return crbs->target;
			}
			if (crbs_trace_ast) printf(" -> failed\n");
			a->pt_idx++;
			continue;
		}


		while(a->grbs_idx < 3) {
			if (crbs_trace_ast) {
				if ((curr->type & 0x0F) == ADDR_ARC_VCONCAVE)
					printf(" try from %s P%ld (real %.2f %.2f) to P%ld", from_type, curr->pt->uid, curr[1].pt->x, curr[1].pt->y, pt->gpt->uid);
				else
					printf(" try from %s P%ld to P%ld", from_type, curr->pt->uid, pt->gpt->uid);
			}
			if (crbs->disable_vconcave) {
				if (a->grbs_idx < 1)
					a->grbs_idx = 1;
			}

			/* prefer concave over convex; order matters when reaching target: first
			   valid solution will stop the search */
			switch(a->grbs_idx) {
				case 0: if (crbs_trace_ast) printf(" vconcave"); next = crbs_next_hop(ctx, curr, pt, GRBS_ADIR_VCONCAVE); break;
				case 1: if (crbs_trace_ast) printf(" convex ccw"); next = crbs_next_hop(ctx, curr, pt, GRBS_ADIR_CONVEX_CCW); break;
				case 2: if (crbs_trace_ast) printf(" convex cw"); next = crbs_next_hop(ctx, curr, pt, GRBS_ADIR_CONVEX_CW); break;
			}

			a->grbs_idx++;
			if (next != NULL) {
				if (crbs_trace_ast) printf(" -> ok\n");
				return next;
			}
			if (crbs_trace_ast) printf(" -> failed\n");
		}
	}

	return NULL;
}

static void crbs_ast_set_mark(usrch_a_star_t *ctx, void *node, usrch_a_star_node_t *mark)
{
	crbs_t *crbs = ctx->user_data;
	grbs_detached_addr_t *det = node;
	htad_entry_t *e;

	if (det->pt == crbs->first->pt) {
		crbs->first_mark = mark;
		return;
	}

	if (det->pt == crbs->target->pt) {
		crbs->target_mark = mark;
		return;
	}

/*	{
		static dcnt=0;
		dcnt++;
		printf("DET get: key: "); print_detkey(grbs_det_addr_to_key(det)); printf(" | det: ");print_det(det); printf(" {%d}\n", dcnt);
	}*/
	e = htad_getentry(&crbs->addrs, grbs_det_addr_to_key(det));
	if (e == NULL) { /* this may happen if we land on a different orbit since the last try (e.g. in legit spiral case) */
		htad_value_t val = {0};

		val.valid = 1;
#warning TODO: allocate this with ualloc stacks
		val.det = calloc(sizeof(grbs_detached_addr_t), 3);
		memcpy(val.det, det, sizeof(grbs_detached_addr_t)*3);
/*			printf("\nDET SET: key: "); print_detkey(key); printf(" | det: ");print_det(res); printf("\n");*/
		htad_set(&crbs->addrs, grbs_det_addr_to_key(val.det), val);
		e = htad_getentry(&crbs->addrs, grbs_det_addr_to_key(det));
	}
/*	printf("DET res: e=%p\n", e);*/
	fflush(stdout);
	assert(e != NULL);
	e->value.mark = mark;
}

static usrch_a_star_node_t *crbs_ast_get_mark(usrch_a_star_t *ctx, void *node)
{
	crbs_t *crbs = ctx->user_data;
	grbs_detached_addr_t *det = node;
	htad_entry_t *e;

	if (det->pt == crbs->first->pt)
		return crbs->first_mark;
	if (det->pt == crbs->target->pt)
		return crbs->target_mark;

	e = htad_getentry(&crbs->addrs, grbs_det_addr_to_key(det));
	if (e == NULL)
		return NULL;
	return e->value.mark;
}

static grbs_detached_addr_t *addr_new_point(crbs_t *crbs, point_t *pt, grbs_detached_addr_t *addr)
{
	crbs_point_t *cp = pt->data;

	memset(addr, 0, sizeof(grbs_detached_addr_t));
	addr->type = ADDR_POINT;
	addr->pt = cp->gpt;
	addr->user_long = GRBS_ADIR_INC;

	/* this address is unique to every two-net, but will look the same when
	   started from the same point so do not save it in the hash */
	return addr;
}

static int rt_topo_crbs_route_net(crbs_t *crbs, crbs_2net_t *ctn)
{
	usrch_res_t sres;
	usrch_a_star_t ast = {0};
	usrch_a_star_node_t *it;
	grbs_detached_addr_t addr_tmp[2];
	grbs_detached_addr_t *first, *da, *last;
	grbs_addr_t *na, *pa, *firsta, *lasta;
	crbs_2net_t *tn;
	int res = -1;


	/* reset net collisions */
	for(tn = gdl_first(&crbs->twonets); tn != NULL; tn = gdl_next(&crbs->twonets, tn))
		tn->coll = 0;

	/* reset address hash */
	{
		htad_entry_t *e;

/*		printf("DET: --- reset ---\n");*/
#warning TODO: when det allocation is libualloc staks based, these two lines can be replaced with a stack reset:
		for(e = htad_first(&crbs->addrs); e != NULL; e = htad_next(&crbs->addrs, e))
			free(e->value.det);
		htad_clear(&crbs->addrs);
	}

	crbs->first = first = addr_new_point(crbs, ctn->cstart, &addr_tmp[0]);
	crbs->target = ast.target = last = addr_new_point(crbs, ctn->cend, &addr_tmp[1]);

#warning TODO: per net wire thickness
	if (ctn->gtn == NULL) {
		crbs->routing_tn = ctn->gtn = grbs_2net_new(&crbs->grbs, rt_topo_cfg.wire_thick, rt_topo_cfg.wire_clr);
		crbs->routing_tn->user_data = ctn;
	}
	else
		crbs->routing_tn = ctn->gtn;

	/* check if it is possible to start and end the twonet as incident line in
	   start and end point */
	if ((grbs_is_target_pt_routable(&crbs->grbs, crbs->routing_tn, first->pt) == 0) || (grbs_is_target_pt_routable(&crbs->grbs, crbs->routing_tn, last->pt) == 0)) {
		printf("  Can not route from start or to end because of incident line end cap collision\n");
		res = -1;
		goto err;
	}


	ast.heuristic = crbs_ast_heuristic;
	ast.cost = crbs_ast_cost;
	ast.neighbor_pre = crbs_ast_neighbor_pre;
	ast.neighbor = crbs_ast_neighbor;
	ast.set_mark = crbs_ast_set_mark;
	ast.get_mark = crbs_ast_get_mark;
	ast.user_data = crbs;

	crbs->max_hop_cnt = hop_cnt + crbs->grbs.all_points.length * 1000.0 * rt_topo_cfg.max_hop_mult;

	cnt_grbs++;

	printf(" route_net: from P%ld to P%ld (step 3_GRBS_routed_%d) max_hop %ld\n",
		((crbs_point_t *)(ctn->cstart->data))->gpt->uid,
		((crbs_point_t *)(ctn->cend->data))->gpt->uid,
		cnt_grbs, crbs->max_hop_cnt);

	sres = usrch_a_star_start(&ast, first);
	if (sres != USRCH_RES_SUCCESS) {
		printf("  a* start fail 1\n");
		goto err;
	}

	while((sres = usrch_a_star_iter(&ast)) == USRCH_RES_CONTINUE) {
		if (hop_cnt > crbs->max_hop_cnt) {
			printf("  a* search fail: giving up on too many hops\n");
			goto err;
		}
	}

	if (sres != USRCH_RES_FOUND) {
		printf("  a* search fail 2\n");
		goto err;
	}

	/*** reconstruct the path ***/

	/* abuse da->user_data for building a linked list for reversing */
	first = NULL;
	for(da = usrch_a_star_path_first(&ast, &it); da != NULL; da = usrch_a_star_path_next(&ast, &it)) {
		da->user_data = first;
		first = da;
	}

	/* build from start to end, in the original order */
	firsta = NULL;
	lasta = pa = grbs_addr_new(&crbs->grbs, ADDR_POINT, first->pt);
	lasta->user_data = NULL;
	printf("GT 2net_new %s__%ld %f %f  from P%ld", ctn->tn->net->hdr.oid, ctn->tn->uid, rt_topo_cfg.wire_thick, rt_topo_cfg.wire_clr, first->pt->uid);
	for(da = first->user_data; da != NULL; da = da->user_data) {
		hop_cnt++;
		switch(da->user_long) {
			case GRBS_ADIR_INC:         printf(" to P%ld", da->pt->uid); break;
			case GRBS_ADIR_CONVEX_CCW:  printf(" ccw P%ld", da->pt->uid); break;
			case GRBS_ADIR_CONVEX_CW:   printf(" cw P%ld", da->pt->uid); break;
			case GRBS_ADIR_VCONCAVE:    printf(" vconcave P%ld", da->pt->uid); break;
		}
		na = grbs_path_next(&crbs->grbs, crbs->routing_tn, pa, da->pt, da->user_long);
		if (na != NULL) {
			na->user_data = pa; /* ->user_data is next; build the list again in reverse order for realize */
			pa = na;
			firsta = na;
		}
		else {
			if (hop_svg) {
				char tmp[256];
				sprintf(tmp, "3_GRBS_routfail_%d_h%d_b.svg", cnt_grbs, hop_cnt);
				crbs_grbs_draw(crbs, tmp);
			}
			printf("\nInternal ERROR, TODO: failed to pre-realize the path (%s at P%ld) {%d}\n", ctn->tn->net->hdr.oid, da->pt->uid, hop_cnt);
			fflush(stdout);
			abort();
		}
	}

	printf("\n");
	fflush(stdout);

	pa = NULL;
	for(na = firsta; na != NULL; pa = na, na = na->user_data) {
		if (grbs_path_validate(&crbs->grbs, crbs->routing_tn, pa, na, na->user_data) != 0) {
			abort();
		}
		if (pa != NULL)
			rt_topo_crbs_cdt_inc_edge(crbs, pa, na);
	}

	for(na = firsta; na != NULL; na = na->user_data) {
		if ((na->type & 0x0F) != ADDR_ARC_VCONCAVE) {
			grbs_arc_t *arc = grbs_path_realize(&crbs->grbs, crbs->routing_tn, na, 0);
			if (arc != NULL)
				arc->user_data = ctn;
			else
				abort(); /* we have validated the path so this can not fail */
		}
	}

	{
		char tmp[128];

		sprintf(tmp, "3_GRBS_routed_%d.svg", cnt_grbs);
		crbs_grbs_draw(crbs, tmp);
	}

	res = 0;
err:

	usrch_a_star_uninit(&ast);
	crbs->routing_tn = NULL;
	return res;
}

/* Returns 1 if ripped up something */
static int on_fail_ripup(crbs_t *crbs, crbs_2net_t *tn)
{
	int best_r = crbs->max_ripup;
	crbs_2net_t *ctn, *best = NULL;
	char tmp[128];

	printf(" Failed to route; checking what to rip up:\n");

	for(ctn = gdl_first(&crbs->twonets); ctn != NULL; ctn = gdl_next(&crbs->twonets, ctn)) {
		if (ctn->coll) {
			printf("  %s (%d)\n", ctn->tn->net->hdr.oid, ctn->ripped_up);
			if (ctn->ripped_up < best_r) {
				best_r = ctn->ripped_up;
				best = ctn;
			}
		}
	}

	if (best == NULL)
		return 0; /* nothing to rip up */

	printf(" ripping up %s__%ld\n", best->tn->net->hdr.oid, best->tn->uid);
	printf("GT 2net_del %s__%ld\n", best->tn->net->hdr.oid, best->tn->uid);

	sprintf(tmp, "3_GRBS_routed_%d_rip1.svg", cnt_grbs);
	crbs_grbs_draw(crbs, tmp);


	/* remove the offending 2net */
	best->ripped_up++;
	rt_topo_crbs_cdt_unref_tn(crbs, best->gtn);
	grbs_path_remove_2net(&crbs->grbs, best->gtn);
	best->gtn = NULL;

	/* re-schedule it as last (to avoid cycling through the same 2..3 nets) */
	gdl_remove(&crbs->twonets, best, link);
	gdl_append(&crbs->twonets, best, link);

	sprintf(tmp, "3_GRBS_routed_%d_rip2.svg", cnt_grbs);
	crbs_grbs_draw(crbs, tmp);

	return 1;
}

static int rt_topo_crbs_layer_(rtrnd_t *ctx, crbs_t *crbs, rtrnd_layer_t *ly, long attempt, rtrnd_layer_t *ly_cdt, rtrnd_layer_t *ly_bnk)
{
	rtrnd_layer_t ly_neighb = {0}, ly_route = {0}, ly_cdt2 = {0};
	vtp0_t annot = {0};
	char fn[1024];
	crbs_2net_t *ttn;
	int res = 0;


	vtp0_append(&annot, ly_cdt);
	vtp0_append(&annot, ly_bnk);
	snprintf(fn, sizeof(fn), "3_%s_%ld_1_cdt", ly->name, attempt);
	rtrnd_export(ctx, "svg", fn, NULL, &annot);

	/* route all nets */
	for(ttn = gdl_first(&crbs->twonets); ttn != NULL; ) {
		printf("CRBS route net %s:\n", ttn->tn->net->hdr.oid);
		if (rt_topo_crbs_route_net(crbs, ttn) != 0) {
			if (on_fail_ripup(crbs, ttn))
				continue; /* ripped up another net, retry with ttn */
			/* else: coudln't find anything to rip up or ripup count is too high
			   already: give up routing this net */
			printf(" => Giving up routing %s\n", ttn->tn->net->hdr.oid);
		}
		ttn = gdl_next(&crbs->twonets, ttn);
	}

	rtrnd_layer_init(&ly_cdt2, "cdt");
	strcpy(ly_cdt2.color, "#555555");
	vtp0_append(&annot, &ly_cdt2);

	rtrnd_layer_init(&ly_neighb, "neighb");
	strcpy(ly_neighb.color, "#ff0000");
	vtp0_append(&annot, &ly_neighb);
/*	crbs_draw_neigh(crbs, &ly_neighb);*/

	rtrnd_layer_init(&ly_route, "route");
	strcpy(ly_route.color, "#00ff00");
	vtp0_append(&annot, &ly_route);

	crbs_draw_routes(crbs, (res == 0) ? ly : NULL, &ly_route);

	rt_topo_crbs_cdt_draw(ctx, &ly_cdt2, &crbs->cdt);

	snprintf(fn, sizeof(fn), "3_%s_%ld_2_route", ly->name, attempt);
	rtrnd_export(ctx, "svg", fn, NULL, &annot);

	vtp0_uninit(&annot);
	return res;
}

static int rt_topo_crbs_layer(rtrnd_t *ctx, rtrnd_layer_t *ly, gdl_list_t *tnl)
{
	crbs_t crbs;
	rt_topo_2net_t *t;
	rtrnd_layer_t ly_cdt = {0}, ly_bnk = {0};
	int res;

	memset(&crbs, 0, sizeof(crbs_t));
	crbs.disable_vconcave = 0;
	crbs_init(&crbs, ctx);

	rt_topo_crbs_cdt_init(ctx, &crbs);
	rt_topo_crbs_cdt_create_points(ctx, ly, &crbs);

	rtrnd_layer_init(&ly_cdt, "laa");
	strcpy(ly_cdt.color, "#888888");
	rtrnd_layer_init(&ly_bnk, "laa");
	strcpy(ly_bnk.color, "#6666aa");

	/* convert input twonets to crbs twonets, making sure they have endpoints in the cdt */
	crbs.max_ripup = 0;
	for(t = gdl_first(tnl); t != NULL; t = t->link.next) {
#warning TODO: use libualloc
		crbs_2net_t *ct = calloc(sizeof(crbs_2net_t), 1);
		rtrnd_net_t *net = ct->net;

		ct->cstart = crbs_make_point_near(&crbs, t->x[0], t->y[0], 0.001, 0.001, NULL, net, 0.5);
		ct->cend = crbs_make_point_near(&crbs, t->x[1], t->y[1], 0.001, 0.001, NULL, net, 0.5);
		ct->tn = t;
		ct->net = t->net;
		gdl_append(&crbs.twonets, ct, link);
		crbs.max_ripup++;
	}

	rt_topo_crbs_cdt_create_edges(ctx, ly, &crbs);

	rt_topo_crbs_cdt_draw(ctx, &ly_cdt, &crbs.cdt);

	res = rt_topo_crbs_layer_(ctx, &crbs, ly, 0, &ly_cdt, &ly_bnk);

	crbs_uninit(&crbs);
	return res;
}
