/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020 and 2021)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

/* LAA step III: calculate minimum detour of a branch component around a
   connected component of a net.
   Included from laa3.c */

/* append the branch point br to pts if it is on the requested side of tncl */
static void laa_detour_append_sidept(vtve0_t *pts, g2d_cline_t *tncl, rtrnd_2branch_t *br, int req_side)
{
	g2d_vect_t pt;
	g2d_calc_t d;
	g2d_vect_t ve;

	pt.x = br->x; pt.y = br->y;
	d = g2d_side_cline_pt(tncl, pt);

	/* don't append if we are on the wrong side */
	if (req_side && (d > 0)) return;
	if (!req_side && (d < 0)) return;

	/* NOTE: d == 0: it is on the line; append it for a corner case:
	   we may have exacty 3 points lined up */

	ve.x = br->x; ve.y = br->y;
	vtve0_append(pts, ve);
}

static void laa_detour_append_open(rtrnd_2branch_t **open, rtrnd_2branch_t *br)
{
	if (br->found)
		return;

	br->found_next = *open;
	*open = br->found_next;
	br->found = 1;
}

/* find any outgoing 2net from br and:
   - add the terminating point (on the same layer) to pts
   - if it uses the current layer (lid) all the way to the other endpoint,
     add the other endpoint to the open list as well */
static void laa_detour_find_tns(laa3_t *laa3, vtve0_t *pts, rtrnd_2branch_t **open, g2d_cline_t *tncl, int lid, rtrnd_net_t *net, rtrnd_2branch_t *br, int side_req)
{
	const rt_topo_laa_2net_t *tn;
	rtrnd_rtree_it_t it;
	rtrnd_rtree_box_t bbox;

	bbox.x1 = br->x - 0.001; bbox.y1 = br->y - 0.001;
	bbox.x2 = br->x + 0.001; bbox.y2 = br->y + 0.001;
	for(tn = rtrnd_rtree_first(&it, laa3->r2net, &bbox); tn != NULL; tn = rtrnd_rtree_next(&it)) {
		int lasti = tn->br.used - 1;
		rtrnd_2branch_t *br1 = &tn->br.array[0], *br2 = &tn->br.array[lasti];
		if ((br1->x == br->x) && (br1->y == br->y)) {
			/* first point match, let's find our way toward the second point */
			g2d_cline_t cl;
			int idx[2];
			laa_br_get_ends_on(tn, 0, &cl, idx, lid);
			br1->found = 1;
			laa_detour_append_sidept(pts, tncl, &tn->br.array[idx[1]], side_req);
			if (idx[1] == lasti) /* all the 2net is used, may continue from the second point */
				laa_detour_append_open(open, br2);
		}
		else if ((br2->x == br->x) && (br2->y == br->y)) {
			/* first point match, let's find our way toward the second point */
			g2d_cline_t cl;
			int idx[2];
			laa_br_get_ends_on(tn, lasti, &cl, idx, lid);
			br2->found = 1;
			laa_detour_append_sidept(pts, tncl, &tn->br.array[idx[0]], side_req);
			if (idx[0] == 0) /* all the 2net is used, may continue from the first point */
				laa_detour_append_open(open, br1);
		}
	}
}

static int laa_detour_len_half(laa3_t *laa3, const rt_topo_laa_2net_t *tn, int bridx, g2d_cline_t *tncl, double tncl_len, const rt_topo_laa_2net_t *ttn, int tbridx, vtve0_t *pts, double *len, long start, int which, int make_pts)
{
	long n, hull_used;
	int lid = tn->asg[bridx], hres, res = 0;
	double dlen;
	g2d_cline_t tcl;
	rtrnd_2branch_t *open   = NULL;   /* branches to check from */
	rtrnd_2branch_t *closed = NULL; /* branches to clear ->found on */
	int idx[2];

	/* append the ends of the section-on-this-layer of the crossing branch */
	laa_br_get_ends_on(ttn, tbridx, &tcl, idx, lid);
	laa_detour_append_sidept(pts, tncl, &ttn->br.array[idx[0]], which);
	laa_detour_append_sidept(pts, tncl, &ttn->br.array[idx[1]], which);
	ttn->br.array[idx[0]].found = 1;
	ttn->br.array[idx[0]].found_next = open;
	open = &ttn->br.array[idx[0]];
	ttn->br.array[idx[1]].found = 1;
	ttn->br.array[idx[1]].found_next = open;
	open = &ttn->br.array[idx[1]];

	/* set up open: if our crossing ends in an endpoint, append new branches ending there */
	if (idx[0] == 0)
		laa_detour_append_open(&open, &ttn->br.array[idx[0]]);
	if (idx[1] == (ttn->br.used-1))
		laa_detour_append_open(&open, &ttn->br.array[idx[1]]);

	while(open != NULL) {
		rtrnd_2branch_t *br = open;
		open = open->found_next;
		br->found_next = closed;
		closed = br;

		laa_detour_find_tns(laa3, pts, &open, tncl, lid, ttn->net, br, which);
	}

	/* clear found flags */
	{
		rtrnd_2branch_t *br, *next;
		for(br = closed; br != NULL; br = next) {
			next = br->found_next;
			br->found = 0;
			br->found_next = NULL;
		}
	}

	/* now all points are in pts, from 'start' - calculate the hull */
	vtp0_enlarge(&laa3->hull, pts->used - start + 4);
	hull_used = laa3->hull.alloced;
	hres = g2d_chull((g2d_vect_t **)laa3->hull.array, &hull_used, pts->array+start, pts->used - start);
	assert(hres == 0);
	laa3->hull.used = hull_used;

	/* calculate detour len (depends on [used+1] being the first point */
	dlen = -2 * tncl_len; /* the original line is replaced by the detour; subtract it once more because the penalty is not the total length of the detour but how much longer it is than the original straight line solution */
	for(n = 0; n < laa3->hull.used; n++) { /* this depends on the hull algo really setting one extra point beyond the end which matches the first point */
		g2d_vect_t *a = laa3->hull.array[n], *b = laa3->hull.array[n+1];
		dlen += sqrt(g2d__distance2(g2d_cvect_t_convfrom_g2d_vect_t(*a), g2d_cvect_t_convfrom_g2d_vect_t(*b)));
	}

	if (dlen < *len) {
		if (make_pts) { /* save points for drawing the detour, when requested */
			g2d_vect_t *v = malloc(sizeof(g2d_vect_t) * (laa3->hull.used+1));

			/* copy over the points in the right order */
			for(n = 0; n < laa3->hull.used; n++) {
				g2d_vect_t *a = laa3->hull.array[n];
				v[n] = *a;
			}

			v[n].x = 0; v[n].y = 0; /* terminator (unused really) */

			/* replace the array in pts */
			free(pts->array);
			pts->array = v;
			pts->used = pts->alloced = laa3->hull.used;
		}
		else
			pts->used = start;

		*len = dlen;
		res = 1;
	}
	else /* we are not better, forget it! */
		pts->used = start;

	return res;
}

/* calculate a detour tn:bridx needs to do around ttn at ttn:tbridx; use
   pts for temporary storage. Currently known shortest detour is in len.
   If the new detour is
    - shorter: change *len and pts to hold the new detour
    - not shorter: leave pts and len as is.
   Return 1 if this detour is shorter than the previous
*/
static int laa_detour_len(laa3_t *laa3, const rt_topo_laa_2net_t *tn, int bridx, const rt_topo_laa_2net_t *ttn, int tbridx, vtve0_t *pts, double *len, int make_pts)
{
	g2d_cline_t tncl;
	long start;
	int idx[2], ret = 0;
	g2d_vect_t ve;
	double tncl_len;

	laa_br_get_ends_on(tn, bridx, &tncl, idx, -1);
	tncl_len = sqrt(g2d__distance2(g2d_cvect_t_convfrom_g2d_vect_t(tncl.p1), g2d_cvect_t_convfrom_g2d_vect_t(tncl.p2)));

	/* insert the line segment from the original, for first half */
	start = pts->used;
	ve.x = tn->br.array[idx[0]].x; ve.y = tn->br.array[idx[0]].y;
	vtve0_append(pts, ve);
	ve.x = tn->br.array[idx[1]].x; ve.y = tn->br.array[idx[1]].y;
	vtve0_append(pts, ve);
	ret |= laa_detour_len_half(laa3, tn, bridx, &tncl, tncl_len, ttn, tbridx, pts, len, start, 0, make_pts);

	/* insert the line segment from the original, for second half */
	start = pts->used;
	ve.x = tn->br.array[idx[0]].x; ve.y = tn->br.array[idx[0]].y;
	vtve0_append(pts, ve);
	ve.x = tn->br.array[idx[1]].x; ve.y = tn->br.array[idx[1]].y;
	vtve0_append(pts, ve);
	ret |= laa_detour_len_half(laa3, tn, bridx, &tncl, tncl_len, ttn, tbridx, pts, len, start, 1, make_pts);

	return ret;
}
