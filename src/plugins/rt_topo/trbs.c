/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020 and 2021)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"

#include <genlist/gendlist.h>

#include "data.h"
#include "io.h"
#include "route_res.h"

#include "trbs.h"

/*#include <libcdtr/trace.h>*/

#include "trbs_util.c"
#include "trbs_cdt.c"
#include "trbs_vis.c"
#include "trbs_route.c"
#include "trbs_pull.c"

static void trbs_init(trbs_t *trbs, rtrnd_t *ctx)
{
	trbs->ctx = ctx;

	/* initialize the low level allocator: use libc's malloc/free */
	trbs->sys.alloc     = uall_stdlib_alloc;
	trbs->sys.free      = uall_stdlib_free;
	trbs->sys.page_size = 4096; /* how much memory to allocate to grow the stack */

	/* initialize the stack allocators */
	trbs->crosses.sys = &trbs->sys; trbs->crosses.elem_size = sizeof(trbs_cross_t);
	trbs->tedges.sys = &trbs->sys;  trbs->tedges.elem_size = sizeof(trbs_edge_t);

}

static void trbs_clean(trbs_t *trbs, int routed_flag)
{
	long n;
	trbs_2net_t *ttn;

	/* remove non-sentinel edge crosses */
	for(n = 0; n < trbs->cdt.edges.used; n++) {
		edge_t *e = trbs->cdt.edges.array[n];
		trbs_edge_t *te = e->data;
		trbs_cross_t *cr = gdl_first(&te->crosses), *next;

		for(cr = cr->link_edge.next; cr->link_edge.next != NULL; cr = next) {
			next = cr->link_edge.next;
			gdl_remove(&te->crosses, cr, link_edge);
		}

		if (te->pp_blk) {
			te->pp_blk = 0;
			te->blocked = 0;
		}

		te->cap = te->cap_orig;
	}

	/* remove route crosses */
	for(ttn = gdl_first(&trbs->twonets); ttn != NULL; ttn = gdl_next(&trbs->twonets, ttn)) {
		trbs_cross_t *cr;
		while((cr = gdl_first(&ttn->route)) != NULL)
			gdl_remove(&ttn->route, cr, link_route);
		if (routed_flag)
			ttn->routed = 0;
	}

}

static void trbs_uninit(trbs_t *trbs)
{
	uall_stacks_clean(&trbs->crosses);
	uall_stacks_clean(&trbs->tedges);
}

static long cross_uids = 0;
trbs_cross_t *trbs_cross_new(trbs_t *trbs, trbs_2net_t *tnet, edge_t *edge, trbs_cross_t *aft)
{
	trbs_cross_t *cr = uall_stacks_alloc(&trbs->crosses);
	trbs_edge_t *tedge = edge->data;

	memset(cr, 0, sizeof(trbs_cross_t));

	cr->twonet = tnet;
	cr->edge = edge;
	if (aft != NULL)
		gdl_insert_after(&tedge->crosses, aft, cr, link_edge);


	cr->uid = cross_uids++;
	return cr;
}

static void trbs_draw_cross_uids(trbs_t *trbs, rtrnd_layer_t *ly_out)
{
	VTEDGE_FOREACH(edge, &trbs->cdt.edges)
		trbs_edge_t *tedge = edge->data;
		int n, len = gdl_length(&tedge->crosses);
		trbs_cross_t *cr;

		for(n = 0, cr = tedge->crosses.first; n < len; n++, cr = cr->link_edge.next) {
			char tmp[64];
			sprintf(tmp, "%ld", cr->uid);
			rtrnd_text_new(ly_out, cr->x, cr->y, tmp, 0.25);
		}
	VTEDGE_FOREACH_END();
}

static void trbs_draw_routes(trbs_t *trbs, rtrnd_layer_t *ly_out, rtrnd_layer_t *ly_drw)
{
	rtrnd_any_obj_t *o;
	trbs_2net_t *ttn;
	for(ttn = gdl_first(&trbs->twonets); ttn != NULL; ttn = gdl_next(&trbs->twonets, ttn)) {
		trbs_cross_t *last, *cr = gdl_first(&ttn->route);

		if (cr == NULL) {
			/* special case: route direct point-to-point on an edge - if it is really routed */
			if (ttn->routed) {
				rtrnd_line_new(ly_drw, NULL, NULL, ttn->start->pos.x, ttn->start->pos.y, ttn->end->pos.x, ttn->end->pos.y, 0.2, 0);
				if (ly_out != NULL) {
					o = (rtrnd_any_obj_t *)rtrnd_line_new(ly_out, NULL, NULL, ttn->start->pos.x, ttn->start->pos.y, ttn->end->pos.x, ttn->end->pos.y, rt_topo_cfg.wire_thick, 0);
					rtrnd_res_add(trbs->ctx, o);
				}
			}
			continue;
		}

		rtrnd_line_new(ly_drw, NULL, NULL, ttn->start->pos.x, ttn->start->pos.y, cr->x, cr->y, 0.2, 0);
		if (ly_out != NULL) {
			o = (rtrnd_any_obj_t *)rtrnd_line_new(ly_out, NULL, NULL, ttn->start->pos.x, ttn->start->pos.y, cr->x, cr->y, rt_topo_cfg.wire_thick, 0);
			rtrnd_res_add(trbs->ctx, o);
		}

		for(last = cr; cr != NULL; last = cr, cr = gdl_next(&ttn->route, cr)) {
			rtrnd_line_new(ly_drw, NULL, NULL, last->x, last->y, cr->x, cr->y, 0.2, 0);
			if (ly_out != NULL) {
				o = (rtrnd_any_obj_t *)rtrnd_line_new(ly_out, NULL, NULL, last->x, last->y, cr->x, cr->y, rt_topo_cfg.wire_thick, 0);
				rtrnd_res_add(trbs->ctx, o);
			}
		}

		rtrnd_line_new(ly_drw, NULL, NULL, ttn->end->pos.x, ttn->end->pos.y, last->x, last->y, 0.2, 0);
		if (ly_out != NULL) {
			o = (rtrnd_any_obj_t *)rtrnd_line_new(ly_out, NULL, NULL, ttn->end->pos.x, ttn->end->pos.y, last->x, last->y, rt_topo_cfg.wire_thick, 0);
			rtrnd_res_add(trbs->ctx, o);
		}
	}
}

static void reorder(trbs_t *trbs, trbs_2net_t *ttn, trbs_2net_t *before)
{
	gdl_remove(&trbs->twonets, ttn, link);
	gdl_insert_before(&trbs->twonets, before, ttn, link);
}

static int rt_topo_trbs_layer_(rtrnd_t *ctx, trbs_t *trbs, rtrnd_layer_t *ly, long attempt, rtrnd_layer_t *ly_cdt, rtrnd_layer_t *ly_bnk, int force_draw)
{
	rtrnd_layer_t ly_crossuid = {0}, ly_route = {0};
	vtp0_t annot = {0};
	char fn[1024];
	trbs_2net_t *ttn;
	int res = 0;


	vtp0_append(&annot, ly_cdt);
	vtp0_append(&annot, ly_bnk);
	snprintf(fn, sizeof(fn), "3_%s_%ld_1_cdt", ly->name, attempt);
	rtrnd_export(ctx, "svg", fn, NULL, &annot);

	/* route all nets */
	for(ttn = gdl_first(&trbs->twonets); ttn != NULL; ttn = gdl_next(&trbs->twonets, ttn)) {
		printf("TRBS route net %s:\n", ttn->net->hdr.oid);
		if (rt_topo_trbs_route_net(trbs, ttn) != 0) {
			reorder(trbs, ttn, trbs->collision);
			res = 1;
			break;
		}
	}

	rt_topo_trbs_pull_init(trbs);
	if (res == 0) {
		long n, p;
		for(n = 0; n < 1000; n++) {
			p = rt_topo_trbs_pull(trbs);
			printf(" pullres=%ld\n", p);
			if (p == 0)
				break;
		}
	}

	rtrnd_layer_init(&ly_crossuid, "crossings");
	strcpy(ly_crossuid.color, "#ff0000");
	vtp0_append(&annot, &ly_crossuid);
	trbs_draw_cross_uids(trbs, &ly_crossuid);

	rtrnd_layer_init(&ly_route, "route");
	strcpy(ly_route.color, "#00ff00");
	vtp0_append(&annot, &ly_route);
	trbs_draw_routes(trbs, ((res == 0) || force_draw) ? ly : NULL, &ly_route);

	snprintf(fn, sizeof(fn), "3_%s_%ld_2_route", ly->name, attempt);
	rtrnd_export(ctx, "svg", fn, NULL, &annot);

	vtp0_uninit(&annot);
	return res;
}

static int rt_topo_trbs_layer(rtrnd_t *ctx, rtrnd_layer_t *ly, gdl_list_t *tnl)
{
	trbs_t trbs;
	rt_topo_2net_t *t;
	rtrnd_layer_t ly_cdt = {0}, ly_bnk = {0};
	int res, tries;

	rt_topo_trbs_cdt_init(ctx, ly, &trbs);
	trbs_init(&trbs, ctx);

	rtrnd_layer_init(&ly_cdt, "laa");
	strcpy(ly_cdt.color, "#888888");
	rtrnd_layer_init(&ly_bnk, "laa");
	strcpy(ly_bnk.color, "#6666aa");

	/* convert input twonets to trbs twonets, making sure they have endpoints in the cdt */
	for(t = gdl_first(tnl); t != NULL; t = t->link.next) {
		trbs_2net_t *tt = calloc(sizeof(trbs_2net_t), 1);
		tt->start = trbs_insert_point(&trbs, t->x[0], t->y[0], NULL, t->net);
		tt->end = trbs_insert_point(&trbs, t->x[1], t->y[1], NULL, t->net);
		tt->net = t->net;
		gdl_append(&trbs.twonets, tt, link);
	}

	/* init sentinels on edges (have to be after adding all points) */
	trbs_init_cdt_edges(&trbs, &ly_bnk);

	rt_topo_trbs_cdt_draw(ctx, &ly_cdt, &trbs.cdt);

	for(tries = gdl_length(&trbs.twonets); tries > 0; tries--) {
		int is_last = (tries == 1);
		res = rt_topo_trbs_layer_(ctx, &trbs, ly, 0, &ly_cdt, &ly_bnk, is_last);
		trbs_clean(&trbs, ((res != 0) && !is_last));
		if (res == 0)
			break;
	}

	trbs_uninit(&trbs);
	return res;
}


int rt_topo_trbs(rtrnd_t *ctx, rt_topo_laa2rbs_t *src)
{
	int n, res = 0;

	/* route each layer */
	for(n = 0; n < ctx->board->layers.used; n++)
		res |= rt_topo_trbs_layer(ctx, ctx->board->layers.array[n], src->ly2nets.array[n]);

	return res;
}

