BUILDIN_RT_TOPO = \
	$(PLG)/rt_topo/rt_topo.o \
	$(PLG)/rt_topo/vt2br.o \
	$(PLG)/rt_topo/vtcr.o \
	$(PLG)/rt_topo/vtve0.o \
	$(PLG)/rt_topo/laa.o \
	$(PLG)/rt_topo/trbs.o \
	$(PLG)/rt_topo/crbs.o \
	$(THIRD)/libgrbs/grbs.o \
	$(THIRD)/libgrbs/addr_hash.o \
	$(THIRD)/libgrbs/rtree.o \
	$(THIRD)/libgrbs/debug.o
