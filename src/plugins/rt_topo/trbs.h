#ifndef RT_TOPO_RBS_H
#define RT_TOPO_RBS_H
#include "data.h"
#include <libcdtr/cdt.h>
#include <genlist/gendlist.h>
#include <libualloc/stacks_api.h>

#include "rt_topo.h"

typedef struct trbs_2net_s trbs_2net_t;
typedef struct trbs_cross_s trbs_cross_t;
typedef struct trbs_edge_s trbs_edge_t;

struct trbs_cross_s {
	edge_t *edge;
	trbs_2net_t *twonet;    /* NULL on edge endpoint sentinel */
	void *mark;             /* for libusearch A* */
	gdl_elem_t link_edge;   /* crossing within edge->crosses */
	gdl_elem_t link_route;  /* crossing within twonet->route or NULL on edge endpoint sentinel */
	long uid;
	double x, y;
	double edge_offs;       /* offset of x;y on the edge, 0..1 */
};


struct trbs_2net_s {
	point_t *start, *end;
	rtrnd_net_t *net;
	gdl_list_t route; /* of trbs_cross_t, ordered from start to end */
	gdl_elem_t link;  /* in trbs->twonets, ordered list of 2nets to route */
	unsigned routed:1; /* A* succeeded */
};

struct trbs_edge_s {
	gdl_list_t crosses; /* of trbs_cross_t, ordered from endp 0->1, always has at least 2 crossings: sentinel crossings for the endpoints */
	double cap;         /* remaining capacity */
	double cap_orig;    /* original capacity, so cap can be restored after a failed routing attempt */
	rtrnd_any_obj_t *obj; /* if not NULL, the edge is inserted for the given object */
	double vx, vy;      /* cached */
	double pdia_cop[2]; /* cached endpoint copper size matching edge->endp[] */
	double pdia_clr[2]; /* cached endpoint clearance size matching edge->endp[] */
	unsigned blocked:1; /* line along this edge */
	unsigned pp_blk:1;  /* 'blocked' set because of a point-to-point conn */
} ;

typedef struct trbs_point_s {
	long uid;
	rtrnd_any_obj_t *obj;
	rtrnd_net_t *net;
} trbs_point_t;

typedef struct trbs_s {
	rtrnd_t *ctx;
	cdt_t cdt;
	gdl_list_t twonets; /* of trbs_2net_t */

	/* caches */
	uall_sysalloc_t sys;
	uall_stacks_t crosses;
	uall_stacks_t tedges;
	long pt_uid;

	/* temporary */
	rtrnd_net_t *routing_net;
	trbs_2net_t *collision;

	point_t *target;
	void *target_mark;
} trbs_t;

/* Allocate a new crossing between tnet and edge; place it after crossing aft
   on the edge */
trbs_cross_t *trbs_cross_new(trbs_t *trbs, trbs_2net_t *tnet, edge_t *edge, trbs_cross_t *aft);


/*** A* path search ***/

/* Map possible next step edge crossings considering triangle edge visibility.
   Append (to dst) crossing points on visible target edges after which the
   new crossing point could be inserted. Starting from a point or an edge
   (specified by the crossing point the source crossing is after) */
void trbs_next_edge_from_point(trbs_t *trbs, vtp0_t *dst, point_t *from);
void trbs_next_edge_from_edge(trbs_t *trbs, vtp0_t *dst, triangle_t *t, trbs_cross_t *from_aft, double thickness);

/* Create the current best topological path for a twonet, between start and
   end or return NULL if there's no path */
trbs_2net_t *rt_top_trbs_2net_path(trbs_t *trbs, point_t *start, point_t *end);


/*** main entry point */
int rt_topo_trbs(rtrnd_t *ctx, rt_topo_laa2rbs_t *src);

#endif
