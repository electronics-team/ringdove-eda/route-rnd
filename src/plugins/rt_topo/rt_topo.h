#ifndef ROUTE_RND_TOPO_H
#define ROUTE_RND_TOPO_H

#define NETDATA_LAA(net)            ((net)->hdr.rt_data.p[0])

typedef struct {
	double x[2], y[2];        /* endpoint coords */
	rtrnd_net_t *net;         /* the net this 2net is part of */
	long uid;
	gdl_elem_t link;          /* part of an ordered list of 2nets on the same layer */
} rt_topo_2net_t;

/* Used to communicate per layer 2nets between laa and rbs */
typedef struct {
	vtp0_t ly2nets;           /* each element is a gdl_list of rt_topo_2net_t elements; index is layer ID */
} rt_topo_laa2rbs_t;

typedef struct {
	/* configured */
	double wire_thick, wire_clr, via_dia, via_clr, beta;
	int octilin, algo;
	double max_hop_mult, junction_penalty, via_via_space;

	/* calculated */
	double alpha, alpha2;

} rt_topo_cfg_t;

extern rt_topo_cfg_t rt_topo_cfg;

#endif
