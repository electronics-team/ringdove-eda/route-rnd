/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020 and 2021)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

/* stop pulling if distance/difference is smaller than this value in mm */
#define STOP_PULL_DIFF 0.001

#define DEBUG_CR_UID 245

#ifdef DEBUG_CR_UID
#	define crprintf if (cr->uid == DEBUG_CR_UID) printf
#else
static void crprintf(const char *fmt, ...) {}
#endif



static void rt_topo_trbs_place_cr(trbs_t *trbs)
{
	VTEDGE_FOREACH(edge, &trbs->cdt.edges)
		trbs_edge_t *tedge = edge->data;
		double x, y, vx = edge->endp[1]->pos.x - edge->endp[0]->pos.x, vy = edge->endp[1]->pos.y - edge->endp[0]->pos.y, vlen;
		int n, len = gdl_length(&tedge->crosses);
		trbs_cross_t *cr;
		g2d_cline_t el = g2d_cline(g2d_vect(edge->endp[0]->pos.x, edge->endp[0]->pos.y), g2d_vect(edge->endp[1]->pos.x, edge->endp[1]->pos.y));


		vlen = sqrt(vx*vx + vy*vy);
		tedge->vx = vx / vlen;
		tedge->vy = vy / vlen;

		vx /= (double)(len-1);
		vy /= (double)(len-1);
		vx -= 2*vx/8;
		vy -= 2*vy/8;
		x = edge->endp[0]->pos.x + vx/8;
		y = edge->endp[0]->pos.y + vy/8;

		for(n = 0, cr = tedge->crosses.first; n < len; n++, x += vx, y += vy, cr = cr->link_edge.next) {
			cr->x = x;
			cr->y = y;
			cr->edge_offs = g2d__offs_cline_pt(&el, g2d_cvect(x, y));
		}
	VTEDGE_FOREACH_END();
}

static rtrnd_net_t *get_pnet(point_t *p)
{
	trbs_point_t *tp = p->data;
	if ((tp == NULL) || (tp->obj == NULL))
		return NULL;
	return tp->obj->hdr.net;
}

static long rt_topo_trbs_pull_cr(trbs_t *trbs, rtrnd_net_t *net, trbs_cross_t *prev, trbs_cross_t *cr, trbs_cross_t *next)
{
	edge_t *e = cr->edge;
	trbs_edge_t *te = e->data;
	g2d_cline_t el = g2d_cline(g2d_vect(e->endp[0]->pos.x, e->endp[0]->pos.y), g2d_vect(e->endp[1]->pos.x, e->endp[1]->pos.y));
	g2d_cline_t tl = g2d_cline(g2d_vect(prev->x, prev->y), g2d_vect(next->x, next->y));
	g2d_cvect_t ip[2];
	g2d_offs_t offs[2];
	int iscs, can_do_best, moved, samenet;
	double adj_cop, adj_clr, my_cop, my_clr, stepdir, ax, ay, step, bumpx, bumpy, bumpo, besto, dx, dy;
	trbs_cross_t *adj;
	rtrnd_net_t *pnet;

	/* calculate crossing point of the route-prev-next points and the edge;
	   that's where our crossing should be if we wanted minimum wire length */
	iscs = g2d__iscp_cline_cline_o(&el, &tl,  ip,  offs, 1);

	/* overlap: it does not matter where we cross, can't pull */
	if (iscs == 2)
		return 0;

	besto = offs[0]; /* where we'd get the shortest line */

	if (iscs == 0) {
		/* point out of the edge range, beyond either the starting or ending point */
		if (besto <= 0.0) {
			adj = gdl_prev(&te->crosses, cr);
			stepdir = +1;
		}
		else if (besto >= 1.0) {
			adj = gdl_next(&te->crosses, cr);
			stepdir = -1;
		}
		else
			goto in_range;
	}
	else { /* point insiged of the edge range see in which direction to the current offset */
		in_range:;
		if (besto < cr->edge_offs) {
			adj = gdl_prev(&te->crosses, cr);
			stepdir = +1;
		}
		else {
			adj = gdl_next(&te->crosses, cr);
			stepdir = -1;
		}
	}

	/* check if adjacent cr we try to get close to is a sentinel */
	if (gdl_prev(&te->crosses, adj) == NULL) {
		adj_cop = te->pdia_cop[0];
		adj_clr = te->pdia_clr[0];
		ax = e->endp[0]->pos.x;
		ay = e->endp[0]->pos.y;
		pnet = get_pnet(e->endp[0]);
	}
	else if (gdl_next(&te->crosses, adj) == NULL) {
		adj_cop = te->pdia_cop[1];
		adj_clr = te->pdia_clr[1];
		ax = e->endp[1]->pos.x;
		ay = e->endp[1]->pos.y;
		pnet = get_pnet(e->endp[1]);
	}
	else { /* adj is another net */
		adj_cop = rt_topo_cfg.wire_thick / 2.0;
		adj_clr = rt_topo_cfg.wire_clr;
		ax = adj->x;
		ay = adj->y;
		pnet = NULL;
	}

	my_cop = rt_topo_cfg.wire_thick / 2.0;
	my_clr = rt_topo_cfg.wire_clr;


	samenet = ((pnet != NULL) && (net == pnet));

	if (!samenet)
		step = adj_cop + my_cop + TRBS_MAX(adj_clr, my_clr);
	else
		step = 0;

	crprintf("pull cr %ld at %f;%f o=%f   step=%f stepdir=%f   v=%f;%f\n", cr->uid, cr->x, cr->y, cr->edge_offs, step, stepdir, te->vx, te->vy);

	bumpx = ax + step * stepdir * te->vx;
	bumpy = ay + step * stepdir * te->vy;
	bumpo = g2d__offs_cline_pt(&el, g2d_cvect(bumpx, bumpy));

	can_do_best = 0;

	/* if best is between bump and current, it is safe for going the best */
	if ((besto >= 0) && (besto <= 1)) {
		if (cr->edge_offs < bumpo) {
			if ((besto >= cr->edge_offs) && (besto <= bumpo))
				can_do_best = 1;
		}
		else { /* bumpo < cr->edge_offs */
			if ((besto >= bumpo) && (besto <= cr->edge_offs))
				can_do_best = 1;
		}
	}

	if (can_do_best) {
		dx = ip[0].x - cr->x;
		dy = ip[0].y - cr->y;
		cr->x = ip[0].x;
		cr->y = ip[0].y;
		cr->edge_offs = besto;
	}
	else {
		dx = bumpx - cr->x;
		dy = bumpy - cr->y;
		cr->x = bumpx;
		cr->y = bumpy;
		cr->edge_offs = bumpo;
	}

	moved = ((dx*dx + dy*dy) > (STOP_PULL_DIFF*STOP_PULL_DIFF));

	crprintf(" => %f;%f o=%f can_do_best=%d moved=%d\n", cr->x, cr->y, cr->edge_offs, can_do_best, moved);

	return moved;
}

static long rt_topo_trbs_pull_net(trbs_t *trbs, trbs_2net_t *ttn)
{
	long pulled = 0;
	trbs_cross_t ctmp = {0}, *cr = gdl_first(&ttn->route), *next;

	if (cr == NULL)
		return 0;

	ctmp.x = ttn->start->pos.x;
	ctmp.y = ttn->start->pos.y;

	/* corner case: single cr route; fake both start and end cr */
	next = gdl_next(&ttn->route, cr);
	if (next == NULL) {
		trbs_cross_t ctmp2 = {0};
		ctmp2.x = ttn->end->pos.x;
		ctmp2.y = ttn->end->pos.y;
		return rt_topo_trbs_pull_cr(trbs, ttn->net, &ctmp, cr, &ctmp2);
	}

	/* normal case; first point needs to be faked */
	pulled += rt_topo_trbs_pull_cr(trbs, ttn->net, &ctmp, cr, next);

	for(cr = gdl_next(&ttn->route, cr); gdl_next(&ttn->route, cr) != NULL; cr = gdl_next(&ttn->route, cr))
		pulled += rt_topo_trbs_pull_cr(trbs, ttn->net, gdl_prev(&ttn->route, cr), cr, gdl_next(&ttn->route, cr));

	/* fake last point */
	ctmp.x = ttn->end->pos.x;
	ctmp.y = ttn->end->pos.y;
	pulled += rt_topo_trbs_pull_cr(trbs, ttn->net, gdl_prev(&ttn->route, cr), cr, &ctmp);

	return pulled;
}

static void rt_topo_trbs_pull_init(trbs_t *trbs)
{
	rt_topo_trbs_place_cr(trbs);
}

static long rt_topo_trbs_pull(trbs_t *trbs)
{
	trbs_2net_t *ttn;
	long pulled = 0;

	for(ttn = gdl_first(&trbs->twonets); ttn != NULL; ttn = gdl_next(&trbs->twonets, ttn))
		pulled += rt_topo_trbs_pull_net(trbs, ttn);

	return pulled;
}

