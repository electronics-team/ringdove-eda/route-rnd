/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020 and 2021)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"

#include <libusteiner/libusteiner.h>
#include "geo.h"
#include <gengeo2d/cline.h>
#include <gengeo2d/sline.h>
#include <gengeo2d/chull.h>
#include <libpsrand/mtw.h>
#include <libualloc/stacks_api.h>

#include "data.h"
#include "vt2br.h"
#include "vtcr.h"
#include "vtve0.h"
#include "rtree.h"
#include "find.h"
#include "io.h"

#include "rt_topo.h"
#include "laa.h"


#define LAA_MIN(a, b) (((a) < (b)) ? (a) : (b))
#define LAA_MAX(a, b) (((a) > (b)) ? (a) : (b))

/* return a bitmask with all 1's for all possible layers */
static long layer_bits_all(rtrnd_t *ctx)
{
	return (1 << ctx->board->layers.used) - 1;
}

/* return a bitmask corresponding to the object; for layer objects this means
   1 only on the layer the object is on; for vias it's a block of 1s the via
   spans */
static long layer_bits(rtrnd_t *ctx, rtrnd_any_obj_t *obj)
{
	long lid;

	switch(obj->hdr.type) {
		case RTRND_LINE:
		case RTRND_ARC:
		case RTRND_POLY:
			for(lid = 0; lid < ctx->board->layers.used; lid++)
				if (&obj->hdr.parent->layer == ctx->board->layers.array[lid])
					return 1 << lid;
			assert(!"invalid layer");
			return 0;

		case RTRND_VIA:
#warning TODO: this ignores bbvia
			return layer_bits_all(ctx);

		case RTRND_BOARD:
		case RTRND_LAYER:
		case RTRND_NET:
		case RTRND_NETSEG:
			assert(!"invalid object for layer_bits");
			break;
	}
	return 0;
}

#include "laa1.c"
#include "laa2.c"
#include "laa3.c"

laa_test_hook_t laa_test_hook[5];

int rt_topo_laa(rtrnd_t *ctx, rt_topo_laa2rbs_t *dst)
{
	rtrnd_layer_t ly_mst = {0};
	rtrnd_rtree_t r2net;
	vtp0_t annot = {0};
	int n, res = 0;

	rtrnd_layer_init(&ly_mst, "laa");
	strcpy(ly_mst.color, "#777700");

	rtrnd_rtree_init(&r2net);

	if (laa_test_hook[0] != NULL) laa_test_hook[0](ctx, &annot, &r2net);
	laa_2nets(ctx, &ly_mst, &r2net); /* step I */

	if (laa_test_hook[1] != NULL) laa_test_hook[1](ctx, &annot, &r2net);
	laa_2vias(ctx, &ly_mst, &r2net); /* step II */

	vtp0_append(&annot, &ly_mst);
	rtrnd_export(ctx, "svg", "2_1_laa", NULL, &annot);
	annot.used = 0;
	rtrnd_layer_uninit(&ly_mst);

	if (laa_test_hook[2] != NULL) laa_test_hook[2](ctx, &annot, &r2net);

	res = laa_solve(ctx, &annot, &r2net, dst); /* step III */

	if (laa_test_hook[4] != NULL) laa_test_hook[4](ctx, &annot, &r2net);

	rtrnd_export(ctx, "svg", "2_2_laa", NULL, &annot);

	for(n = 0; n < annot.used; n++) {
		rtrnd_layer_uninit(annot.array[n]);
		free(annot.array[n]);
	}

	rtrnd_rtree_uninit(&r2net);
	vtp0_uninit(&annot);
	return res;
}
