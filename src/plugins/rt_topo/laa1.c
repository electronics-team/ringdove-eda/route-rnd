/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: topological, extended rubber band sketch (based on Tal Dayan's thesis)
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020 and 2021)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

/* LAA step I: build the 2net branch graph for each net (no vias); builds the
   minimal steiner tree per net. Included from laa.c */

/* Create a new node in the steiner tree input for a point */
static ustn_node_t *laa_mst_add_point(ustn_tree_t *tree, ustn_coord_t x, ustn_coord_t y, int fixed)
{
	ustn_node_t *nd = ustn_find_node(tree, x, y, fixed);
	if (nd == NULL) {
		nd = ustn_add_node(tree, x, y);
		if (fixed > 0)
			nd->fixed = 1;
	}
	return nd;
}

/* Add an object to the steiner tree input */
static void laa_mst_add(rtrnd_t *ctx, ustn_tree_t *mst, rtrnd_any_obj_t *obj)
{
	ustn_node_t *p1, *p2;
	ustn_edge_t *e;

	switch(obj->hdr.type) {
		case RTRND_LINE:
			if (obj->hdr.terminal) { /* place one point, in the center */
				laa_mst_add_point(mst, (obj->line.cline.p1.x + obj->line.cline.p2.x)/2, (obj->line.cline.p1.y + obj->line.cline.p2.y)/2, -1);
				break;
			}

			if (g2d__distance2(g2d_cvect_t_convfrom_g2d_vect_t(obj->line.cline.p1), g2d_cvect_t_convfrom_g2d_vect_t(obj->line.cline.p2)) < 2) {
				laa_mst_add_point(mst, obj->line.cline.p1.x, obj->line.cline.p1.y, -1);
				break;
			}

			/* create unmovable points and connect with an edge */
			p1 = laa_mst_add_point(mst, obj->line.cline.p1.x, obj->line.cline.p1.y, 1);
			p2 = laa_mst_add_point(mst, obj->line.cline.p2.x, obj->line.cline.p2.y, 1);
			e = ustn_add_edge(mst, p1, p2);
			e->fixed = 1;
			e->user_ptr = obj;
			p1->user_long |= p2->user_long |= e->user_long |= layer_bits(ctx, obj);
			p1->user_ptr = obj;
			p2->user_ptr = obj;
			break;
		case RTRND_VIA:
			p1 = laa_mst_add_point(mst, obj->via.x, obj->via.y, -1);
			p1->user_long |= layer_bits_all(ctx);
			p1->user_ptr = obj;
			break;

		case RTRND_ARC:
			assert(!"TODO: arc");
			break;
		case RTRND_POLY:
			if (obj->hdr.terminal) { /* place one point, in the center */
				double cx = 0, cy = 0;
				rtp_vertex_t *v;
				for(v = gdl_first(&obj->poly.rtpoly.lst); v != NULL; v = v->link.next) {
					cx += v->x;
					cy += v->y;
				}
				if (gdl_length(&obj->poly.rtpoly.lst) > 1) {
					cx /= (double)gdl_length(&obj->poly.rtpoly.lst);
					cy /= (double)gdl_length(&obj->poly.rtpoly.lst);
				}
				p1 = laa_mst_add_point(mst, cx, cy, -1);
				p1->user_long = layer_bits(ctx, obj);
				p1->user_ptr = obj;
			}
			else {
#warning TODO: non-terminal poly heuristics
			}
			break;
		case RTRND_BOARD:
		case RTRND_LAYER:
		case RTRND_NET:
		case RTRND_NETSEG:
			assert(!"invalid object for the steiner tree");
			break;
	}
}

/* Returns if a node is in a terminal (in which case it should be considered unmovable) */
static int homed(ustn_node_t *nd)
{
	return (nd->x == nd->ix) && (nd->y == nd->iy);
}

/* calculate the steiner tree of a single net */
static void laa_2net(rtrnd_t *ctx, rtrnd_net_t *net, rtrnd_layer_t *ly, rtrnd_rtree_t *r2net)
{
	ustn_tree_t mst = {0};
	double wth;
	long n;
	rt_topo_2nets_t *tns = calloc(sizeof(rt_topo_2nets_t), 1);

	NETDATA_LAA(net) = tns;
	tns->net = net;

	rtrnd_any_obj_t *obj;
	for(obj = gdl_first(&net->objs); obj != NULL; obj = gdl_next(&net->objs, obj))
		laa_mst_add(ctx, &mst, obj);

	/* make sure steiner points are not too close: leave room for 8 wires */
	wth = (rt_topo_cfg.wire_thick*8 + rt_topo_cfg.wire_clr*4) * rt_topo_cfg.junction_penalty;
	mst.pt_too_close2 = wth*wth;

	/* calculate the minimal steiner tree */
	ustn_solve_iterate_pre(&mst);
	while(mst.itc < 9000)
		if (!ustn_solve_iterate(&mst))
			break;
	ustn_optimize(&mst);


	/* collect mst edges as 2-nets */
	for(n = 0; n < mst.edges.used; n++) {
		ustn_edge_t *edge = mst.edges.array[n];
		rt_topo_laa_2net_t *tn;
		rtrnd_any_obj_t *o1, *o2;

		/* do not route if start and end are in the same netseg (they are
		   already connected) */
		o1 = edge->p1->user_ptr;
		o2 = edge->p2->user_ptr;
		if ((o1 != NULL) && (o2 != NULL) && (o1->hdr.netseg == o2->hdr.netseg))
			continue;

		tn = calloc(sizeof(rt_topo_laa_2net_t), 1);

		/* steiner points are vias, potentially using any layer */
		if ((edge->p1->user_long == 0) && !edge->p1->fixed) edge->p1->user_long = layer_bits_all(ctx);
		if ((edge->p2->user_long == 0) && !edge->p2->fixed) edge->p2->user_long = layer_bits_all(ctx);

		tn->bbox.x1 = LAA_MIN(edge->p1->x, edge->p2->x);
		tn->bbox.y1 = LAA_MIN(edge->p1->y, edge->p2->y);
		tn->bbox.x2 = LAA_MAX(edge->p1->x, edge->p2->x);
		tn->bbox.y2 = LAA_MAX(edge->p1->y, edge->p2->y);
		tn->x1 = edge->p1->x; tn->y1 = edge->p1->y;
		tn->x2 = edge->p2->x; tn->y2 = edge->p2->y;
		tn->p1ly = edge->p1->user_long; tn->p1fx = edge->p1->fixed || homed(edge->p1);
		tn->p2ly = edge->p2->user_long; tn->p2fx = edge->p2->fixed || homed(edge->p2);
		tn->ely = edge->user_long; tn->efx = edge->fixed;
		tn->net = net;

		rtrnd_rtree_insert(r2net, tn, &tn->bbox);

		tn->next = tns->head;
		tns->head = tn;
	}


#warning TODO: free mst fields
}

/* Main entry point, deals with the whole board */
static void laa_2nets(rtrnd_t *ctx, rtrnd_layer_t *ly, rtrnd_rtree_t *r2net)
{
	htsp_entry_t *e;

	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e))
		laa_2net(ctx, e->value, ly, r2net);
}
