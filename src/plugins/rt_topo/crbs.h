#ifndef RT_TOPO_CRBS_H
#define RT_TOPO_CRBS_H

#include "data.h"
#include "rt_topo.h"
#include <libcdtr/cdt.h>
#include <genlist/gendlist.h>
#include <libualloc/stacks_api.h>
#include <libgrbs/grbs.h>

#include <libgrbs/addr_hash.h>


typedef grbs_addr_key_t htad_key_t;
typedef struct htad_value_s {
	unsigned valid:1;
	grbs_detached_addr_t *det; /* 3 entries */
	void *mark;
} htad_value_t;
#define HT_INVALID_VALUE (htad_value_t){0}
#define HT(x) htad_ ## x
#include <genht/ht.h>
#undef HT

typedef struct {
	point_t *cstart, *cend;
	rt_topo_2net_t *tn;     /* for nets we are routing; NULL for fixed objects */
	rtrnd_net_t *net;
	grbs_2net_t *gtn;
	unsigned int ripped_up; /* how many times this net got ripped up */
	unsigned coll:1;        /* collided with current routing effort */
	unsigned old:1;         /* virtual 2-net for an old object that already existed on load */
	gdl_elem_t link;
} crbs_2net_t;

typedef struct crbs_point_s {
	rtrnd_any_obj_t *obj;
	point_t *cpt;
	grbs_point_t *gpt;
	rtrnd_net_t *net;
} crbs_point_t;

typedef struct crbs_edge_s {
	long nets;     /* nets parallel with this edge */
	double ang[2]; /* outgoing edge angle, indexed by endp[] */
	edge_t *edge;
} crbs_edge_t;

typedef struct {
	rtrnd_t *ctx;
	cdt_t cdt;
	grbs_t grbs;
	gdl_list_t twonets; /* of crbs_2net_t */
	htad_t addrs;

	unsigned disable_vconcave:1;  /* when set to 1, so not do the virtual-concave optimization */

	/* routing */
	grbs_detached_addr_t *first;  /* the address the twonet is starting from */
	grbs_detached_addr_t *target; /* the address we are trying to reach */
	void *first_mark, *target_mark;
	grbs_2net_t *routing_tn;
	int max_ripup; /* how many times are we willing to rip up a 2net (total within a layer routing) */
	long max_hop_cnt; /* stop hopeless searches after this many hops */
} crbs_t;

int rt_topo_crbs(rtrnd_t *ctx, rt_topo_laa2rbs_t *src);

/* When a collision happens but the 2net is not known */
extern grbs_2net_t tn_unknown;

/* create a new point (e.g. for a via) in the triangulation and in grbs */
point_t *crbs_make_point(crbs_t *crbs, double x, double y, double cop, double clr, rtrnd_via_t *via, rtrnd_net_t *net);


/*** internal ***/
void crbs_draw_routes(crbs_t *crbs, rtrnd_layer_t *ly_out, rtrnd_layer_t *ly_drw);
void crbs_draw_vias(rtrnd_t *ctx);

#define DIST_HEUR_MULT 1000.0

#endif
