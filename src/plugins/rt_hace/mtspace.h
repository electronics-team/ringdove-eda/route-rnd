/*
 *                            COPYRIGHT
 *
 *  pcb-rnd, interactive printed circuit board design
 *  (this file is based on PCB, interactive printed circuit board design)
 *  Copyright (C) 1994,1995,1996 Thomas Nau
 *  Copyright (C) 1998,1999,2000,2001 harry eaton
 *
 *  this file, mtspace.h, was written and is
 *  Copyright (c) 2001 C. Scott Ananian.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 *
 *
 *  Old contact info:
 *  harry eaton, 6697 Buttonhole Ct, Columbia, MD 21044 USA
 *  haceaton@aplcomm.jhuapl.edu
 *
 */

/* "empty space" routines (needed for via-space tracking in the auto-router. */

#ifndef PCB_MTSPACE_H
#define PCB_MTSPACE_H

/* mtspace data structures are built on r-trees. */

#include "config.h"
#include "data.h"
#include "vector.h"							/* for vector_t in mtspace_query_rect prototype */

/* 1 nm to match pcb-rnd's original behavior */
#define DELTA 0.0000001

typedef struct rnd_cheap_point_s {
	double X, Y;
} rnd_cheap_point_t;

RTRND_INLINE rtrnd_bool_t rnd_box_is_good(const rtrnd_rtree_box_t *b)
{
	return (b->x1 < b->x2) && (b->y1 < b->y2);
}

RTRND_INLINE rtrnd_bool_t rnd_box_intersect(const rtrnd_rtree_box_t * a, const rtrnd_rtree_box_t * b)
{
	return (a->x1 < b->x2) && (b->x1 < a->x2) && (a->y1 < b->y2) && (b->y1 < a->y2);
}

RTRND_INLINE rtrnd_bool_t rnd_point_in_box(const rtrnd_rtree_box_t * box, double X, double Y)
{
	return (X >= box->x1) && (Y >= box->y1) && (X <= box->x2) && (Y <= box->y2);
}

#define RND_MIN(a,b)  ((a) < (b) ? (a) : (b))
#define RND_MAX(a,b)  ((a) > (b) ? (a) : (b))
#define RND_MAKE_MIN(a,b)            if ((b) < (a)) (a) = (b)
#define RND_MAKE_MAX(a,b)            if ((b) > (a)) (a) = (b)

RTRND_INLINE rtrnd_bool_t rnd_box_in_box(const rtrnd_rtree_box_t *outer, const rtrnd_rtree_box_t *inner)
{
	return (outer->x1 <= inner->x1) && (inner->x2 <= outer->x2) && (outer->y1 <= inner->y1) && (inner->y2 <= outer->y2);
}

RTRND_INLINE rtrnd_rtree_box_t rnd_clip_box(const rtrnd_rtree_box_t *box, const rtrnd_rtree_box_t *clipbox)
{
	rtrnd_rtree_box_t r;
	assert(rnd_box_intersect(box, clipbox));
	r.x1 = RND_MAX(box->x1, clipbox->x1);
	r.x2 = RND_MIN(box->x2, clipbox->x2);
	r.y1 = RND_MAX(box->y1, clipbox->y1);
	r.y2 = RND_MIN(box->y2, clipbox->y2);
	return r;
}

RTRND_INLINE rtrnd_rtree_box_t rnd_shrink_box(const rtrnd_rtree_box_t * box, double amount)
{
	rtrnd_rtree_box_t r = *box;
	r.x1 += amount;
	r.y1 += amount;
	r.x2 -= amount;
	r.y2 -= amount;
assert(r.x1 < r.x2);
assert(r.y1 < r.y2);
	return r;
}

RTRND_INLINE rtrnd_rtree_box_t rnd_bloat_box(const rtrnd_rtree_box_t * box, double amount)
{
	return rnd_shrink_box(box, -amount);
}

RTRND_INLINE rnd_cheap_point_t rnd_closest_cheap_point_in_box(const rnd_cheap_point_t * from, const rtrnd_rtree_box_t * box)
{
	rnd_cheap_point_t r;
	assert(box->x1 < box->x2 && box->y1 < box->y2);
	r.X = (from->X < box->x1) ? box->x1 : (from->X > box->x2) ? box->x2 : from->X;
	r.Y = (from->Y < box->y1) ? box->y1 : (from->Y > box->y2) ? box->y2 : from->Y;
	assert(rnd_point_in_box(box, r.X, r.Y));
	return r;
}

/* construct a box that holds a single point */
RTRND_INLINE rtrnd_rtree_box_t rnd_point_box(double X, double Y)
{
	rtrnd_rtree_box_t r;
	r.x1 = X;
	r.x2 = X + 0.000001;
	r.y1 = Y;
	r.y2 = Y + 0.000001;
	return r;
}

#ifndef MIN
#define MIN(a,b)  ((a) < (b) ? (a) : (b))
#define MAX(a,b)  ((a) > (b) ? (a) : (b))
#endif

#define RND_CLAMP(x, low, high)  (((x) > (high)) ? (high) : (((x) < (low)) ? (low) : (x)))
#define RND_ABS(a)               (((a) < 0) ? -(a) : (a))
#define RND_BOX_CENTER_X(b) ((b).x1 + ((b).x2 - (b).x1)/2)
#define RND_BOX_CENTER_Y(b) ((b).y1 + ((b).y2 - (b).y1)/2)

#define RND_BOX_ROTATE_TO_NORTH(box, dir) \
do { double t;\
	switch(dir) {\
	case RND_EAST: \
		t = (box).x1; (box).x1 = (box).y1; (box).y1 = -(box).x2;\
		(box).x2 = (box).y2; (box).y2 = -t; break;\
	case RND_SOUTH: \
		t = (box).x1; (box).x1 = -(box).x2; (box).x2 = -t;\
		t = (box).y1; (box).y1 = -(box).y2; (box).y2 = -t; break;\
	case RND_WEST: \
		t = (box).x1; (box).x1 = -(box).y2; (box).y2 = (box).x2;\
		(box).x2 = -(box).y1; (box).y1 = t; break;\
	case RND_NORTH: break;\
	default: assert(0);\
	}\
} while (0)

#define RND_BOX_ROTATE_FROM_NORTH(box, dir) \
do { double t;\
	switch(dir) {\
	case RND_WEST: \
		t = (box).x1; (box).x1 = (box).y1; (box).y1 = -(box).x2;\
		(box).x2 = (box).y2; (box).y2 = -t; break;\
	case RND_SOUTH: \
		t = (box).x1; (box).x1 = -(box).x2; (box).x2 = -t;\
		t = (box).y1; (box).y1 = -(box).y2; (box).y2 = -t; break;\
	case RND_EAST: \
		t = (box).x1; (box).x1 = -(box).y2; (box).y2 = (box).x2;\
		(box).x2 = -(box).y1; (box).y1 = t; break;\
	case RND_NORTH: break;\
	default: assert(0);\
	}\
} while (0)


typedef struct mtspace mtspace_t;
typedef enum { FIXED, ODD, EVEN } mtspace_type_t;
typedef struct vetting vetting_t;

/* create an "empty space" representation with a shrunken boundary */
mtspace_t *mtspace_create(void);
/* destroy an "empty space" representation. */
void mtspace_destroy(mtspace_t ** mtspacep);

/* -- mutation -- */

/* add a space-filler to the empty space representation.  The given box
 * should *not* be bloated; it should be "true".  The feature will fill
 * *at least* a radius of clearance around it;
 */
void mtspace_add(mtspace_t * mtspace, const rtrnd_rtree_box_t * box, mtspace_type_t which, double clearance);
/* remove a space-filler from the empty space representation.  The given box
 * should *not* be bloated; it should be "true".  The feature will fill
 * *at least* a radius of clearance around it;
 */
void mtspace_remove(mtspace_t * mtspace, const rtrnd_rtree_box_t * box, mtspace_type_t which, double clearance);


vetting_t *mtspace_query_rect(mtspace_t * mtspace, const rtrnd_rtree_box_t * region,
															double radius, double clearance,
															vetting_t * work,
															vector_t * free_space_vec,
															vector_t * lo_conflict_space_vec,
															vector_t * hi_conflict_space_vec, rtrnd_bool_t is_odd, rtrnd_bool_t with_conflicts, rnd_cheap_point_t * desired);

void mtsFreeWork(vetting_t **);
int mtsBoxCount(vetting_t *);

#endif /* ! PCB_MTSPACE_H */
