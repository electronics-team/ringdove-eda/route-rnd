 /*
  *                            COPYRIGHT
  *
  *  pcb-rnd, interactive printed circuit board design
  *  Copyright (C) 2017,2020 Tibor 'Igor2' Palinkas
  *
  *  This program is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program; if not, write to the Free Software
  *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
  */

#include <librnd/config.h>

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "rtree.h"
#include "rnd_rtree.h"

/* Temporary compatibility layer for the transition */
rtrnd_rtree_t *rnd_r_create_tree(void)
{
	rtrnd_rtree_t *root = malloc(sizeof(rtrnd_rtree_t));
	rtrnd_rtree_init(root);
	return root;
}

void rnd_r_destroy_tree(rtrnd_rtree_t **tree)
{
	rtrnd_rtree_uninit(*tree);
	free(*tree);
	*tree = NULL;
}

void rnd_r_insert_entry(rtrnd_rtree_t *rtree, const rtrnd_rtree_box_t *which)
{
	assert(which != NULL);
	rtrnd_rtree_insert(rtree, (void *)which, (rtrnd_rtree_box_t *)which); /* assumes first field is the bounding box */
}

void rnd_r_insert_array(rtrnd_rtree_t *rtree, const rtrnd_rtree_box_t *boxlist[], long len)
{
	long n;

	if (len == 0)
		return;

	assert(boxlist != 0);

	for(n = 0; n < len; n++)
		rnd_r_insert_entry(rtree, boxlist[n]);
}

rtrnd_bool_t rnd_r_delete_entry(rtrnd_rtree_t *rtree, const rtrnd_rtree_box_t *which)
{
	assert(which != NULL);
	return rtrnd_rtree_delete(rtree, (void *)which, (rtrnd_rtree_box_t *)which) == 0; /* assumes first field is the bounding box */
}

rtrnd_bool_t rnd_r_delete_entry_free_data(rtrnd_rtree_t *rtree, rtrnd_rtree_box_t *box, void (*free_data)(void *d))
{
	void *obj = box; /* assumes first field is the bounding box */
	assert(obj != NULL);
	if (rtrnd_rtree_delete(rtree, obj, (rtrnd_rtree_box_t *)box) != 0)
		return rtrnd_false;
	free_data(obj);
	return rtrnd_true;
}

typedef struct {
	rnd_r_dir_t (*region_in_search)(const rtrnd_rtree_box_t *region, void *closure);
	rnd_r_dir_t (*rectangle_in_region)(const rtrnd_rtree_box_t *box, void *closure);
	void *clo;
} r_cb_t;

static rtrnd_rtree_dir_t r_cb_node(void *ctx_, void *obj, const rtrnd_rtree_box_t *box)
{
	r_cb_t *ctx = (r_cb_t *)ctx_;
	return ctx->region_in_search((const rtrnd_rtree_box_t *)box, ctx->clo);
}

static rtrnd_rtree_dir_t r_cb_obj(void *ctx_, void *obj, const rtrnd_rtree_box_t *box)
{
	r_cb_t *ctx = (r_cb_t *)ctx_;
	return ctx->rectangle_in_region((const rtrnd_rtree_box_t *)obj, ctx->clo);
}


rnd_r_dir_t rnd_r_search(rtrnd_rtree_t *rtree, const rtrnd_rtree_box_t *query,
	rnd_r_dir_t (*region_in_search)(const rtrnd_rtree_box_t *region, void *closure),
	rnd_r_dir_t (*rectangle_in_region)(const rtrnd_rtree_box_t *box, void *closure),
	void *closure, int *num_found)
{
	rnd_r_dir_t res;
	rtrnd_rtree_cardinal_t out_cnt;
	r_cb_t ctx;
	ctx.region_in_search = region_in_search;
	ctx.rectangle_in_region = rectangle_in_region;
	ctx.clo = closure;

	res = rtrnd_rtree_search_any(rtree, (const rtrnd_rtree_box_t *)query,
		(ctx.region_in_search != NULL) ? r_cb_node : NULL,
		(ctx.rectangle_in_region != NULL) ? r_cb_obj : NULL,
		&ctx, &out_cnt);

	if (num_found != NULL)
		*num_found = out_cnt;

	return res;
}

int rnd_r_region_is_empty(rtrnd_rtree_t *rtree, const rtrnd_rtree_box_t *region)
{
	return rtrnd_rtree_is_box_empty(rtree, (const rtrnd_rtree_box_t *)region);
}

void rnd_r_free_tree_data(rtrnd_rtree_t *rtree, void (*free)(void *ptr))
{
	rtrnd_rtree_it_t it;
	void *o;

	for(o = rtrnd_rtree_all_first(&it, rtree); o != NULL; o = rtrnd_rtree_all_next(&it))
		free(o);
}

rtrnd_rtree_box_t *rnd_r_first(rtrnd_rtree_t *tree, rtrnd_rtree_it_t *it)
{
	if (tree == NULL)
		return NULL;
	return (rtrnd_rtree_box_t *)rtrnd_rtree_all_first(it, tree);
}

rtrnd_rtree_box_t *rnd_r_next(rtrnd_rtree_it_t *it)
{
	return (rtrnd_rtree_box_t *)rtrnd_rtree_all_next(it);
}

void rnd_r_end(rtrnd_rtree_it_t *it)
{
}
