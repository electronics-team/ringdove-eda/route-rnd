#ifndef RTRND_RT_HORVER_BUS_H
#define RTRND_RT_HORVER_BUS_H

#include "util_grid.h"

/* bus lines on the edge */
typedef struct {
	int len;
	rtrnd_raline_t *raline; /* array of 'len' lines */
} bus_t;

void bus_init(bus_t *bus, int dirpol, double first, double spacing, int len, double minor0, double minor1);
void bus_uninit(bus_t *bus);
rtrnd_raline_t *bus_reserve(bus_t *bus, double from, double to);
void bus_draw_grid(bus_t *bus, rtrnd_layer_t *ly, int is_major_x);


#endif
