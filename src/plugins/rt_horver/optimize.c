/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: escape wires for simple grid based horizontal/vertical routing
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include <genht/htpp.h>
#include <genht/hash.h>
#include <genht/ht_utils.h>
#include "rt_horver.h"
#include "optimize.h"
#include "netseg.h"

#include "route_res.h"

static int debug = 0;

typedef struct { /* possible escape in a given direction */
	rtrnd_raline_t *rl;     /* original, for major */
	rtrnd_raline_t availm;   /* local, containing arm range; inverted: blocked ranges are the ones where arms reach out; based on mid points */
	rtrnd_raline_t availo;   /* local, containing arm range; inverted: blocked ranges are the ones where arms reach out; based on obj points */
	double obj_major;
} arm_t;

static int opt1_map_cb(void *cbctx, rtrnd_raline_t *rl, int is_major_x, int dirpol, double mid_minor, double bus_minor, double obj_major, double obj_minor, int dry)
{
	htpp_t *rlhash = cbctx;
	arm_t *arm;
	long avidx, cnt = 0;

	if (!htpp_has(rlhash, rl)) {
		arm = calloc(sizeof(arm_t), 1);
		arm->availo.major = rl->major;
		arm->availm.major = rl->major;
		arm->rl = rl;
		arm->obj_major = obj_major;
		htpp_set(rlhash, rl, arm);
		vtd0_append(&arm->availo.minor, dirpol < 0 ? -10000 : obj_minor);
		vtd0_append(&arm->availo.minor, dirpol > 0 ? +10000 : obj_minor);
		vtd0_append(&arm->availm.minor, dirpol < 0 ? -10000 : obj_minor);
		vtd0_append(&arm->availm.minor, dirpol > 0 ? +10000 : obj_minor);
	}
	arm = htpp_get(rlhash, rl);


	avidx = rtrnd_raline_pt_range(rl, mid_minor);
	if (avidx >= 0) {
		double fromm, tom, fromo, too;
printf("  MAP: %f %d : mid=%f in %f;%f\n", rl->major, dirpol, mid_minor, rl->minor.array[avidx], rl->minor.array[avidx+1]);
		if (dirpol < 0) {
			fromm = rl->minor.array[avidx];
			tom = mid_minor;
			fromo = rl->minor.array[avidx];
			too = obj_minor;
		}
		else {
			fromm = mid_minor;
			tom = rl->minor.array[avidx+1];
			fromo = obj_minor;
			too = rl->minor.array[avidx+1];
		}

		rtrnd_raline_block(&arm->availm, fromm, tom);
		rtrnd_raline_block(&arm->availo, fromo, too);

		/* another object might be crossing this rl, which is then a better option (no dogleg needed) */
		if (arm->rl->major == obj_major)
			arm->obj_major = obj_major;

		cnt++;
		if (debug)
			printf("   map: %f   +    for %f..%f (idx=%ld)\n", rl->major, fromm, tom, avidx);
	}
	else if (debug)
		printf("   map: %f failed for %f..%f\n", rl->major, bus_minor, mid_minor);
	

	return cnt;
}

static void free_arms(htpp_t *rlhash)
{
	genht_uninit_deep(htpp, rlhash, {
		arm_t *arm = htent->value;
		if (arm != NULL) {
			vtd0_uninit(&arm->availm.minor);
			vtd0_uninit(&arm->availo.minor);
			free(arm);
		}
	});

	free(rlhash);
}

typedef struct {
	rtrnd_raline_t *rl[2];
	double major[2];
	double minor[3];
	double score;
} match_t;

static void dump_ra(rtrnd_raline_t *ra)
{
	long n;
	for(n = 0; n < ra->minor.used; n+=2)
		printf(" [%f %f]", ra->minor.array[n], ra->minor.array[n+1]);
	printf("\n");
}

static void find_overlap(rtrnd_ragrid_t *grid, arm_t *arm1, arm_t *arm2, match_t *best)
{
	long n, m, o, penalty;
	match_t ma;

	printf("    find_overlap:\n");
	printf("     arm1 %f:", arm1->rl->major); dump_ra(&arm1->availo);
	printf("     arm2 %f:", arm2->rl->major); dump_ra(&arm2->availo);

	for(n = 1; n < arm1->availm.minor.used-2; n+=2) {
		m = rtrnd_raline_pt_neg_range(&arm2->availm, arm1->availm.minor.array[n]);
		o = rtrnd_raline_pt_neg_range(&arm2->availo, arm1->availo.minor.array[n]);
		if (m >= 0) {
			printf("       match: %ld %ld at %f!\n", n, m, arm1->availm.minor.array[n]);
			ma.rl[0] = arm2->rl;
			ma.rl[1] = arm1->rl;
			ma.major[0] = arm2->obj_major;
			ma.major[1] = arm1->obj_major;
			ma.minor[0] = arm2->availm.minor.array[m];
			ma.minor[2] = arm1->availm.minor.array[n+1];

			/* mid-point */
			ma.minor[1] = (arm2->availm.minor.array[m] + arm1->availm.minor.array[n+1])/2;

			/* make sure the above path is still available - other opt1 paths may have blocked it */
			if (!rtrnd_raline_range_avail(ma.rl[0], ma.minor[0], ma.minor[1])) continue;
			if (!rtrnd_raline_range_avail(ma.rl[1], ma.minor[1], ma.minor[2])) continue;

			/* adjust end to reach the object */
			{
				long n2;
				ma.minor[0] = arm2->availo.minor.array[o];

				/* nothing guarantees 'n' found for avaim means the same for availo; search the same range in availo here, assuming it's wider */
				n2 = rtrnd_raline_pt_neg_range(&arm1->availo, arm1->availm.minor.array[n+1]);
				ma.minor[2] = arm1->availo.minor.array[n2+1];
			}

			penalty = rtrnd_raline_dist(grid, arm1->rl, arm2->rl);
			if (penalty < 0)
				penalty = -penalty;
			ma.score = fabs(ma.minor[2] - ma.minor[0]) + penalty;
			if (ma.score < best->score) {
				*best = ma;
				printf("       better match! score=%f\n", ma.score);
			}
			else {
				printf("       worst  match! score=%f\n", ma.score);
			}
		}
	}
}


static void find_best_match(rtrnd_t *ctx, horver_t *hvctx, int is_major_x, rtrnd_netseg_t *ns1, rtrnd_netseg_t *ns2)
{
	rtrnd_ragrid_t *grid = &hvctx->grid[is_major_x];
	htpp_entry_t *e;
	htpp_t *left = NSDATA_O1LEFT(ns1), *right = NSDATA_O1RIGHT(ns2);
	match_t best;

	if ((NSDATA_O1MERGE(ns1) == ns2) || (NSDATA_O1MERGE(ns2) == ns1))
		return;

	best.score = HUGE_VAL;

	printf("  bestmatch %s vs. %s:\n", ns1->hdr.oid, ns2->hdr.oid);

	for(e = htpp_first(left); e != NULL; e = htpp_next(left, e)) {
		arm_t *arm1 = e->value, *arm2, *arm2m = NULL, *arm2p = NULL;
		rtrnd_raline_t *r;

#warning TODO: consider +-1 majors on arm2
/*		printf("    major %f:\n", arm1->rl->major);*/

		/* from arm1 to the same major on arm2 */
		arm2 = htpp_get(right, arm1->rl);

		/* figure previous and next arm2 possibilities (step one up or down in major direction) */
		r = rtrnd_raline_step(grid, arm1->rl, -1);
		if (r != NULL)
			arm2m = htpp_get(right, r);
		r = rtrnd_raline_step(grid, arm1->rl, +1);
		if (r != NULL)
			arm2p = htpp_get(right, r);

#warning TODO: straight connection is disabled for now for debugging
		if (arm2 != NULL)
			find_overlap(grid, arm1, arm2, &best);
		if (arm2m != NULL)
			find_overlap(grid, arm1, arm2m, &best);
		if (arm2p != NULL)
			find_overlap(grid, arm1, arm2p, &best);

	}
	
	if (best.score < HUGE_VAL) {
		NSDATA_O1MERGE(ns2) = ns1;
		printf("  best: %f: %f;%f %f;%f %f;%f %f;%f\n", best.score,
			best.minor[0], best.rl[0]->major,
			best.minor[1], best.rl[0]->major,
			best.minor[1], best.rl[1]->major,
			best.minor[2], best.rl[1]->major
		);

		if (is_major_x) {
			/* dogleg */
			rtrnd_draw_res_line(ctx, hvctx->ly_copper[is_major_x], hvctx->ly_escape[is_major_x], ns1->net,
				best.major[0], best.minor[0], best.rl[0]->major, best.minor[0], wire_thick, wire_clr, 0.1, 0);

			rtrnd_draw_res_line(ctx, hvctx->ly_copper[is_major_x], hvctx->ly_escape[is_major_x], ns1->net,
				best.rl[0]->major, best.minor[0], best.rl[0]->major, best.minor[1], wire_thick, wire_clr, 0.1, 0);
			rtrnd_draw_res_line(ctx, hvctx->ly_copper[is_major_x], hvctx->ly_escape[is_major_x], ns1->net,
				best.rl[0]->major, best.minor[1], best.rl[1]->major, best.minor[1], wire_thick, wire_clr, 0.1, 0);
			rtrnd_draw_res_line(ctx, hvctx->ly_copper[is_major_x], hvctx->ly_escape[is_major_x], ns1->net,
				best.rl[1]->major, best.minor[1], best.rl[1]->major, best.minor[2], wire_thick, wire_clr, 0.1, 0);

			/* dogleg */
			rtrnd_draw_res_line(ctx, hvctx->ly_copper[is_major_x], hvctx->ly_escape[is_major_x], ns1->net,
				best.rl[1]->major, best.minor[1], best.major[1], best.minor[2], wire_thick, wire_clr, 0.1, 0);

			rtrnd_raline_block(best.rl[0], best.minor[0], best.minor[1]);
			rtrnd_raline_block(best.rl[1], best.minor[1], best.minor[2]);
		}
		else {
		 /* dogleg */
			rtrnd_draw_res_line(ctx, hvctx->ly_copper[is_major_x], hvctx->ly_escape[is_major_x], ns1->net,
				best.minor[0], best.rl[0]->major, best.minor[0], best.major[0], wire_thick, wire_clr, 0.1, 0);

			rtrnd_draw_res_line(ctx, hvctx->ly_copper[is_major_x], hvctx->ly_escape[is_major_x], ns1->net,
				best.minor[0], best.rl[0]->major, best.minor[1], best.rl[0]->major, wire_thick, wire_clr, 0.1, 0);
			rtrnd_draw_res_line(ctx, hvctx->ly_copper[is_major_x], hvctx->ly_escape[is_major_x], ns1->net,
				best.minor[1], best.rl[0]->major, best.minor[1], best.rl[1]->major, wire_thick, wire_clr, 0.1, 0);
			rtrnd_draw_res_line(ctx, hvctx->ly_copper[is_major_x], hvctx->ly_escape[is_major_x], ns1->net,
				best.minor[1], best.rl[1]->major, best.minor[2], best.rl[1]->major, wire_thick, wire_clr, 0.1, 0);

			/* dogleg */
			rtrnd_draw_res_line(ctx, hvctx->ly_copper[is_major_x], hvctx->ly_escape[is_major_x], ns1->net,
				best.minor[2], best.rl[1]->major, best.minor[2], best.major[1], wire_thick, wire_clr, 0.1, 0);

			rtrnd_raline_block(best.rl[0], best.minor[0], best.minor[1]);
			rtrnd_raline_block(best.rl[1], best.minor[1], best.minor[2]);
		}
	}
}

long opt1_noescape_dir(rtrnd_t *ctx, horver_t *hvctx, int is_major_x)
{
	rtrnd_ragrid_t *grid = &hvctx->grid[is_major_x];
	htsp_entry_t *e;
	long cnt = 0;

	printf("optimize step 1 in dir is_major_x=%d\n", is_major_x);
	/* build the has for arms facing left (dirpol=-1) */
	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;
		rtrnd_netseg_t *ns;
		for(ns = gdl_first(&net->segments); ns != NULL; ns = gdl_next(&net->segments, ns)) {
			rtrnd_any_obj_t *o;
			htpp_t *left, *right;

			NSDATA_O1LEFT(ns)  = left = htpp_alloc(ptrhash, ptrkeyeq);
			NSDATA_O1RIGHT(ns) = right = htpp_alloc(ptrhash, ptrkeyeq);

			for(o = gdl_first(&ns->objs); o != NULL; o = gdl_next(&ns->objs, o)) {
				horver_map_obj(opt1_map_cb, left, grid, o, is_major_x, -1, 0);
				horver_map_obj(opt1_map_cb, right, grid, o, is_major_x, +1, 0);
			}
		}
	}

	/* do a search on arms facing right (dirpol=+1) to see if any of them meet
	   any of the hashed left facing arms */
	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;
		rtrnd_netseg_t *ns1, *ns2;
		for(ns1 = gdl_first(&net->segments); ns1 != NULL; ns1 = gdl_next(&net->segments, ns1)) {
			for(ns2 = gdl_first(&net->segments); ns2 != NULL; ns2 = gdl_next(&net->segments, ns2)) {
				if (ns1 == ns2)
					continue;
				find_best_match(ctx, hvctx, is_major_x, ns1, ns2);
			}
		}
	}


	/* free hashes */
	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;
		rtrnd_netseg_t *ns, *target;

		/* free arms data */
		for(ns = gdl_first(&net->segments); ns != NULL; ns = gdl_next(&net->segments, ns)) {
			free_arms(NSDATA_O1LEFT(ns));
			free_arms(NSDATA_O1RIGHT(ns));
			NSDATA_O1LEFT(ns) = NSDATA_O1RIGHT(ns) = NULL;
		}

		/* merge net segs that got connected */
		for(ns = gdl_first(&net->segments); ns != NULL; ns = gdl_next(&net->segments, ns)) {
			target = NSDATA_O1MERGE(ns);
			if (target != NULL) {
				printf("Merge req: %s with %s\n", ns->hdr.oid, target->hdr.oid);
				rtrnd_netseg_merge(target, ns);
			}
			NSDATA_O1MERGE(ns) = NULL;
		}
	}

	return cnt;
}


long opt1_noescape(rtrnd_t *ctx, horver_t *hvctx)
{
	return opt1_noescape_dir(ctx, hvctx, 0) + opt1_noescape_dir(ctx, hvctx, 1);
}

