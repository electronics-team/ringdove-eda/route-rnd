#ifndef RT_HORVER_H
#define RT_HORVER_H

typedef struct horver_s horver_t;

#include "data.h"
#include "util_grid.h"
#include "escape.h"
#include "bus.h"

extern horver_t *hvctx; /* temporary */
extern double wire_thick, wire_clr, via_dia, via_clr; /* temporary */
extern int disable_dir[4];

#define NETDATA_IS_MAJOR_X(net)    ((net)->hdr.rt_data.l[0])
#define NETDATA_DIR(net)           ((net)->hdr.rt_data.l[1])

#define NETDATA_FAILDIR(net, dir)      ((net)->hdr.rt_data.l[2] & (1 << dir))
#define NETDATA_FAILDIR_SET(net, dir)  ((net)->hdr.rt_data.l[2] |= (1 << dir))
#define NETDATA_FAILDIRS(net)          ((net)->hdr.rt_data.l[2] & 0xF)
#define NETDATA_FAILED(net)            ((net)->hdr.rt_data.l[2] & 0x80000)
#define NETDATA_FAILED_SET(net)        ((net)->hdr.rt_data.l[2] |= 0x80000)
#define NETDATA_FAILED_CLR(net)        ((net)->hdr.rt_data.l[2] &= ~0x80000)


#define NSDATA_IDX(ns)            ((ns)->hdr.rt_data.l[0])
#define NSDATA_ESEG(ns)           ((ns)->hdr.rt_data.p[1])
#define NSDATA_O1LEFT(ns)         ((ns)->hdr.rt_data.p[1])
#define NSDATA_O1RIGHT(ns)        ((ns)->hdr.rt_data.p[2])
#define NSDATA_O1MERGE(ns)        ((ns)->hdr.rt_data.p[3])



#ifndef RT_HORVER_ONLY_TYPEDEF

struct horver_s {
	rtrnd_layer_t *ly_wiregrid, *ly_escape[2], *ly_busgrid, *ly_copper[2];
	rtrnd_ragrid_t grid[2];
	g2d_box_t wirebox;
	escape_t escape[4]; /* indexed with escape_dir_t */
	bus_t bus[4]; /* indexed with escape_dir_t */
};

double wirebox_minor_end(int is_major_x, int dirpol);


/* Append all possible escape lines of an object to eseg.
   Return the number of newly added ralines available for escape */
typedef int (*horver_map_cb_t)(void *cbctx, rtrnd_raline_t *rl, int is_major_x, int dirpol, double mid_minor, double bus_minor, double obj_major, double obj_minor, int dry);
long horver_map_obj(horver_map_cb_t cb, void *cbctx, rtrnd_ragrid_t *grid, rtrnd_any_obj_t *o, int is_major_x, int dirpol, int dry);

#endif
#endif
