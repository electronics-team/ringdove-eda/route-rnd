typedef struct { /* possible escape in a given direction */
	rtrnd_raline_t *rl;
	rtrnd_raline_t arms; /* the range is available where any object of the netseg can reach */
} o1_poss_t;

long opt1_noescape(rtrnd_t *ctx, horver_t *hvctx);

