/* Calculate matching in a bipartite graph. Input:
   - nodes are numbered from 0 to num_nodes
   - nodes are implicitly grouped in sets u and v
     (every edge connects one u and one v node)
   - (typically u nodes are listed first then v nodes)
   - hpkp_solve() returns the number of pairs...
   - ... and loads ctx->match[] so that for every u it gives the corresponding v
   - the Hopcroft-Karp algorithm is used for solving the problem
*/

typedef struct {
	int num_nodes, num_edges, max_edges;
	int *estart, *enext, *edge_v, *match, *cost, *bp;
} hpkp_t;

/* num_nodes is num_u + num_v; max_edge is an upper estimation of how many
   times hpkp_add_edge() is going to be called */
void hpkp_init(hpkp_t *ctx, int num_nodes, int max_edges);

/* free all memory within ctx (doesn't free ctx itself) */
void hpkp_uninit(hpkp_t *ctx);

void hpkp_add_edge(hpkp_t *ctx, int u, int v);

int hpkp_solve(hpkp_t *ctx);
