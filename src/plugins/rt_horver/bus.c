/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: bus at the edges
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "bus.h"
#include "rt_horver.h"

void bus_init(bus_t *bus, int dirpol, double first, double spacing, int len, double minor0, double minor1)
{
	int n;
	double ma;

	/* cheat: cheap way to make sure vias next to each other on adjacent bus lines won't get too close in diagonal */
	if (spacing < via_dia + via_clr)
		spacing *= 1.5;

	if (dirpol < 0)
		spacing = -spacing;

	bus->len = len;
	bus->raline = calloc(sizeof(rtrnd_raline_t), len);
	for(ma = first, n = 0; n < len; n++, ma += spacing) {
		bus->raline[n].major = ma;
		vtd0_append(&bus->raline[n].minor, minor0);
		vtd0_append(&bus->raline[n].minor, minor1);
	}
}

void bus_uninit(bus_t *bus)
{
	int n;
	for(n = 0; n < bus->len; n++)
		vtd0_uninit(&bus->raline[n].minor);
	free(bus->raline);
}


rtrnd_raline_t *bus_reserve(bus_t *bus, double from, double to)
{
	int n;
	for(n = 0; n < bus->len; n++) {
		if (rtrnd_raline_range_avail(&bus->raline[n], from, to)) {
			rtrnd_raline_block(&bus->raline[n], from, to);
			return &bus->raline[n];
		}
	}
	return NULL;
}


void bus_draw_grid(bus_t *bus, rtrnd_layer_t *ly, int is_major_x)
{
	int n;
	for(n = 0; n < bus->len; n++) {
		rtrnd_raline_t *rl = &bus->raline[n];
		long last = rl->minor.used - 1;
		if (is_major_x)
			rtrnd_line_new(ly, NULL, NULL, rl->major, rl->minor.array[0], rl->major, rl->minor.array[last], 0.1, 0);
		else
			rtrnd_line_new(ly, NULL, NULL, rl->minor.array[0], rl->major, rl->minor.array[last], rl->major, 0.1, 0);
	}
}

