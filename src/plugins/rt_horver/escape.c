/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: escape wires for simple grid based horizontal/vertical routing
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "rt_horver.h"
#include "hpkp.h"
#include "escape.h"

#include "route_res.h"

/* Add an escape raline to eseg at escape line rl, if it is available the
   escape range is available in direction dirpol. An escape lane spans from
   bus_minor to mid_minor then a dogleg connects rl->major;mid_minor with
   obj_major;obj_minor.
   Returns 1 if escape found is available.
   If dry is non-zero, do not modify eseg or rl.
   */
static int escape_add(void *cbctx, rtrnd_raline_t *rl, int is_major_x, int dirpol, double mid_minor, double bus_minor, double obj_major, double obj_minor, int dry)
{
	escape_seg_t *eseg = cbctx;
	escape_dir_t dir;
	int n, avail;
	vtp0_t *lines;

	/* check if eseg already contains an escape line for this major; if so,
	   look if we can optimize it (get the escape shorter) */
	if (!dry)
	for(n = 0; n < eseg->len; n++) {
		if (eseg->poss[n].rl->major == rl->major) {
			/* choose the mid_minor that is closer to the edge - escape lines should be short */
			if (dirpol < 0) {
				if (mid_minor < eseg->poss[n].mid_minor) {
					eseg->poss[n].mid_minor = mid_minor;
					eseg->poss[n].obj_minor = obj_minor;
					eseg->poss[n].obj_major = obj_major;
					if (!eseg->poss[n].avail) {
						dir = ESCAPE_DIR(is_major_x, dirpol);
						rl = eseg->poss[n].rl;
						eseg->poss[n].avail = rtrnd_raline_range_avail(rl, bus_minor, mid_minor);
						printf("   FR1M: ma=%f mi=%f..%f avail=%d rlid=%ld\n", rl->major, mid_minor, bus_minor, eseg->poss[n].avail, rl->rt_data.l[dir]);
					}
				}
			}
			else {
				if (mid_minor > eseg->poss[n].mid_minor) {
					eseg->poss[n].mid_minor = mid_minor;
					eseg->poss[n].obj_minor = obj_minor;
					eseg->poss[n].obj_major = obj_major;
					if (!eseg->poss[n].avail) {
						dir = ESCAPE_DIR(is_major_x, dirpol);
						rl = eseg->poss[n].rl;
						eseg->poss[n].avail = rtrnd_raline_range_avail(rl, mid_minor, bus_minor);
						printf("   FR0M: ma=%f mi=%f..%f avail=%d rlid=%ld\n", rl->major, mid_minor, bus_minor, eseg->poss[n].avail, rl->rt_data.l[dir]);
					}
				}
			}
			return eseg->poss[n].avail; /* already added */
		}
	}

	/* new major, not present in eseg yet; check if the main escape range
	between bus_minor and mid_minor is avaialble */
	if (mid_minor > bus_minor)
		avail = rtrnd_raline_range_avail(rl, bus_minor, mid_minor);
	else
		avail = rtrnd_raline_range_avail(rl, mid_minor, bus_minor);

	if (dry) return avail;

	/* allocate an rl within the target eseg for this major and remember
	   the current idea for escaping (we may optimize it to shorter minor
	   later on if we are called with the same major from another object
	   again) */
	eseg->poss[eseg->len].rl = rl;
	eseg->poss[eseg->len].avail = avail;
	eseg->poss[n].bus_minor = bus_minor;
	eseg->poss[n].mid_minor = mid_minor;
	eseg->poss[n].obj_major = obj_major;
	eseg->poss[n].obj_minor = obj_minor;
	eseg->len++;

	/* also remember the raline, by id, per escape direction */
	dir = ESCAPE_DIR(is_major_x, dirpol);
	if (rl->rt_data.l[dir] == 0) {
		rl->rt_data.l[dir] = hvctx->escape[dir].next_raline_id++;
		lines = &hvctx->escape[dir].ralines;
		vtp0_append(lines, rl);
	}

	printf("   From: ma=%f mi=%f..%f avail=%d rlid=%ld\n", rl->major, mid_minor, bus_minor, avail, rl->rt_data.l[dir]);
	return avail;
}

static long escape_obj_append(escape_seg_t *eseg, rtrnd_ragrid_t *grid, rtrnd_any_obj_t *o, int is_major_x, int dirpol, int dry)
{
	return horver_map_obj(escape_add, eseg, grid, o, is_major_x, dirpol, dry);
}


/* return 1 if object is reacahble on the layer of the escape line in a given direction */
static int escape_obj_on_right_layer(horver_t *hvctx, rtrnd_any_obj_t *o, int is_major_x)
{
#warning TODO: this ignores bbvia
	if (o->hdr.type == RTRND_VIA) return 1;

	return (&o->hdr.parent->layer == hvctx->ly_copper[is_major_x]);
}

/* Calculate all escape lines for each net segment of a net
   in a given direction and sotre the resulting newly allocated eseg
   in each netseg; returns number of available escapes or -1 on error */
static long escape_net(horver_t *hvctx, rtrnd_ragrid_t *grid, rtrnd_net_t *net, int is_major_x, int dirpol, int dry)
{
	rtrnd_netseg_t *ns;
	long cnt = 0;
	int bad = 0;

	for(ns = gdl_first(&net->segments); ns != NULL; ns = gdl_next(&net->segments, ns)) {
		escape_seg_t *eseg = NULL;
		rtrnd_any_obj_t *o;
		double span;

		if (!dry) {
			eseg = calloc(sizeof(escape_seg_t), 1);
			eseg->chosen = -1;
			NSDATA_ESEG(ns) = eseg;
		}

		/* worst case allocation: all lines the bbox of the segment potentially spans,
		   +1 before +1 after */
		span = is_major_x ? (ns->hdr.bbox.x2 - ns->hdr.bbox.x1) : (ns->hdr.bbox.y2 - ns->hdr.bbox.y1);
		span = ceil(span / grid->spacing) + 2;
		if (!dry)
			eseg->poss = malloc(sizeof(escape_poss_t) * ((long)span));

		if (!dry)
			printf(" ns %s bbox = %f;%f .. %f;%f alloc-span=%ld\n", ns->hdr.oid, ns->hdr.bbox.x1, ns->hdr.bbox.y1, ns->hdr.bbox.x2, ns->hdr.bbox.y2, (long)span);

		/* map esegs from each object of the netseg */
		for(o = gdl_first(&ns->objs); o != NULL; o = gdl_next(&ns->objs, o)) {
			long r;
			if (!escape_obj_on_right_layer(hvctx, o, is_major_x))
				continue;
			r = escape_obj_append(eseg, grid, o, is_major_x, dirpol, dry);
			if (r < 0)
				bad++;
			else
				cnt += r;
		}
	}

	return bad ? -1 : cnt;
}

/* Init the escape system, mapping all escapes of all nets in the best
   direction for the given net */
void escape_init(rtrnd_t *ctx, horver_t *hvctx)
{
	htsp_entry_t *e;
	int g;
	long n;

	/* reset raline allocation */
	for(g = 0; g < 2; g++) {
		for(n = 0; n < hvctx->grid[g].len; n++) {
			rtrnd_raline_t *rl = &hvctx->grid[g].raline[n];
			rl->rt_data.l[0] = rl->rt_data.l[1] = rl->rt_data.l[2] = rl->rt_data.l[3] = 0;
		}
	}

	printf("ESC: determining net escape direction\n");
	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;
		printf(" net: %s segs=%ld\n", net->hdr.oid, net->segments.length);
		if (net->segments.length > 1) {
			escape_dir_t dir;
			rtrnd_netseg_t *ns;
			int n, bestn;
			long best = 0, av[4] = {0};
			int ismx[4] = {0,   0,  1,  1};
			int dpol[4] = {-1, +1, -1, +1};

			/* estimate number of escapes in all directions that have not been disabled/failed */
			for(n = 0; n < 4; n++) {
				dir = ESCAPE_DIR(ismx[n], dpol[n]);
				if (NETDATA_FAILDIR(net, dir) || disable_dir[dir])
					av[n] = -1; /* already failed in that dir or disabled */
				else
					av[n] = escape_net(hvctx, &hvctx->grid[ismx[n]], net, ismx[n], dpol[n], 1);
			}

			printf(" escapes per dir:");

			/* for now simply pick the most promising one:
			   the direction that is not yet failed/disabled and has the
			   largest number of potential escapes */
			for(bestn = n = 0; n < 4; n++) {
					printf(" %ld", av[n]);
				if (av[n] > best) {
					best = av[n];
					bestn = n;
				}
			}

			if (av[bestn] <= 0) {
				printf("  picked nothing: no chance to escape %s\n", net->hdr.oid);
				continue;
			}

			printf("  picked: %ld/%ld\n", best, av[bestn]);

			/* assign direction to the net */
			NETDATA_IS_MAJOR_X(net) = ismx[bestn];
			NETDATA_DIR(net) = dpol[bestn];
			dir = ESCAPE_DIR(NETDATA_IS_MAJOR_X(net), NETDATA_DIR(net));

			/* assign an unique ID to each net seg */
			for(ns = gdl_first(&net->segments); ns != NULL; ns = gdl_next(&net->segments, ns))
				NSDATA_IDX(ns) = hvctx->escape[dir].num_ns++;
		}
	}

	/* allocates the vector for all available escape lines */
	for(n = 0; n < 4; n++) {
		hvctx->escape[n].next_raline_id = hvctx->escape[n].num_ns;
		vtp0_init(&hvctx->escape[n].ralines);
	}

	/* map all escape lines in the direction picked for the net */
	printf("ESC: escapeing\n");
	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;
		if (net->segments.length > 1) {
			long r;
			int grididx = !!NETDATA_IS_MAJOR_X(net);
			r = escape_net(hvctx, &hvctx->grid[grididx], net, NETDATA_IS_MAJOR_X(net), NETDATA_DIR(net), 0);
			printf(" net esc: %s segs=%ld res=%ld\n", net->hdr.oid, net->segments.length, r);
		}
	}

}

/* free all escape lines and esegs in each netsg */
void escape_uninit(rtrnd_t *ctx, horver_t *hvctx)
{
	int n;
	htsp_entry_t *e;

	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;
		if (net->segments.length > 1) {
			rtrnd_netseg_t *ns;
			for(ns = gdl_first(&net->segments); ns != NULL; ns = gdl_next(&net->segments, ns)) {
				escape_seg_t *eseg = NSDATA_ESEG(ns);
				free(eseg->poss);
				free(eseg);
			}
		}
	}

	for(n = 0; n < 4; n++)
		vtp0_uninit(&hvctx->escape[n].ralines);
}

/* Draw the escape line of a netseg at a given rl->major grid line */
void escape_draw(rtrnd_t *ctx, horver_t *hvctx, int is_major_x, int dirpol, rtrnd_netseg_t *ns, rtrnd_raline_t *rl)
{
	escape_seg_t *eseg = NSDATA_ESEG(ns);
	long n;

	for(n = 0; n < eseg->len; n++) {
		if (eseg->poss[n].rl->major == rl->major) {
			double mid_minor = eseg->poss[n].mid_minor, obj_major = eseg->poss[n].obj_major, obj_minor = eseg->poss[n].obj_minor;
			double mi_edge = eseg->poss[n].bus_minor;
			if (is_major_x) {
				rtrnd_draw_res_line(ctx, hvctx->ly_copper[is_major_x], hvctx->ly_escape[is_major_x], ns->net, rl->major, mid_minor, rl->major, mi_edge, wire_thick, wire_clr, 0.1, 0);
				if ((rl->major != obj_major) || (mid_minor != obj_minor))
					rtrnd_draw_res_line(ctx, hvctx->ly_copper[is_major_x], hvctx->ly_escape[is_major_x], ns->net, rl->major, mid_minor, obj_major, obj_minor, wire_thick, wire_clr, 0.1, 0);
			}
			else {
				rtrnd_draw_res_line(ctx, hvctx->ly_copper[is_major_x], hvctx->ly_escape[is_major_x], ns->net, mid_minor, rl->major, mi_edge, rl->major, wire_thick, wire_clr, 0.1, 0);
				if ((rl->major != obj_major) || (mid_minor != obj_minor))
					rtrnd_draw_res_line(ctx, hvctx->ly_copper[is_major_x], hvctx->ly_escape[is_major_x], ns->net, mid_minor, rl->major, obj_minor, obj_major, wire_thick, wire_clr, 0.1, 0);
			}
		}
	}
}

/* when placed an escape line in one direction, available escape lines on
   the opposite direction may be affected. Since they are already as short
   as possible for a netseg, if they are affected, they are simply unavailable */
static void escape_adjust_avail(rtrnd_t *ctx, horver_t *hvctx, int is_major_x, int dirpol, double major, double minor_from, double minor_to)
{
	htsp_entry_t *e;

	if (minor_from > minor_to) {
		double tmp = minor_from;
		minor_from = minor_to;
		minor_to = tmp;
	}

	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;

		/* skip nets not trying to use this direction */
		if ((NETDATA_IS_MAJOR_X(net) != is_major_x) || (NETDATA_DIR(net) != dirpol))
			continue;

		if (net->segments.length > 1) {
			rtrnd_netseg_t *ns;
			for(ns = gdl_first(&net->segments); ns != NULL; ns = gdl_next(&net->segments, ns)) {
				long n;
				escape_seg_t *eseg = NSDATA_ESEG(ns);

				for(n = 0; n < eseg->len; n++) {
					if (!eseg->poss[n].avail || (eseg->poss[n].rl->major != major))
						continue;
					/* found a major */
					printf("Possible ADJUST at major %f\n", major);
					if ((eseg->poss[n].mid_minor >= minor_from) && (eseg->poss[n].mid_minor <= minor_to)) {
						printf(" had to disable\n");
						eseg->poss[n].avail = 0;
					}
				}
			}
		}
	}
}


/* make the pairing for a direction; returns the number of net segs failed to escape */
long escape_calc(rtrnd_t *ctx, horver_t *hvctx, int is_major_x, int dirpol, int adjust_dirpol)
{
	htsp_entry_t *e;
	escape_dir_t dir = ESCAPE_DIR(is_major_x, dirpol);
	hpkp_t h;
	long bad = 0;

	printf("PAIRING: segs=%ld lines=%ld (%ld)\n", hvctx->escape[dir].num_ns, hvctx->escape[dir].ralines.used, hvctx->escape[dir].next_raline_id);
	if (hvctx->escape[dir].num_ns == 0) {
		printf(" -> unused, skipping\n");
		return 0;
	}

	if (hvctx->escape[dir].ralines.used == 0) {
		printf(" -> no ralines, skipping\n");
		return 0;
	}

	/* u:v is ns:raline */
	hpkp_init(&h, hvctx->escape[dir].next_raline_id, hvctx->escape[dir].num_ns * hvctx->escape[dir].ralines.used);

	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;

		/* skip nets not trying to use this direction */
		if ((NETDATA_IS_MAJOR_X(net) != is_major_x) || (NETDATA_DIR(net) != dirpol))
			continue;

		if (net->segments.length > 1) {
			rtrnd_netseg_t *ns;
			for(ns = gdl_first(&net->segments); ns != NULL; ns = gdl_next(&net->segments, ns)) {
				escape_seg_t *eseg = NSDATA_ESEG(ns);
				long n, found;
				for(found = n = 0; n < eseg->len; n++) {
					rtrnd_raline_t *rl = eseg->poss[n].rl;
					if (eseg->poss[n].avail) {
						hpkp_add_edge(&h, NSDATA_IDX(ns), rl->rt_data.l[dir]);
						printf("  add edge: %ld %ld\n", NSDATA_IDX(ns), rl->rt_data.l[dir]);
						found++;
					}
				}
				if (found == 0) {
					printf(" ERROR: failed to escape netseg %s: no available grid lines\n", ns->hdr.oid);
					NETDATA_FAILED_SET(net);
					NETDATA_FAILDIR_SET(net, dir);
				}
			}
		}
	}

	hpkp_solve(&h);

	printf("Matches:\n");
	{
		int n;
		for(n = 0; n < hvctx->escape[dir].num_ns; n++)
			printf(" %d %d\n", n, h.match[n]);
	}


	for(e = htsp_first(&ctx->board->nets); e != NULL; e = htsp_next(&ctx->board->nets, e)) {
		rtrnd_net_t *net = e->value;
		
		/* care about nets with the right escape direction only */
		if (ESCAPE_DIR(NETDATA_IS_MAJOR_X(net), NETDATA_DIR(net)) != dir)
			continue;

		if (net->segments.length > 1) {
			rtrnd_netseg_t *ns;
			for(ns = gdl_first(&net->segments); ns != NULL; ns = gdl_next(&net->segments, ns)) {
				int nsidx = NSDATA_IDX(ns);
				if (h.match[nsidx] != -1) {
					int i;
					escape_seg_t *eseg = NSDATA_ESEG(ns);
					void **rl_ = vtp0_get(&hvctx->escape[dir].ralines, h.match[nsidx] - hvctx->escape[dir].num_ns, 0);
					rtrnd_raline_t *rl = NULL;
					assert(rl_ != NULL);
					rl = *rl_;
					assert(h.match[nsidx] == rl->rt_data.l[dir]);

					assert(NSDATA_IDX(ns) == nsidx);
					for(i = 0; i < eseg->len; i++) {
						if (rl == eseg->poss[i].rl) {
							eseg->chosen = i;
							if (adjust_dirpol != 0)
								escape_adjust_avail(ctx, hvctx, is_major_x, adjust_dirpol, rl->major, eseg->poss[i].bus_minor, eseg->poss[i].mid_minor);
							break;
						}
					}
					printf(" %d:%ld on major=%f chosen=%p/%d\n", nsidx, h.match[nsidx] - hvctx->escape[dir].num_ns, rl->major, eseg, eseg->chosen);
					assert(eseg->chosen != -1);
				}
				else {
					printf(" ERROR: no escape grid match for %s\n", ns->hdr.oid);
					bad++;
				}
			}
		}
	}

	hpkp_uninit(&h);
	return bad;
}

long escape(rtrnd_t *ctx, horver_t *hvctx)
{
	long bad;

	escape_init(ctx, hvctx);

	bad  = escape_calc(ctx, hvctx, 0, +1, -1);
	bad += escape_calc(ctx, hvctx, 0, -1, 0);
	bad += escape_calc(ctx, hvctx, 1, +1, -1);
	bad += escape_calc(ctx, hvctx, 1, -1, 0);

	return bad;
}

