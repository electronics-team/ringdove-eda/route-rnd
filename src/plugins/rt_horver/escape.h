/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  router: escape wires for simple grid based horizontal/vertical routing
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#ifndef RT_HORVER_ESCAPE_H
#define RT_HORVER_ESCAPE_H

#define RT_HORVER_ONLY_TYPEDEF 1
#include "rt_horver.h"
#undef RT_HORVER_ONLY_TYPEDEF

typedef int escape_dir_t;
#define ESCAPE_DIR(is_major_x, dir) (((!!(is_major_x)) << 1) | (dir > 0))


typedef struct { /* possible escape in a given direction */
	rtrnd_raline_t *rl;
	double bus_minor, mid_minor, obj_minor, obj_major;
	unsigned avail:1; /* whether to use this line as escape */
} escape_poss_t;

typedef struct {
	int ex, ey;   /* escape direction */

	/* possible escapes */
	int chosen; /* index of the escape currently used */
	int len;    /* length of poss (number of possible escapes) */
	escape_poss_t *poss; /*possible escapes */

	int coll_cnt; /* collision counter: how many times we had to sacrifice another escape_seg_t to get this one escaped */
} escape_seg_t;

typedef struct {              /* all escape states in a specific one direction: */
	vtp0_t ralines;             /* a raline-id-ordered list of all grid lines used in a direction for the graph pairing */
	long next_raline_id;        /* for escape_grid's id allocation */
	long num_ns;                /* how many net segments we have on each direction */
} escape_t;

long escape(rtrnd_t *ctx, horver_t *hvctx);
void escape_draw(rtrnd_t *ctx, horver_t *hvctx, int is_major_x, int dirpol, rtrnd_netseg_t *ns, rtrnd_raline_t *rl);
void escape_uninit(rtrnd_t *ctx, horver_t *hvctx);


#endif
