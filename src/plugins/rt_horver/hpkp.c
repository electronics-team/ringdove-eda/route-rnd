#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "hpkp.h"

#define INVALID ((1L<<31)-2)
#define END -1

void hpkp_init(hpkp_t *ctx, int nodes, int max_edges)
{
	assert(nodes > 0);
	assert(max_edges > 0);

	ctx->num_nodes = nodes;
	ctx->num_edges = 0;
	ctx->max_edges = max_edges;

	nodes++;
	ctx->estart = malloc(sizeof(int) * nodes);
	ctx->match = calloc(sizeof(int), nodes);
	ctx->cost = malloc(sizeof(int) * nodes);
	ctx->bp = malloc(sizeof(int) * nodes);
	ctx->enext = malloc(sizeof(int) * ctx->max_edges);
	ctx->edge_v = malloc(sizeof(int) * ctx->max_edges);
	memset(ctx->estart, END, sizeof(int) * nodes);
}

void hpkp_uninit(hpkp_t *ctx)
{
	free(ctx->estart);
	free(ctx->match);
	free(ctx->cost);
	free(ctx->bp);
	free(ctx->enext);
	free(ctx->edge_v);
}

/* Adding edge between u and v */
void hpkp_add_edge(hpkp_t *ctx, int u, int v)
{
	assert(u < ctx->num_nodes);
	assert(v < ctx->num_nodes);
	u++;v++; /* internal indexing is 1-based, API is 0-based */
	ctx->enext[ctx->num_edges] = ctx->estart[u];
	ctx->estart[u] = ctx->num_edges;
	ctx->edge_v[ctx->num_edges] = v;
	ctx->num_edges++;
}

static int hpkp_bfs(hpkp_t *ctx)
{
	int u, v, n, qh = 0, qt = 0;
	for(n = 1; n <= ctx->num_nodes; n++) {
		if (!ctx->match[n]) {
			ctx->cost[n] = 0;
			ctx->bp[qt] = n;
			qt++;
		}
		else
			ctx->cost[n] = INVALID;
	}
	ctx->cost[0] = INVALID;
	while(qh < qt) {
		u = ctx->bp[qh];
		qh++;
		if (u != 0) {
			for(n = ctx->estart[u]; n != END; n = ctx->enext[n]) {
				v = ctx->edge_v[n];
				if (ctx->cost[ctx->match[v]] == INVALID) {
					ctx->cost[ctx->match[v]] = ctx->cost[u] + 1;
					ctx->bp[qt] = ctx->match[v];
					qt++;
				}
			}
		}
	}
	return ctx->cost[0] != INVALID;
}

static int hpkp_dfs(hpkp_t *ctx, int u)
{
	int v, n;

	if (u != 0) {
		for(n = ctx->estart[u]; n != END; n = ctx->enext[n]) {
			v = ctx->edge_v[n];
			if (ctx->cost[ctx->match[v]] == ctx->cost[u] + 1) {
				if (hpkp_dfs(ctx, ctx->match[v])) {
					ctx->match[v] = u;
					ctx->match[u] = v;
					assert(u <= ctx->num_nodes);
					assert(v <= ctx->num_nodes);
					return 1;
				}
			}
		}
		ctx->cost[u] = INVALID;
		return 0;
	}
	return 1;
}

int hpkp_solve(hpkp_t *ctx)
{
	int n, matching = 0;
	while(hpkp_bfs(ctx))
		for(n = 1; n <= ctx->num_nodes; n++)
			if (!ctx->match[n] && hpkp_dfs(ctx, n))
				matching++;

	/* internal indexing is 1-based, API is 0-based */
	for(n = 1; n <= ctx->num_nodes; n++)
		ctx->match[n-1] = ctx->match[n]-1;
	return matching;
}
