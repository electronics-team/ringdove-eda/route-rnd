/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  rats nest (shortest pairwise netseg connection)
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "route-rnd.h"
#include "data.h"
#include <gengeo2d/box.h>
#include "util_rat.h"

#define SQR(a) ((a) * (a))

RTRND_INLINE int cfg_compatible(rtrnd_any_obj_t *o1, rtrnd_ratsnest_cfg_t cfg)
{
	if ((cfg & RTRND_RNS_NO_ARC) && (o1->hdr.type == RTRND_ARC))
		return 0;

	if ((cfg & RTRND_RNS_NO_DIAG) && (o1->hdr.type == RTRND_LINE)) {
		if ((o1->line.cline.p1.x != o1->line.cline.p2.x) && (o1->line.cline.p1.y != o1->line.cline.p2.y))
			return 0;
	}

	return 1;
}

RTRND_INLINE void obj_pt_next(rtrnd_any_obj_t *obj, long *idx, double *xo, double *yo)
{
	switch(obj->hdr.type) {
		case RTRND_LINE:
			(*idx)++;
			if (*idx == 2)
				break;
			if (*idx == 0) {
				*xo = obj->line.cline.p1.x;
				*yo = obj->line.cline.p1.y;
			}
			else {
				*xo = obj->line.cline.p2.x;
				*yo = obj->line.cline.p2.y;
			}
			return;
		case RTRND_VIA:
			(*idx)++;
			if (*idx == 0) {
				*xo = obj->via.x;
				*yo = obj->via.y;
				return;
			}
			break;
		default:
			;
	}
			*idx = -1;
}

RTRND_INLINE double find_rat(rtrnd_rat_t *dst, rtrnd_netseg_t *ns1, rtrnd_netseg_t *ns2, rtrnd_ratsnest_cfg_t cfg)
{
	rtrnd_any_obj_t *o1, *o2;


	dst->cost = HUGE_VAL;

	for(o1 = gdl_first(&ns1->objs); o1 != NULL; o1 = gdl_next(&ns1->objs, o1)) {
		if (!cfg_compatible(o1, cfg)) continue;
		for(o2 = gdl_first(&ns2->objs); o2 != NULL; o2 = gdl_next(&ns2->objs, o2)) {
			long i1, i2;
			double x[2], y[2];
			if ((o1 == o2) || !cfg_compatible(o2, cfg)) continue;
			for(i1 = -1, obj_pt_next(o1, &i1, &x[0], &y[0]); i1 >= 0; obj_pt_next(o1, &i1, &x[0], &y[0])) {
				for(i2 = -1, obj_pt_next(o2, &i2, &x[1], &y[1]); i2 >= 0; obj_pt_next(o2, &i2, &x[1], &y[1])) {
					double cost = SQR(x[0] - x[1]) + SQR(y[0] - y[1]);
					if (cost < dst->cost) {
						dst->x[0] = x[0]; dst->x[1] = x[1];
						dst->y[0] = y[0]; dst->y[1] = y[1];
						dst->o[0] = o1; dst->o[1] = o2;
						dst->cost = cost;
					}
				}
			}
		}
	}
	return dst->cost;
}

RTRND_INLINE void dup_append_rat(rtrnd_ratsnest_t *dst, const rtrnd_rat_t *src)
{
	rtrnd_rat_t *r = malloc(sizeof(rtrnd_rat_t));
	memcpy(r, src, sizeof(rtrnd_rat_t));
	memset(&r->link, 0, sizeof(r->link));
	gdl_append(&dst->lst, r, link);
}



#define mxi(i1, i2, len) \
	(((i1) < (i2)) ? ((i1)*(len)+(i2)) : ((i2)*(len)+(i1)))

void rtrnd_ratsnest_map(rtrnd_ratsnest_t *nest, rtrnd_board_t *board, rtrnd_ratsnest_cfg_t cfg)
{
	htsp_entry_t *e;
	rtrnd_rat_t *dist = NULL;
	int *comp = NULL;
	long max_size = 0;
	nest->cfg = cfg;
	rtrnd_netseg_t **ns;

	/* allocation for the worst case (biggest net) */
	for(e = htsp_first(&board->nets); e != NULL; e = htsp_next(&board->nets, e)) {
		rtrnd_net_t *net = e->value;
		if (net->segments.length > max_size)
			max_size = net->segments.length;
	}

	if (max_size <= 1)
		return;

	dist = malloc(max_size * max_size * sizeof(rtrnd_rat_t));
	comp = malloc(max_size * sizeof(int));
	ns = malloc(max_size * sizeof(rtrnd_netseg_t *));

	/* minimal spanning tree for each net */
	for(e = htsp_first(&board->nets); e != NULL; e = htsp_next(&board->nets, e)) {
		rtrnd_net_t *net = e->value;
		long i1, i2, n, len, from, to;
		rtrnd_netseg_t *nseg;

		if (net->segments.length < 1) /* skip nets already completed */
			continue;

		len = net->segments.length;

		/* set up the distance matrix and component tracking */
		for(n = 0, nseg = gdl_first(&net->segments); nseg != NULL; nseg = gdl_next(&net->segments, nseg), n++) {
			ns[n] = nseg;
			comp[n] = n;
		}
		for(i1 = 0; i1 < len; i1++)
			for(i2 = i1+1; i2 < len; i2++)
				find_rat(&dist[mxi(i1, i2, len)], ns[i1], ns[i2], cfg);

		/* greedy algorithm: realize the shortest possible connection and 'merge'
		   the components, until all components are gone */
/*printf("net=%s\n", net->hdr.oid);*/
		for(;;) {
			int besti1, besti2;
			double best_cost = HUGE_VAL;
/*printf(" outer\n");*/
			for(i1 = 0; i1 < len; i1++) {
				for(i2 = i1+1; i2 < len; i2++) {
					if (comp[i1] == comp[i2]) continue;
					if (dist[mxi(i1, i2, len)].cost < best_cost) {
						besti1 = i1; besti2 = i2;
						best_cost = dist[mxi(i1, i2, len)].cost;
					}
				}
			}
			if (best_cost == HUGE_VAL)
				break;
			i1 = besti1;
			i2 = besti2;
/*printf("  connect #%ld(%d) #%ld(%d)\n", i1, comp[i1], i2, comp[i2]);*/
			dup_append_rat(nest, &dist[mxi(i1, i2, len)]);
			dist[mxi(i1, i2, len)].cost = HUGE_VAL;
			from = comp[i2];
			to = comp[i1];
			for(n = 0; n < len; n++)
				if (comp[n] == from)
					comp[n] = to;
		}


#if 0
printf(" done net:");
			for(n = 0; n < len; n++)
				printf(" %d", comp[n]);
printf("\n");
#endif

	}
	free(dist);
	free(comp);
	free(ns);
}

void rtrnd_ratsnest_draw(rtrnd_ratsnest_t *nest, rtrnd_layer_t *ly)
{
	rtrnd_rat_t *r;
	for(r = gdl_first(&nest->lst); r != NULL; r = gdl_next(&nest->lst, r))
		rtrnd_line_new(ly, NULL, r->o[0]->hdr.net, r->x[0], r->y[0], r->x[1], r->y[1], 0.1, 0);
}


void rtrnd_ratsnest_uninit(rtrnd_ratsnest_t *nest)
{
	rtrnd_rat_t *r;
	while((r = gdl_first(&nest->lst)) != NULL) {
		gdl_remove(&nest->lst, r, link);
		free(r);
	}
}

