#ifndef RTRND_CONF_H
#define RTRND_CONF_H

#include <stddef.h>

typedef enum {
	RTRND_CT_TERMINATOR,
	RTRND_CT_BOOLEAN,
	RTRND_CT_INTEGER,
	RTRND_CT_DOUBLE,
	RTRND_CT_COORD,
	RTRND_CT_STRING
} rtrnd_conf_type_t;

#define RTRND_CT_INVALID RTRND_CT_TERMINATOR
#define RTRND_CONF_NOVAL HUGE_VAL

typedef struct {
	const char *name;
	rtrnd_conf_type_t type;
	union {
		double dval;
		const char *sval;
	} defval;
	double min, max;
	const char *desc;
	union {
		void *any;
		int *b;
		long *i;
		double *d;
		char *s;
	} data;
} rtrnd_conf_t;

#define RTRND_CONF_BOOLEAN(name, defval, desc, ptr) \
	{name, RTRND_CT_BOOLEAN, {defval}, RTRND_CONF_NOVAL, RTRND_CONF_NOVAL, desc, {ptr}},

#define RTRND_CONF_INTEGER(name, defval, min, max, desc, ptr) \
	{name, RTRND_CT_INTEGER, {defval}, min, max, desc, {ptr}},

#define RTRND_CONF_DOUBLE(name, defval, min, max, desc, ptr) \
	{name, RTRND_CT_DOUBLE, {defval}, min, max, desc, {ptr}},

#define RTRND_CONF_COORD(name, defval, min, max, desc, ptr) \
	{name, RTRND_CT_COORD, {defval}, min, max, desc, {ptr}},

#define RTRND_CONF_STRING(name, defval, desc, ptr) \
	{name, RTRND_CT_STRING, {0, defval}, RTRND_CONF_NOVAL, RTRND_CONF_NOVAL, desc, {ptr}},

#define RTRND_CONF_TERMINATE \
	{NULL, RTRND_CT_TERMINATOR, {0}, RTRND_CONF_NOVAL, RTRND_CONF_NOVAL, NULL, {NULL}}

const char *rtrnd_conf_type2name(rtrnd_conf_type_t type);

int rtrnd_conf_set(rtrnd_conf_t *table, const char *key, const char *val);

/* Load the config values behind a table with the default values */
int rtrnd_conf_defaults(rtrnd_conf_t *table);

#endif
