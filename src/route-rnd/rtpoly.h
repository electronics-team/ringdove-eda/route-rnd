/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  rtree based, hole-free polygons
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */


#ifndef RTPOLY_H
#define RTPOLY_H

#include <genlist/gendlist.h>
#include "rtree.h"
#include "geo.h"

typedef struct rtp_vertex_s {
	rtrnd_rtree_box_t bbox;
	double x, y;
	gdl_elem_t link;
} rtp_vertex_t;

typedef struct {
	g2d_box_t bbox;
	gdl_list_t lst;      /* of rtp_vertex_t */
	rtrnd_rtree_t tree;  /* of rtp_vertex_t */
	double area;
	unsigned valid:1;
} rtpoly_t;

void rtpoly_new_begin(rtpoly_t *poly);
void rtpoly_new_append(rtpoly_t *poly, double x, double y);
int rtpoly_new_end(rtpoly_t *poly);
void rtpoly_free(rtpoly_t *poly);


/* returns non-zero if x;y intersects with poly */
int rtpoly_isc_point(const rtpoly_t *poly, double x, double y);

/* returns non-zero if a circle of radius r at x;y intersects with poly */
int rtpoly_isc_circle(const rtpoly_t *poly, double x, double y, double r);

/* returns non-zero if a cline intersects with poly */
int rtpoly_isc_cline(const rtpoly_t *poly, const g2d_cline_t *cline);

/* returns non-zero if two rtpolys intersect */
int rtpoly_isc_rtpoly(const rtpoly_t *p1, const rtpoly_t *p2);

#endif
