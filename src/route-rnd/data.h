/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  data model
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#ifndef RTRND_DATA_H
#define RTRND_DATA_H

#include <genvector/vtp0.h>
#include <genlist/gendlist.h>
#include <genht/htsp.h>
#include "route-rnd.h"
#include "rtree.h"
#include "geo.h"
#include "rtpoly.h"

typedef enum {
	rtrnd_false = 0,
	rtrnd_true = 1
} rtrnd_bool_t;

typedef enum {
	RTRND_LINE  = 0x000001,
	RTRND_ARC   = 0x000002,
	RTRND_POLY  = 0x000004,
	RTRND_VIA   = 0x000008,
	RTRND_TEXT  = 0x000100,
	RTRND_BOARD = 0x010000,
	RTRND_LAYER = 0x020000,
	RTRND_NET   = 0x040000,
	RTRND_NETSEG= 0x080000
} rtrnd_obj_type_t;

typedef struct rtrnd_net_s rtrnd_net_t;
typedef struct rtrnd_netseg_s rtrnd_netseg_t;

typedef struct {
		long l[4];
		void *p[4];
} rtrnd_udata_t; /* used/caller data */


typedef struct rtrnd_hdr_s rtrnd_hdr_t;
struct rtrnd_hdr_s {
	rtrnd_rtree_box_t bbox;
	rtrnd_obj_type_t type;
	char *oid;
	rtrnd_any_obj_t *parent;

	unsigned long mark;

	rtrnd_udata_t rt_data;

	/* for layer objects: */
	rtrnd_net_t *net;
	rtrnd_netseg_t *netseg;
	gdl_elem_t net_link;
	gdl_elem_t netseg_link;
	unsigned cnst_move:1; /* constraint: move */
	unsigned cnst_del:1;  /* constraint: del */
	unsigned terminal:1;  /* object is a terminal (pin or pad) */
	unsigned created:1;   /* created by the autorouter */
};

typedef struct rtrnd_line_s {
	rtrnd_hdr_t hdr;
	g2d_cline_t cline;
	double thickness, clearance;
} rtrnd_line_t;

typedef struct rtrnd_text_s {
	rtrnd_hdr_t hdr;
	/* text string is hdr.oid */
	double x, y, size;
} rtrnd_text_t;

typedef struct rtrnd_arc_s {
	rtrnd_hdr_t hdr;
	g2d_carc_t carc;
	double thickness, clearance;
} rtrnd_arc_t;

typedef struct rtrnd_poly_s {
	rtrnd_hdr_t hdr;
	rtpoly_t rtpoly; /* transformed coords */
} rtrnd_poly_t;

typedef struct rtrnd_via_s {
	rtrnd_hdr_t hdr;
	double x, y, dia, clearance;
} rtrnd_via_t;

struct rtrnd_board_s {
	rtrnd_hdr_t hdr;
	vtp0_t layers; /* ordered from top to bottom */
	htsp_t nets;
	gdl_list_t orphaned_segments;
	rtrnd_rtree_t vias;
	const char *mask_names[32];
};

typedef enum {
	RTRND_LLOC_TOP,
	RTRND_LLOC_INTERN,
	RTRND_LLOC_BOTTOM
} rtrnd_layer_loc_t;

struct rtrnd_layer_s {
	rtrnd_hdr_t hdr;
	char *name;
	char color[8];
	rtrnd_rtree_t objs;
	rtrnd_layer_loc_t loc;
};

struct rtrnd_net_s {
	rtrnd_hdr_t hdr;
	gdl_list_t objs; /* point to each object that is in the net */
	gdl_list_t segments;
};

struct rtrnd_netseg_s {
	rtrnd_hdr_t hdr; /* parent is unused */
	rtrnd_board_t *brd;
	rtrnd_net_t *net; /* only if there is a single net in the segment */
	gdl_list_t objs; /* point to each object that is in the net segment */
	gdl_elem_t link; /* link within the an rtrnd_net_t or in brd's orphaned net segments list */
	unsigned shorted:1; /* 1 if there are objects of multiple nets connected in this segment */
};

union rtrnd_any_obj_s {
	rtrnd_hdr_t hdr;
	rtrnd_line_t line;
	rtrnd_text_t text;
	rtrnd_arc_t arc;
	rtrnd_poly_t poly;
	rtrnd_via_t via;
	rtrnd_board_t board;
	rtrnd_layer_t layer;
};

rtrnd_line_t *rtrnd_line_new(rtrnd_layer_t *layer, const char *oid, rtrnd_net_t *net, double x1, double y1, double x2, double y2, double thick, double clr);
rtrnd_arc_t *rtrnd_arc_new(rtrnd_layer_t *layer, const char *oid, rtrnd_net_t *net, double cx, double cy, double r, double start, double delta, double thick, double clr);
rtrnd_via_t *rtrnd_via_new(rtrnd_board_t *brd, const char *oid, rtrnd_net_t *net, double x, double y, double dia, double clr);
rtrnd_poly_t *rtrnd_poly_new_from_xys(rtrnd_layer_t *layer, const char *oid, rtrnd_net_t *net, double *xy, long xylen, double ox, double oy);

/* only for debug annotations: */
rtrnd_text_t *rtrnd_text_new(rtrnd_layer_t *layer, double x, double y, const char *txt, double size);


rtrnd_via_t *rtrnd_via_alloc(const char *oid, double x, double y, double dia, double clr);
rtrnd_via_t *rtrnd_via_reg(rtrnd_board_t *brd, rtrnd_net_t *net, rtrnd_via_t *via);


void rtrnd_layer_init(rtrnd_layer_t *layer, const char *name);
void rtrnd_layer_uninit(rtrnd_layer_t *layer);
rtrnd_board_t *rtrnd_board_new(void);
void rtrnd_board_free(rtrnd_board_t *brd);
void rtrnd_board_bbox(g2d_box_t *dst, rtrnd_board_t *board);

int rtrnd_obj_center(rtrnd_any_obj_t *obj, double *cx, double *cy);

/* returns whether a via touches (crosses or ends on) a layer; thru-hole
   vias will always return 1, bbvias depend on span */
int rtrnd_via_touches_layer(rtrnd_layer_t *layer, rtrnd_via_t *via);

/* Create a new annotation layer at the end (on top in rendering) */
rtrnd_layer_t *rtrnd_annot_new(rtrnd_t *ctx, const char *name);

unsigned long rtrnd_mark_alloc(rtrnd_board_t *brd, const char *name);
void rtrnd_mark_free(rtrnd_board_t *brd, unsigned long mask);
void rtrnd_mark_clear(rtrnd_board_t *brd, unsigned long mask);

#define RTRND_DEG2RAD(a)  ((double)(a) / 180.0 * M_PI)
#define RTRND_RAD2DEG(a)  ((double)(a) / M_PI * 180.0)

/* Return the x;y coordinates on an arc's circle for a given angle */
RTRND_INLINE void rtrnd_arc_xy(rtrnd_arc_t *arc, double ang, double *x, double *y)
{
	*x = arc->carc.c.x + (arc->carc.r * cos(ang));
	*y = arc->carc.c.y + (arc->carc.r * sin(ang));
}

#endif
