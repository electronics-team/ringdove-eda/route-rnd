#ifndef RTRND_RTREE_H
#define RTRND_RTREE_H

#define RTR(n)  rtrnd_rtree_ ## n
#define RTRU(n) RTRND_RTREE_ ## n
typedef long int rtrnd_rtree_cardinal_t;
typedef double rtrnd_rtree_coord_t;
#define rtrnd_rtree_privfunc static
#define rtrnd_rtree_size 6
#define rtrnd_rtree_stack_max 1024

#include <genrtree/genrtree_api.h>

#ifndef RTRND_RTREE_KEEP_RTR
#undef RTR
#undef RTRU
#endif

#endif
