/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  data mode handling
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"
#include <assert.h>
#include "data.h"
#include "geo.h"
#include "compat_misc.h"
#include "netseg.h"
#include <gengeo2d/cline.h>
#include <gengeo2d/carc.h>
#include <genht/hash.h>
#include <genht/ht_utils.h>

static void box_geo2rtree(rtrnd_rtree_box_t *dst, const g2d_box_t *src)
{
	dst->x1 = src->p1.x; dst->y1 = src->p1.y;
	dst->x2 = src->p2.x; dst->y2 = src->p2.y;
}

static void rtrnd_obj_reg_ly(rtrnd_layer_t *layer, rtrnd_any_obj_t *obj)
{
	rtrnd_rtree_insert(&layer->objs, obj, &obj->hdr.bbox);
	obj->hdr.parent = (rtrnd_any_obj_t *)layer;
}

static void rtrnd_obj_unreg_ly(rtrnd_layer_t *layer, rtrnd_any_obj_t *obj)
{
	rtrnd_rtree_delete(&layer->objs, obj, &obj->hdr.bbox);
	obj->hdr.parent = NULL;
}

static void rtrnd_obj_reg_via(rtrnd_board_t *brd, rtrnd_via_t *via)
{
	rtrnd_rtree_insert(&brd->vias, via, &via->hdr.bbox);
	via->hdr.parent = (rtrnd_any_obj_t *)brd;
}

static void rtrnd_obj_unreg_via(rtrnd_board_t *brd, rtrnd_via_t *via)
{
	rtrnd_rtree_delete(&brd->vias, via, &via->hdr.bbox);
	via->hdr.parent = NULL;
}

static void rtrnd_obj_reg_net(rtrnd_net_t *net, rtrnd_any_obj_t *obj)
{
	assert(obj->hdr.net == NULL);
	if (net != NULL) {
		obj->hdr.net = net;
		gdl_append(&net->objs, obj, hdr.net_link);
	}
}

static void rtrnd_obj_unreg_net(rtrnd_net_t *net, rtrnd_any_obj_t *obj)
{
	assert(obj->hdr.net == net);
	if (net != NULL) {
		obj->hdr.net = NULL;
		gdl_remove(&net->objs, obj, hdr.net_link);
	}
}

rtrnd_line_t *rtrnd_line_new(rtrnd_layer_t *layer, const char *oid, rtrnd_net_t *net, double x1, double y1, double x2, double y2, double thick, double clr)
{
	double t2 = thick/2;
	rtrnd_line_t *line = calloc(sizeof(rtrnd_line_t), 1);
	g2d_box_t b;

	line->hdr.type = RTRND_LINE;
	line->hdr.oid = rnd_strdup_safe(oid);
	line->cline.p1.x = x1; line->cline.p1.y = y1;
	line->cline.p2.x = x2; line->cline.p2.y = y2;
	line->thickness = thick;
	line->clearance = clr;
	b = g2d_cline_bbox(&line->cline);
	b.p1.x -= t2; b.p1.y -= t2;
	b.p2.x += t2; b.p2.y += t2;
	box_geo2rtree(&line->hdr.bbox, &b);

	rtrnd_obj_reg_ly(layer, (rtrnd_any_obj_t *)line);
	rtrnd_obj_reg_net(net, (rtrnd_any_obj_t *)line);

	return line;
}

void rtrnd_line_free(rtrnd_line_t *line)
{
	free(line->hdr.oid);
	rtrnd_obj_unreg_ly(&line->hdr.parent->layer, (rtrnd_any_obj_t *)line);
	rtrnd_obj_unreg_net(line->hdr.net, (rtrnd_any_obj_t *)line);
	free(line);
}


rtrnd_arc_t *rtrnd_arc_new(rtrnd_layer_t *layer, const char *oid, rtrnd_net_t *net, double cx, double cy, double r, double start, double delta, double thick, double clr)
{
	double t2 = thick/2;
	rtrnd_arc_t *arc = calloc(sizeof(rtrnd_arc_t), 1);
	g2d_box_t b;

	arc->hdr.type = RTRND_ARC;
	arc->hdr.oid = rnd_strdup_safe(oid);
	arc->carc.c.x = cx; arc->carc.c.y = cy;
	arc->carc.r = r;
	arc->carc.start = start;
	arc->carc.delta = delta;
	arc->thickness = thick;
	arc->clearance = clr;
	b = g2d_carc_bbox(&arc->carc);
	b.p1.x -= t2; b.p1.y -= t2;
	b.p2.x += t2; b.p2.y += t2;
	box_geo2rtree(&arc->hdr.bbox, &b);

	rtrnd_obj_reg_ly(layer, (rtrnd_any_obj_t *)arc);
	rtrnd_obj_reg_net(net, (rtrnd_any_obj_t *)arc);

	return arc;
}

void rtrnd_arc_free(rtrnd_arc_t *arc)
{
	free(arc->hdr.oid);
	rtrnd_obj_unreg_ly(&arc->hdr.parent->layer, (rtrnd_any_obj_t *)arc);
	rtrnd_obj_unreg_net(arc->hdr.net, (rtrnd_any_obj_t *)arc);
	free(arc);
}

rtrnd_text_t *rtrnd_text_new(rtrnd_layer_t *layer, double x, double y, const char *txt, double size)
{
	rtrnd_text_t *text = calloc(sizeof(rtrnd_text_t), 1);
	g2d_box_t b;

	text->hdr.type = RTRND_TEXT;
	text->hdr.oid = rnd_strdup_safe(txt);
	text->x = x; text->y = y; text->size = size;
	b.p1.x = x-size; b.p1.y = y-size;
	b.p2.x = x+size; b.p2.y = y+size;
	box_geo2rtree(&text->hdr.bbox, &b);

	rtrnd_obj_reg_ly(layer, (rtrnd_any_obj_t *)text);

	return text;
}

void rtrnd_text_free(rtrnd_text_t *text)
{
	free(text->hdr.oid);
	rtrnd_obj_unreg_ly(&text->hdr.parent->layer, (rtrnd_any_obj_t *)text);
	free(text);
}


rtrnd_poly_t *rtrnd_poly_new_from_xys(rtrnd_layer_t *layer, const char *oid, rtrnd_net_t *net, double *xy, long xylen, double ox, double oy)
{
	rtrnd_poly_t *poly;
	long i;

	if ((xylen % 2) != 0)
		return NULL;

	poly = calloc(sizeof(rtrnd_poly_t), 1);
	poly->hdr.type = RTRND_POLY;
	poly->hdr.oid = rnd_strdup_safe(oid);

	rtpoly_new_begin(&poly->rtpoly);
	for(i = 0; i < xylen; i+=2)
		rtpoly_new_append(&poly->rtpoly, xy[i+0] + ox, xy[i+1] + oy);
	rtpoly_new_end(&poly->rtpoly);

	box_geo2rtree(&poly->hdr.bbox, &poly->rtpoly.bbox);

	rtrnd_obj_reg_ly(layer, (rtrnd_any_obj_t *)poly);
	rtrnd_obj_reg_net(net, (rtrnd_any_obj_t *)poly);

	return poly;
}

void rtrnd_poly_free(rtrnd_poly_t *poly)
{
	free(poly->hdr.oid);
	rtpoly_free(&poly->rtpoly);
	rtrnd_obj_unreg_ly(&poly->hdr.parent->layer, (rtrnd_any_obj_t *)poly);
	rtrnd_obj_unreg_net(poly->hdr.net, (rtrnd_any_obj_t *)poly);
	free(poly);
}

rtrnd_via_t *rtrnd_via_alloc(const char *oid, double x, double y, double dia, double clr)
{
	rtrnd_via_t *via = calloc(sizeof(rtrnd_via_t), 1);
	g2d_box_t b;

	via->hdr.type = RTRND_VIA;
	via->hdr.oid = rnd_strdup_safe(oid);
	via->x = x; via->y = y;
	via->dia = dia;
	via->clearance = clr;
	b.p1.x = x - dia/2; b.p1.y = y - dia/2;
	b.p2.x = x + dia/2; b.p2.y = y + dia/2;
	box_geo2rtree(&via->hdr.bbox, &b);

	return via;
}

rtrnd_via_t *rtrnd_via_reg(rtrnd_board_t *brd, rtrnd_net_t *net, rtrnd_via_t *via)
{
	rtrnd_obj_reg_via(brd, via);
	rtrnd_obj_reg_net(net, (rtrnd_any_obj_t *)via);
	return via;
}

rtrnd_via_t *rtrnd_via_new(rtrnd_board_t *brd, const char *oid, rtrnd_net_t *net, double x, double y, double dia, double clr)
{
	return rtrnd_via_reg(brd, net, rtrnd_via_alloc(oid, x, y, dia, clr));
}


void rtrnd_via_free(rtrnd_board_t *brd, rtrnd_via_t *via)
{
	rtrnd_obj_unreg_via(brd, via);
	rtrnd_obj_unreg_net(via->hdr.net, (rtrnd_any_obj_t *)via);
	free(via->hdr.oid);
	free(via);
}

void rtrnd_layer_init(rtrnd_layer_t *layer, const char *name)
{
	rtrnd_rtree_init(&layer->objs);
	layer->name = rnd_strdup(name);
	layer->hdr.type = RTRND_LAYER;
}

void rtrnd_layer_uninit(rtrnd_layer_t *layer)
{
	rtrnd_rtree_it_t it;
	rtrnd_any_obj_t *obj;

	for(obj = rtrnd_rtree_all_first(&it, &layer->objs); obj != NULL; obj = rtrnd_rtree_all_first(&it, &layer->objs)) {
		switch(obj->hdr.type) {
			case RTRND_LINE: rtrnd_line_free(&obj->line); break;
			case RTRND_TEXT: rtrnd_text_free(&obj->text); break;
			case RTRND_POLY: rtrnd_poly_free(&obj->poly); break;
			case RTRND_ARC:  rtrnd_arc_free(&obj->arc); break;
			default: /* can't be on layer */
				break;
		}
	}

	rtrnd_rtree_uninit(&layer->objs);
	free(layer->name);
}


rtrnd_board_t *rtrnd_board_new(void)
{
	rtrnd_board_t *brd = calloc(sizeof(rtrnd_board_t), 1);
	rtrnd_rtree_init(&brd->vias);
	htsp_init(&brd->nets, strhash, strkeyeq);
	return brd;
}

static void rtrnd_net_uninit_segs(rtrnd_net_t *net)
{
	rtrnd_netseg_t *ns;

	for(ns = gdl_first(&net->segments); ns != NULL; ns = gdl_first(&net->segments))
		rtrnd_netseg_free(ns);
}

void rtrnd_board_free(rtrnd_board_t *brd)
{
	htsp_entry_t *e;
	rtrnd_netseg_t *ns;
	rtrnd_via_t *via;
	rtrnd_rtree_it_t it;
	int n;

	for(ns = gdl_first(&brd->orphaned_segments); ns != NULL; ns = gdl_first(&brd->orphaned_segments))
		rtrnd_netseg_free(ns);

	for(e = htsp_first(&brd->nets); e != NULL; e = htsp_next(&brd->nets, e))
		rtrnd_net_uninit_segs(e->value);


	for(n = 0; n < brd->layers.used; n++) {
		rtrnd_layer_uninit(brd->layers.array[n]);
		free(brd->layers.array[n]);
	}
	vtp0_uninit(&brd->layers);

	for(via = rtrnd_rtree_all_first(&it, &brd->vias); via != NULL; via = rtrnd_rtree_all_first(&it, &brd->vias))
		rtrnd_via_free(brd, via);

	genht_uninit_deep(htsp, &brd->nets, {
		free(htent->key);
		free(htent->value);
	});

	free(brd);
}


void rtrnd_board_bbox(g2d_box_t *dst, rtrnd_board_t *board)
{
	g2d_box_t b = g2d_box_invalid();
	rtrnd_via_t *via;
	rtrnd_rtree_it_t it;
	long n;

	for(n = 0; n < board->layers.used; n++) {
		rtrnd_layer_t *layer = board->layers.array[n];
		g2d_vect_t pt;
		pt.x = layer->objs.bbox.x1; pt.y = layer->objs.bbox.y1; g2d_box_bump_pt(&b, pt);
		pt.x = layer->objs.bbox.x2; pt.y = layer->objs.bbox.y2; g2d_box_bump_pt(&b, pt);
	}

	/* just in case some vias are beyond layer objects */
	for(via = rtrnd_rtree_all_first(&it, &board->vias); via != NULL; via = rtrnd_rtree_all_next(&it)) {
		g2d_vect_t pt;
		pt.x = via->hdr.bbox.x1; pt.y = via->hdr.bbox.y1; g2d_box_bump_pt(&b, pt);
		pt.x = via->hdr.bbox.x2; pt.y = via->hdr.bbox.y2; g2d_box_bump_pt(&b, pt);
	}

	*dst = b;
}


int rtrnd_via_touches_layer(rtrnd_layer_t *layer, rtrnd_via_t *via)
{
	return 1; /*bbvia: thru-hole vias only at the moment */
}

int rtrnd_obj_center(rtrnd_any_obj_t *obj, double *cx, double *cy)
{
	switch(obj->hdr.type) {
		case RTRND_LINE:
		case RTRND_POLY:
		case RTRND_VIA:
			*cx = (obj->hdr.bbox.x1 + obj->hdr.bbox.x2) / 2;
			*cy = (obj->hdr.bbox.y1 + obj->hdr.bbox.y2) / 2;
			return 0;
		case RTRND_ARC:
			/* TODO */
		case RTRND_TEXT:
		case RTRND_BOARD:
		case RTRND_LAYER:
		case RTRND_NET:
		case RTRND_NETSEG:
			return -1;
	}

	return -1;
}

rtrnd_layer_t *rtrnd_annot_new(rtrnd_t *ctx, const char *name)
{
	rtrnd_layer_t *layer = calloc(sizeof(rtrnd_layer_t), 1);
	rtrnd_layer_init(layer, name);
	vtp0_append(&ctx->annots, layer);
	return layer;
}

unsigned long rtrnd_mark_alloc(rtrnd_board_t *brd, const char *name)
{
	int n;
	for(n = 0; n < sizeof(brd->mask_names) / sizeof(brd->mask_names[0]); n++) {
		if (brd->mask_names[n] == NULL) {
			brd->mask_names[n] = name;
			return 1 << n;
		}
	}
	assert(!"ran out of mask bits");
	return 0;
}

void rtrnd_mark_free(rtrnd_board_t *brd, unsigned long mask)
{
	int n;
	for(n = 0; n < sizeof(brd->mask_names) / sizeof(brd->mask_names[0]); n++) {
		if ((1 << n) == mask) {
			brd->mask_names[n] = NULL;
			return;
		}
	}
}

void rtrnd_mark_clear(rtrnd_board_t *brd, unsigned long mask)
{
	rtrnd_any_obj_t *obj;
	rtrnd_rtree_it_t it;
	unsigned long clr = ~mask;
	int n;

	for(n = 0; n < brd->layers.used; n++) {
		rtrnd_layer_t *layer = brd->layers.array[n];
		for(obj = rtrnd_rtree_all_first(&it, &layer->objs); obj != NULL; obj = rtrnd_rtree_all_next(&it))
			obj->hdr.mark &= clr;
	}

	for(obj = rtrnd_rtree_all_first(&it, &brd->vias); obj != NULL; obj = rtrnd_rtree_all_next(&it))
		obj->hdr.mark &= clr;
}
