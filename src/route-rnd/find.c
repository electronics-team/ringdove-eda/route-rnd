/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  find galvanic connections
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"
#include <assert.h>
#include "data.h"
#include "rtpoly.h"
#include "geo.h"
#include "find.h"
#include <gengeo2d/cline.h>
#include <gengeo2d/sline.h>

#define LOAD_SLINE(sline, rline) \
do { \
	sline.c = rline->cline; \
	sline.s.cap = G2D_CAP_ROUND; \
	sline.s.width = rline->thickness; \
} while(0)

static int isc_line_line(rtrnd_find_t *ctx, rtrnd_line_t *l1, rtrnd_line_t *l2)
{
	g2d_sline_t sl1, sl2;
	LOAD_SLINE(sl1, l1);
	LOAD_SLINE(sl2, l2);
	sl1.s.width += 2 * ctx->bloat;
	return g2d_isc_sline_sline(&sl1, &sl2);
}

static int isc_line_arc(rtrnd_find_t *ctx, rtrnd_line_t *l, rtrnd_arc_t *a)
{
/*	g2d_sline_t sl;
	LOAD_SLINE(sl, l);*/
	return 0;
}

static int isc_line_poly(rtrnd_find_t *ctx, rtrnd_line_t *l, rtrnd_poly_t *p)
{
	rtpoly_t *rp = &p->rtpoly;
	g2d_sline_t sl;
	g2d_cline_t side1, side2;
	LOAD_SLINE(sl, l);

	g2d_sline_sides(&sl, &side1, &side2);
	sl.s.width += 2 * ctx->bloat;

	if (rtpoly_isc_cline(rp, &side1) || rtpoly_isc_cline(rp, &side2))
		return 1;

	if (rtpoly_isc_circle(rp, sl.c.p1.x, sl.c.p1.y, l->thickness/2))
		return 1;
	if (rtpoly_isc_circle(rp, sl.c.p2.x, sl.c.p2.y, l->thickness/2))
		return 1;

	return 0;
}

static int isc_line_via(rtrnd_find_t *ctx, rtrnd_line_t *l, rtrnd_via_t *v)
{
	g2d_sline_t sl;
	g2d_vect_t center;
	LOAD_SLINE(sl, l);
	center.x = v->x; center.y = v->y;
	return g2d_isc_sline_circle(&sl, center, v->dia/2 + ctx->bloat);
}

static int isc_arc_arc(rtrnd_find_t *ctx, rtrnd_arc_t *a1, rtrnd_arc_t *a2)
{
	return 0;
}

static int isc_arc_poly(rtrnd_find_t *ctx, rtrnd_arc_t *a, rtrnd_poly_t *p)
{
	return 0;
}

static int isc_arc_via(rtrnd_find_t *ctx, rtrnd_arc_t *a, rtrnd_via_t *v)
{
	return 0;
}

static int isc_poly_poly(rtrnd_find_t *ctx, rtrnd_poly_t *p1, rtrnd_poly_t *p2)
{
#warning TODO: this ignores ctx->bloat
	return rtpoly_isc_rtpoly(&p1->rtpoly, &p2->rtpoly);
}

static int isc_poly_via(rtrnd_find_t *ctx, rtrnd_poly_t *p, rtrnd_via_t *v)
{
	return rtpoly_isc_circle(&p->rtpoly, v->x, v->y, v->dia/2 + ctx->bloat);
}

static int isc_via_via(rtrnd_find_t *ctx, rtrnd_via_t *v1, rtrnd_via_t *v2)
{
	g2d_cvect_t c1, c2;
	double max_dist2;

	c1.x = v1->x; c1.y = v1->y;
	c2.x = v2->x; c2.y = v2->y;

	max_dist2 = (v1->dia + v2->dia)/2 + ctx->bloat;
	max_dist2 *= max_dist2;

	return (g2d__distance2(c1, c2) <= max_dist2);
}

/* calculate whether two objects intersect */
int rtrnd_isc_obj_obj(rtrnd_find_t *ctx, rtrnd_any_obj_t *o1, rtrnd_any_obj_t *o2)
{
	switch(o1->hdr.type) {
		case RTRND_LINE:
			switch(o2->hdr.type) {
				case RTRND_LINE:  return isc_line_line(ctx, &o1->line, &o2->line);
				case RTRND_ARC:   return isc_line_arc(ctx, &o1->line, &o2->arc);
				case RTRND_POLY:  return isc_line_poly(ctx, &o1->line, &o2->poly);
				case RTRND_VIA:   return isc_line_via(ctx, &o1->line, &o2->via);
				case RTRND_TEXT:
				case RTRND_BOARD: case RTRND_LAYER: case RTRND_NET: case RTRND_NETSEG:
					break;
			}
			break;
		case RTRND_ARC:
			switch(o2->hdr.type) {
				case RTRND_LINE:  return isc_line_arc(ctx, &o2->line, &o1->arc);
				case RTRND_ARC:   return isc_arc_arc(ctx, &o1->arc, &o2->arc);
				case RTRND_POLY:  return isc_arc_poly(ctx, &o1->arc, &o2->poly);
				case RTRND_VIA:   return isc_arc_via(ctx, &o1->arc, &o2->via);
				case RTRND_TEXT:
				case RTRND_BOARD: case RTRND_LAYER: case RTRND_NET: case RTRND_NETSEG:
					break;
			}
			break;
		case RTRND_POLY:
			switch(o2->hdr.type) {
				case RTRND_LINE:  return isc_line_poly(ctx, &o2->line, &o1->poly);
				case RTRND_ARC:   return isc_arc_poly(ctx, &o2->arc, &o1->poly);
				case RTRND_POLY:  return isc_poly_poly(ctx, &o1->poly, &o2->poly);
				case RTRND_VIA:   return isc_poly_via(ctx, &o1->poly, &o2->via);
				case RTRND_TEXT:
				case RTRND_BOARD: case RTRND_LAYER: case RTRND_NET: case RTRND_NETSEG:
					break;
			}
			break;
		case RTRND_VIA:
			switch(o2->hdr.type) {
				case RTRND_LINE:  return isc_line_via(ctx, &o2->line, &o1->via);
				case RTRND_ARC:   return isc_arc_via(ctx, &o2->arc, &o1->via);
				case RTRND_POLY:  return isc_poly_via(ctx, &o2->poly, &o1->via);
				case RTRND_VIA:   return isc_via_via(ctx, &o1->via, &o2->via);
				case RTRND_TEXT:
				case RTRND_BOARD: case RTRND_LAYER: case RTRND_NET: case RTRND_NETSEG:
					break;
			}
			break;
		case RTRND_TEXT:
		case RTRND_BOARD: case RTRND_LAYER: case RTRND_NET: case RTRND_NETSEG:
			break;
	}
	return 0;
}


/* Do everything that needs to be done for an object found */
static int rtrnd_find_found(rtrnd_find_t *ctx, rtrnd_any_obj_t *obj, rtrnd_any_obj_t *arrived_from)
{
	int fr;

	if (ctx->list_found)
		vtp0_append(&ctx->found, obj);

	ctx->nfound++;

	if ((ctx->found_cb != NULL) && ((fr = ctx->found_cb(ctx, obj, arrived_from)) != 0)) {
		if (fr == RTRND_FIND_DROP_THREAD)
			return 0;
		ctx->aborted = 1;
		return 1;
	}

	return 0;
}


static int rtrnd_find_addobj(rtrnd_find_t *ctx, rtrnd_any_obj_t *obj, rtrnd_any_obj_t *arrived_from, int jump)
{
	obj->hdr.mark |= ctx->mark;
	if (jump)
		vtp0_append(&ctx->open, obj);

	if (rtrnd_find_found(ctx, obj, arrived_from) != 0) {
		ctx->aborted = 1;
		return 1;
	}
	return 0;
}

#define RTRND_FIND_CHECK(ctx, curr, obj, retstmt) \
	do { \
		rtrnd_any_obj_t *__obj__ = (rtrnd_any_obj_t *)obj; \
		if (!(obj->hdr.mark & ctx->mark)) { \
			if (rtrnd_isc_obj_obj(ctx, curr, __obj__)) {\
				if (rtrnd_find_addobj(ctx, __obj__, curr, 1) != 0) { retstmt; } \
			} \
		} \
	} while(0)


static void rtrnd_find_exec(rtrnd_find_t *ctx)
{
	rtrnd_any_obj_t *curr;

	while((ctx->open.used > 0) && (!ctx->aborted)) {
		/* pop the last object, without reallocating to smaller, mark it found */
		ctx->open.used--;
		curr = ctx->open.array[ctx->open.used];

		{ /* search unmkared connections: iterative approach */
			rtrnd_any_obj_t *n;
			rtrnd_rtree_it_t it;
			if (curr->hdr.type == RTRND_VIA) {
#warning TODO: bbvia: consider span
				int lid;
				for(lid = 0; lid < ctx->brd->layers.used; lid++) {
					rtrnd_layer_t *layer = ctx->brd->layers.array[lid];
					for(n = rtrnd_rtree_first(&it, &layer->objs, &curr->hdr.bbox); n != NULL; n = rtrnd_rtree_next(&it))
						RTRND_FIND_CHECK(ctx, curr, n, return);
				}
			}
			else {
				for(n = rtrnd_rtree_first(&it, &curr->hdr.parent->layer.objs, &curr->hdr.bbox); n != NULL; n = rtrnd_rtree_next(&it))
					RTRND_FIND_CHECK(ctx, curr, n, return);
			}
			for(n = rtrnd_rtree_first(&it, &ctx->brd->vias, &curr->hdr.bbox); n != NULL; n = rtrnd_rtree_next(&it)) {
#warning TODO: bbvia: consider span
				RTRND_FIND_CHECK(ctx, curr, n, return);
			}
		}
	}
}

static int rtrnd_find_init_(rtrnd_find_t *fctx, rtrnd_board_t *brd)
{
	if (fctx->in_use)
		return -1;
	fctx->in_use = 1;
	fctx->aborted = 0;
	fctx->mark = rtrnd_mark_alloc(brd, "rtrnd_find_from_obj");
	fctx->nfound = 0;
	fctx->brd = brd;

	if (fctx->list_found)
		vtp0_init(&fctx->found);
	vtp0_init(&fctx->open);
	return 0;
}

int rtrnd_find_from_obj(rtrnd_find_t *ctx, rtrnd_board_t *brd, rtrnd_any_obj_t *from)
{
	if (from == NULL)
		return -1;

	if (rtrnd_find_init_(ctx, brd) < 0)
		return -1;

	rtrnd_mark_clear(brd, ctx->mark);
	rtrnd_find_addobj(ctx, from, NULL, 1); /* add the starting object with no 'arrived_from' */
	rtrnd_find_exec(ctx);
	return 0;
}

unsigned long rtrnd_find_from_obj_next(rtrnd_find_t *ctx, rtrnd_any_obj_t *from)
{
	if (from == NULL)
		return -1;

	rtrnd_find_addobj(ctx, from, NULL, 1); /* add the starting object with no 'arrived_from' */
	rtrnd_find_exec(ctx);
	return 0;
}

void rtrnd_find_free(rtrnd_find_t *ctx)
{
	if (!ctx->in_use)
		return;
	if (ctx->list_found)
		vtp0_uninit(&ctx->found);
	vtp0_uninit(&ctx->open);
	rtrnd_mark_free(ctx->brd, ctx->mark);
	ctx->in_use = 0;
}

