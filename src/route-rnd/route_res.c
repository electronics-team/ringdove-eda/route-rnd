/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  write the route_res block
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"
#include "route_res.h"

void rtrnd_res_init(rtrnd_t *ctx)
{
	if (ctx->io->save_end != NULL)
		ctx->io->save_begin(ctx, ctx->resf);
}

void rtrnd_res_add(rtrnd_t *ctx, const rtrnd_any_obj_t *obj)
{
	if (ctx->io->save_add != NULL)
		ctx->io->save_add(ctx, ctx->resf, obj);
}

static char logbuff[8192];
void rtrnd_res_log(rtrnd_t *ctx, char level, const char *fmt, ...)
{
	int len;
	va_list ap;

	if (ctx->io->save_log == NULL)
		return;

	va_start(ap, fmt);
	len = vsprintf(logbuff, fmt, ap);
	va_end(ap);

	if (len >= sizeof(logbuff)) {
		fprintf(stderr, "rtrnd_res_log: buffer overflow\n");
		abort();
	}

	if (len > 0)
		ctx->io->save_log(ctx, ctx->resf, level, logbuff, len);
}

void rtrnd_res_confkey(rtrnd_t *ctx, const rtrnd_conf_t *item)
{
	if (ctx->io->save_confkey != NULL)
		ctx->io->save_confkey(ctx, ctx->resf, item);
}

void rtrnd_res_uninit(rtrnd_t *ctx)
{
	if (ctx->io->save_end != NULL)
		ctx->io->save_end(ctx, ctx->resf);
}


void rtrnd_res_conf_all(rtrnd_t *ctx, const rtrnd_conf_t *first)
{
	for(; first->type != RTRND_CT_TERMINATOR; first++)
		rtrnd_res_confkey(ctx, first);
}

void rtrnd_draw_res_line(rtrnd_t *ctx, rtrnd_layer_t *ly_copper, rtrnd_layer_t *ly_annot, rtrnd_net_t *net, double x1, double y1, double x2, double y2, double cop_thick, double cop_clr, double annot_thick, double annot_clr)
{
	rtrnd_line_t tmp = {0};

	rtrnd_line_new(ly_annot, NULL, net, x1, y1, x2, y2, annot_thick, annot_clr);

	tmp.hdr.type = RTRND_LINE;
	tmp.hdr.parent = (rtrnd_any_obj_t *)ly_copper;
	tmp.hdr.net = net;
	tmp.cline.p1.x = x1; tmp.cline.p1.y = y1;
	tmp.cline.p2.x = x2; tmp.cline.p2.y = y2;
	tmp.thickness = cop_thick;
	tmp.clearance = cop_clr;

	rtrnd_res_add(ctx, (rtrnd_any_obj_t *)&tmp);
}

void rtrnd_draw_res_via(rtrnd_t *ctx, rtrnd_layer_t *ly_annot, rtrnd_net_t *net, double x, double y, double dia, double clr, double annot_thick, double annot_clr)
{
	rtrnd_via_t tmp = {0};
	double rx = annot_thick/2, ry = annot_thick*2/3;

	rtrnd_line_new(ly_annot, NULL, net, x-rx, y-ry, x+rx, y+ry, 0.05, 0);
	rtrnd_line_new(ly_annot, NULL, net, x+rx, y-ry, x-rx, y+ry, 0.05, 0);

	tmp.hdr.type = RTRND_VIA;
	tmp.hdr.net = net;
	tmp.x = x; tmp.y = y;
	tmp.dia = dia;
	tmp.clearance = clr;

	rtrnd_res_add(ctx, (rtrnd_any_obj_t *)&tmp);
}

int rtrnd_progress(rtrnd_t *ctx, double at)
{
	fprintf(stderr, "### progress: %.02f\n", at);
	return 0;
}
