/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  central, modular infrastructure API
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#ifndef ROUTE_RND_H
#define ROUTE_RND_H

#include <stdio.h>
#include <genvector/vtp0.h>
#include "conf.h"

typedef struct rtrnd_board_s rtrnd_board_t;
typedef struct rtrnd_layer_s rtrnd_layer_t;
typedef union rtrnd_any_obj_s rtrnd_any_obj_t;
typedef struct rtrnd_io_s  rtrnd_io_t;
typedef struct rtrnd_router_s rtrnd_router_t;

typedef struct rtrnd_s {
	const char *fn;
	char *name; /* name of the rotue request or board or task, as read from the file, filled in by the IO plugin */
	int verbose;
	rtrnd_board_t *board;
	vtp0_t annots; /* annotation layers, in order of rendering */
	rtrnd_io_t *io; /* the IO plugin that managed to load the request */
	FILE *resf; /* output route_res file */
	rtrnd_router_t *rt; /* router selected by the user */
} rtrnd_t;

struct rtrnd_io_s {
	const char *name;
	int (*test_parse)(FILE *f);         /* cheap test if a file looks like suitable for reading; returns non-zero on success */
	int (*load)(rtrnd_t *ctx, FILE *f); /* load the request; returns 0 on success */

 /* save the result; returns 0 on success  */
	int (*save_begin)(rtrnd_t *ctx, FILE *f);
	int (*save_add)(rtrnd_t *ctx, FILE *f, const rtrnd_any_obj_t *obj);
	int (*save_log)(rtrnd_t *ctx, FILE *f, char level, char *msg, int len);
	int (*save_confkey)(rtrnd_t *ctx, FILE *f, const rtrnd_conf_t *item);
	int (*save_end)(rtrnd_t *ctx, FILE *f);

};
extern vtp0_t rtrnd_all_io;

typedef struct rtrnd_export_s {
	const char *name;
	/* export a layer (or all layers if layer == NULL) */
	int (*export)(rtrnd_t *ctx, const char *basename, rtrnd_layer_t *layer, vtp0_t *annot_layers);
} rtrnd_export_t;

extern vtp0_t rtrnd_all_export;

struct rtrnd_router_s {
	const char *name, *desc;
	rtrnd_conf_t *conf;
	int (*route)(rtrnd_t *ctx);
};

extern vtp0_t rtrnd_all_router;


int route_rnd_main(int argc, char *argv[]);


/* Print an error message to stderr with the standard route-rnd prefix */
void rtrnd_error(const char *fmt, ...);

#endif
