#ifndef RTRND_IO_H
#define RTRND_IO_H

#include "route-rnd.h"

int rtrnd_load(rtrnd_t *ctx, const char *ifn);
int rtrnd_export(rtrnd_t *ctx, const char *fmt, const char *basename, rtrnd_layer_t *layer, vtp0_t *annot);

int rtrnd_io_set_by_name(rtrnd_t *ctx, const char *name);


#endif
