#ifndef RTRND_UTIL_RAT_H
#define RTRND_UTIL_RAT_H

#include <genlist/gendlist.h>
#include "data.h"

typedef struct rtrnd_rat_s {
	double cost;
	double x[2], y[2];
	rtrnd_any_obj_t *o[2];
	gdl_elem_t link; /* in a rtrnd_ratsnest_t */
} rtrnd_rat_t;

typedef enum { /* bitfield */
	RTRND_RNS_NO_ARC  = 1,
	RTRND_RNS_NO_DIAG = 2
} rtrnd_ratsnest_cfg_t;

typedef struct rtrnd_ratsnest_s {
	rtrnd_ratsnest_cfg_t cfg;
	gdl_list_t lst;
} rtrnd_ratsnest_t;


void rtrnd_ratsnest_map(rtrnd_ratsnest_t *nest, rtrnd_board_t *board, rtrnd_ratsnest_cfg_t cfg);

void rtrnd_ratsnest_draw(rtrnd_ratsnest_t *nest, rtrnd_layer_t *ly);

/* free all rat lines of a ratsnest */
void rtrnd_ratsnest_uninit(rtrnd_ratsnest_t *nest);

#endif
