/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  find galvanic connections
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#ifndef RTRND_FIND_H
#define RTRND_FIND_H

#define RTRND_FIND_DROP_THREAD 4242

typedef struct rtrnd_find_s rtrnd_find_t;
struct rtrnd_find_s {
	/* public config */
	unsigned list_found:1;
	double bloat;

	/* if non-NULL, call after an object is found; if returns non-zero,
	   set ->aborted and stop the search. When search started from an object,
	   it is called for the starting object as well. All object data and ctx
	   fields are updated for new_obj before the call. arrived_from is
	   the previous object (that already triggered a callback) from which
	   new_obj was first found; can be NULL for the starting object.
	   If return PCB_FIND_DROP_THREAD, stop searching this thread of objects
	   and continue elsewhere.
	   */
	int (*found_cb)(rtrnd_find_t *ctx, rtrnd_any_obj_t *new_obj, rtrnd_any_obj_t *arrived_from);

	/* public state/result */
	vtp0_t found;                   /* objects found, when list_found is 1 - of (rtrnd_any_obj_t *) */
	void *user_data;                /* filled in by caller, not read or modified by find code */

	/* private */
	vtp0_t open;                    /* objects already found but need checking for conns of (rtrnd_any_obj_t *) */
	rtrnd_board_t *brd;
	unsigned long mark;
	unsigned long nfound;
	unsigned in_use:1;
	unsigned aborted:1;
};

int rtrnd_find_from_obj(rtrnd_find_t *ctx, rtrnd_board_t *brd, rtrnd_any_obj_t *from);
unsigned long rtrnd_find_from_obj_next(rtrnd_find_t *ctx, rtrnd_any_obj_t *from);

void rtrnd_find_free(rtrnd_find_t *ctx);


#endif

