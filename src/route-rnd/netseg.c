/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  map network segments
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"
#include <assert.h>
#include "data.h"
#include "geo.h"
#include "find.h"
#include <gengeo2d/cline.h>
#include "netseg.h"


static int netseg_found_cb(rtrnd_find_t *ctx, rtrnd_any_obj_t *new_obj, rtrnd_any_obj_t *arrived_from)
{
	if (new_obj->hdr.netseg != NULL) {
		/* two net segs merge; switch this one over */
		rtrnd_netseg_merge(ctx->user_data, new_obj->hdr.netseg);
		return 0;
	}
	rtrnd_netseg_reg_obj(ctx->user_data, new_obj);
	return 0;
}

int rtrnd_netseg_map_from_obj(rtrnd_board_t *brd, rtrnd_any_obj_t *obj)
{
	rtrnd_find_t ctx = {0};
	ctx.found_cb = netseg_found_cb;
	ctx.user_data = rtrnd_netseg_new();

/*	printf("Net seg from: %s\n", obj->hdr.oid); */
	rtrnd_find_from_obj(&ctx, brd, obj);
	rtrnd_find_free(&ctx);

	rtrnd_netseg_reg_net(brd, ctx.user_data);

	return -1;
}

int rtrnd_netseg_map_board(rtrnd_board_t *brd)
{
	long lid;
	rtrnd_rtree_it_t it;
	rtrnd_any_obj_t *obj;

	for(lid = 0; lid < brd->layers.used; lid++) {
		rtrnd_layer_t *layer = brd->layers.array[lid];
		for(obj = rtrnd_rtree_all_first(&it, &layer->objs); obj != NULL; obj = rtrnd_rtree_all_next(&it))
			if (obj->hdr.netseg == NULL)
				rtrnd_netseg_map_from_obj(brd, obj);
	}

	for(obj = rtrnd_rtree_all_first(&it, &brd->vias); obj != NULL; obj = rtrnd_rtree_all_next(&it))
		if (obj->hdr.netseg == NULL)
			rtrnd_netseg_map_from_obj(brd, obj);

	return 0;
}


void rtrnd_netseg_unreg_obj(rtrnd_netseg_t *ns, rtrnd_any_obj_t *o)
{
	gdl_remove(&ns->objs, o, hdr.netseg_link);
	o->hdr.netseg = NULL;
}

void rtrnd_netseg_reg_obj(rtrnd_netseg_t *ns, rtrnd_any_obj_t *o)
{
	gdl_append(&ns->objs, o, hdr.netseg_link);
	o->hdr.netseg = ns;
	if (ns->objs.length == 1)
		ns->hdr.bbox = o->hdr.bbox;
	else
		rtrnd_rtree_box_bump(&ns->hdr.bbox, &o->hdr.bbox);
}

void rtrnd_netseg_free(rtrnd_netseg_t *ns)
{
	rtrnd_any_obj_t *o;

	if (ns->net != NULL)
		gdl_remove(&ns->net->segments, ns, link);
	else
		gdl_remove(&ns->brd->orphaned_segments, ns, link);

	for(o = gdl_first(&ns->objs); o != NULL; o = gdl_first(&ns->objs))
		rtrnd_netseg_unreg_obj(ns, o);
	free(ns->hdr.oid);
	free(ns);
}

void rtrnd_netseg_init(rtrnd_netseg_t *ns)
{
	memset(ns, 0, sizeof(rtrnd_netseg_t));
	ns->hdr.type = RTRND_NETSEG;
}

rtrnd_netseg_t *rtrnd_netseg_new(void)
{
	rtrnd_netseg_t *ns = malloc(sizeof(rtrnd_netseg_t));
	rtrnd_netseg_init(ns);
	return ns;
}

static void netseg_name(rtrnd_netseg_t *ns, const char *p1, const char *p2, long cnt)
{
	long l1 = strlen(p1), l2 = strlen(p2);
	free(ns->hdr.oid);
	ns->hdr.oid = malloc(l1+l2+16);
	if ((cnt > 1000000000L) || (cnt < -1000000000L))
		sprintf(ns->hdr.oid, "%s-%s:x", p1, p2);
	else
		sprintf(ns->hdr.oid, "%s-%s:%ld", p1, p2, cnt);
}


void rtrnd_netseg_reg_net(rtrnd_board_t *brd, rtrnd_netseg_t *ns)
{
	rtrnd_any_obj_t *o;
	int has_null = 0;

	ns->brd = brd;
	ns->net = NULL;
	ns->shorted = 0;
	for(o = gdl_first(&ns->objs); o != NULL; o = gdl_next(&ns->objs, o)) {
		if (o->hdr.net == NULL)
			has_null = 1;
		if (o->hdr.net != NULL)
			ns->net = o->hdr.net;
		else if (o->hdr.net != ns->net)
			ns->shorted = 1;
	}

	if ((ns->net == NULL) || ns->shorted) {
		ns->net = NULL;
		netseg_name(ns, "board", "orphaned", brd->orphaned_segments.length);
		gdl_append(&brd->orphaned_segments, ns, link);
		return;
	}

	netseg_name(ns, "net", ns->net->hdr.oid, ns->net->segments.length);
	gdl_append(&ns->net->segments, ns, link);
	if (has_null)
		for(o = gdl_first(&ns->objs); o != NULL; o = gdl_next(&ns->objs, o))
			if (o->hdr.net == NULL)
				o->hdr.net = ns->net;
}


void rtrnd_netseg_merge(rtrnd_netseg_t *dst, rtrnd_netseg_t *src)
{
	rtrnd_any_obj_t *o;

	assert(dst->hdr.net == src->hdr.net);

	for(o = gdl_first(&src->objs); o != NULL; o = gdl_next(&src->objs, o)) {
		gdl_remove(&src->objs, o, hdr.netseg_link);
		gdl_append(&dst->objs, o, hdr.netseg_link);
		o->hdr.netseg = dst;
		rtrnd_rtree_box_bump(&dst->hdr.bbox, &o->hdr.bbox);
	}

	if (src->shorted) {
		dst->shorted = 1;
		src->shorted = 0;
	}

	if (src->net != NULL) {
		gdl_remove(&src->net->segments, src, link);
		gdl_append(&src->brd->orphaned_segments, src, link);
		src->net = NULL;
	}
}
