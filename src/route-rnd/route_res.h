#ifndef RTRND_ROUTE_RES_H
#define RTRND_ROUTE_RES_H

#include <stdarg.h>

#include "data.h"
#include "conf.h"

void rtrnd_res_init(rtrnd_t *ctx);
void rtrnd_res_add(rtrnd_t *ctx, const rtrnd_any_obj_t *obj);
void rtrnd_res_log(rtrnd_t *ctx, char level, const char *fmt, ...);
void rtrnd_res_confkey(rtrnd_t *ctx, const rtrnd_conf_t *item);
void rtrnd_res_uninit(rtrnd_t *ctx);


void rtrnd_res_conf_all(rtrnd_t *ctx, const rtrnd_conf_t *first);


void rtrnd_draw_res_line(rtrnd_t *ctx, rtrnd_layer_t *ly_copper, rtrnd_layer_t *ly_annot, rtrnd_net_t *net, double x1, double y1, double x2, double y2, double cop_thick, double cop_clr, double annot_thick, double annot_clr);
void rtrnd_draw_res_via(rtrnd_t *ctx, rtrnd_layer_t *ly_annot, rtrnd_net_t *net, double x, double y, double dia, double clr, double annot_thick, double annot_clr);

int rtrnd_progress(rtrnd_t *ctx, double at);


#endif
