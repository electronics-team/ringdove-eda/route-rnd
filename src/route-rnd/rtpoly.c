/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  rtree based, hole-free polygons
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"

#include <string.h>
#include "geo.h"
#include <gengeo2d/cline.h>
#include "rtpoly.h"

#define RTP_MIN(a, b) (((a) < (b)) ? (a) : (b))
#define RTP_MAX(a, b) (((a) > (b)) ? (a) : (b))

void rtpoly_new_begin(rtpoly_t *poly)
{
	memset(poly, 0, sizeof(rtpoly_t));
	rtrnd_rtree_init(&poly->tree);
}

void rtpoly_new_append(rtpoly_t *poly, double x, double y)
{
	rtp_vertex_t *v = calloc(sizeof(rtp_vertex_t), 1);
	v->x = x;
	v->y = y;
	gdl_append(&poly->lst, v, link);
}

static int rtpoly_is_valid_selfi(rtpoly_t *poly)
{
	rtp_vertex_t *v, *vn, *vp, *n, *nn;

	for(v = gdl_first(&poly->lst); v != NULL; v = gdl_next(&poly->lst, v)) {
			rtrnd_rtree_it_t it;

		vn = gdl_next(&poly->lst, v);
		if (vn == NULL) vn = gdl_first(&poly->lst);
		vp = gdl_prev(&poly->lst, v);
		if (vp == NULL) vp = gdl_last(&poly->lst);

		for(n = rtrnd_rtree_first(&it, &poly->tree, &v->bbox); n != NULL; n = rtrnd_rtree_next(&it)) {
			g2d_cline_t c1, c2;

			if ((n == v) || (n == vp) || (n == vn)) continue;

			nn = gdl_next(&poly->lst, n);
			if (nn == NULL) nn = gdl_first(&poly->lst);
			c1.p1.x = v->x; c1.p1.y = v->y;
			c1.p2.x = vn->x; c1.p2.y = vn->y;
			c2.p1.x = n->x; c1.p2.y = n->y;
			c2.p2.x = nn->x; c2.p2.y = nn->y;
			if (g2d_isc_cline_cline(&c1, &c2))
				return 0;
		}
	}
	return 1;
}


int rtpoly_new_end(rtpoly_t *poly)
{
	rtp_vertex_t *v, *vn;

	for(v = gdl_first(&poly->lst); v != NULL; v = gdl_next(&poly->lst, v)) {
		vn = gdl_next(&poly->lst, v);
		if (vn == NULL) vn = gdl_first(&poly->lst);
		poly->area += (v->x - vn->x) * (v->y + vn->y);
	}

	/* make sure about the orientation */
	if (poly->area < 0) {
		gdl_reverse(&poly->lst);
		poly->area = -poly->area;
	}

	poly->bbox = g2d_box_invalid();
	for(v = gdl_first(&poly->lst); v != NULL; v = gdl_next(&poly->lst, v)) {
		g2d_vect_t pt;
		vn = gdl_next(&poly->lst, v);
		if (vn == NULL) vn = gdl_first(&poly->lst);
		v->bbox.x1 = RTP_MIN(v->x, vn->x);
		v->bbox.y1 = RTP_MIN(v->y, vn->y);
		v->bbox.x2 = RTP_MAX(v->x, vn->x);
		v->bbox.y2 = RTP_MAX(v->y, vn->y);
		rtrnd_rtree_insert(&poly->tree, v, &v->bbox);

		pt.x = v->x; pt.y = v->y;
		g2d_box_bump_pt(&poly->bbox, pt);
	}


	poly->area /= 2;
	poly->valid = rtpoly_is_valid_selfi(poly) && (poly->lst.length > 2);
	return !poly->valid;
}

void rtpoly_free(rtpoly_t *poly)
{
	rtp_vertex_t *v;
	rtrnd_rtree_uninit(&poly->tree);
	for(v = gdl_first(&poly->lst); v != NULL; v = gdl_first(&poly->lst)) {
		gdl_remove(&poly->lst, v, link);
		free(v);
	}
}


int rtpoly_isc_point(const rtpoly_t *poly, double x, double y)
{
	rtrnd_rtree_box_t raybox;
	rtrnd_rtree_it_t it;
	g2d_vect_t pt;
	g2d_calc_t cross;
	rtp_vertex_t *n, *nn;
	int cnt = 0;


	/* can't intersect if outside of the bbox */
	if ((x < poly->bbox.p1.x) || (x > poly->bbox.p2.x) || (y < poly->bbox.p1.y) || (y > poly->bbox.p2.y))
		return 0;


	/* cut the polygon with a horizontal ray from the point to right x->inf */
	raybox.x1 = x; raybox.y1 = y;
	raybox.x2 = poly->bbox.p2.x+1; raybox.y2 = y+0.001;

	pt.x = x; pt.y = y;

	for(n = rtrnd_rtree_first(&it, &poly->tree, &raybox); n != NULL; n = rtrnd_rtree_next(&it)) {
		g2d_cline_t edge;
		nn = gdl_next(&poly->lst, n);
		if (nn == NULL) nn = gdl_first(&poly->lst);

		edge.p1.x = n->x; edge.p1.y = n->y;
		edge.p2.x = nn->x; edge.p2.y = nn->y;

	/* the horizontal cutting line is between vectors s->v and s->v->next, but
	   these two can be in any order; because poly contour is CCW, this means if
	   the edge is going up, we went from inside to outside, else we went
	   from outside to inside */
		if (edge.p1.y <= y) {
			if (edge.p2.y > y) { /* this also happens to blocks horizontal poly edges because they are only == */
				g2d_vect_t v1 = g2d_vect_t_sub_g2d_vect_t(edge.p2, edge.p1);
				g2d_vect_t v2 = g2d_vect_t_sub_g2d_vect_t(pt, edge.p1);
				cross = g2d__crossp(g2d_cvect_t_convfrom_g2d_vect_t(v1), g2d_cvect_t_convfrom_g2d_vect_t(v2));
				if (cross == 0)
					return 1; /* special case: if the point is on any edge, the point is in the poly */
				if (cross > 0)
					cnt++;
			}
		}
		else { /* since the other side was <=, when we get here we also blocked horizontal lines of the negative direction */
			if (edge.p2.y <= y) {
				g2d_vect_t v1 = g2d_vect_t_sub_g2d_vect_t(edge.p2, edge.p1);
				g2d_vect_t v2 = g2d_vect_t_sub_g2d_vect_t(pt, edge.p1);
				cross = g2d__crossp(g2d_cvect_t_convfrom_g2d_vect_t(v1), g2d_cvect_t_convfrom_g2d_vect_t(v2));
				if (cross == 0)
					return 1; /* special case: if the point is on any edge, the point is in the poly */
				if (cross < 0)
					cnt--;
			}
		}
	}

	return cnt;
}

int rtpoly_isc_circle(const rtpoly_t *poly, double x, double y, double r)
{
	rtrnd_rtree_box_t circbox;
	rtrnd_rtree_it_t it;
	g2d_vect_t pt;
	rtp_vertex_t *n, *nn;
	double r2, dist2;


	/* can't intersect if outside of the bbox */
	if ((x < poly->bbox.p1.x-r) || (x > poly->bbox.p2.x+r) || (y < poly->bbox.p1.y-r) || (y > poly->bbox.p2.y+r))
		return 0;

	if (rtpoly_isc_point(poly, x, y))
		return 1;

	circbox.x1 = x-r; circbox.y1 = y-r;
	circbox.x2 = x+r; circbox.y2 = y+r;
	pt.x = x; pt.y = y;

	r2 = r*r;

	/* take each poly edge that is close to the circle and... */
	for(n = rtrnd_rtree_first(&it, &poly->tree, &circbox); n != NULL; n = rtrnd_rtree_next(&it)) {
		g2d_cline_t edge;
		nn = gdl_next(&poly->lst, n);
		if (nn == NULL) nn = gdl_first(&poly->lst);

		edge.p1.x = n->x; edge.p1.y = n->y;
		edge.p2.x = nn->x; edge.p2.y = nn->y;

		/* check if the line is closer to circle center than r */
		dist2 = g2d_dist2_cline_pt(&edge, pt);
		if (dist2 <= r2)
			return 1;
	}

	return 0;
}

int rtpoly_isc_cline(const rtpoly_t *poly, const g2d_cline_t *cline)
{
	rtrnd_rtree_box_t clinebox;
	g2d_box_t bx;
	rtrnd_rtree_it_t it;
	rtp_vertex_t *n, *nn;

	clinebox.x1 = RTP_MIN(cline->p1.x, cline->p2.x); clinebox.y1 = RTP_MIN(cline->p1.y, cline->p2.y);
	clinebox.x2 = RTP_MAX(cline->p1.x, cline->p2.x); clinebox.y2 = RTP_MAX(cline->p1.y, cline->p2.y);

	bx.p1.x = clinebox.x1; bx.p1.y = clinebox.y1;
	bx.p2.x = clinebox.x2; bx.p2.y = clinebox.y2;

	/* can't intersect if outside of the bbox */
	if (!g2d_isc_box_box(&poly->bbox, &bx))
		return 0;

	/* take each poly edge that is close to the circle and check for intersection */
	for(n = rtrnd_rtree_first(&it, &poly->tree, &clinebox); n != NULL; n = rtrnd_rtree_next(&it)) {
		g2d_cline_t edge;
		nn = gdl_next(&poly->lst, n);
		if (nn == NULL) nn = gdl_first(&poly->lst);

		edge.p1.x = n->x; edge.p1.y = n->y;
		edge.p2.x = nn->x; edge.p2.y = nn->y;

		if (g2d_isc_cline_cline(&edge, cline))
			return 1;
	}

	/* if either end is in the poly, the whole line is in */
	return rtpoly_isc_point(poly, cline->p1.x, cline->p1.y);
}

int rtpoly_isc_rtpoly(const rtpoly_t *p1, const rtpoly_t *p2)
{
	const rtpoly_t *big, *small;
	rtp_vertex_t *n, *nn, *i, *in;
	rtrnd_rtree_it_t it;

	if (!g2d_isc_box_box(&p1->bbox, &p2->bbox))
		return 0; /* polygons are not even near */

	if (p1->lst.length > p2->lst.length) {
		big = p1;
		small = p2;
	}
	else {
		big = p2;
		small = p1;
	}

	/* since our polygons are typically small and simple, this trivial
	loop will be relatively fast: iteate over each edge of the smaller
	polygon and rtree-search a collision with any edge of the bigger poly;
	worst case is probably O(small * log(big)) */
	for(n = gdl_first(&small->lst); n != NULL; n = gdl_next(&small->lst, n)) {
		g2d_cline_t sedge, bedge;
		g2d_box_t sbox;

		sbox.p1.x = n->bbox.x1; sbox.p1.y = n->bbox.y1;
		sbox.p2.x = n->bbox.x2; sbox.p2.y = n->bbox.y2;
		if (!g2d_isc_box_box(&big->bbox, &sbox))
			continue; /* small edge not even near the big poly */

		nn = gdl_next(&small->lst, n);
		if (nn == NULL) nn = gdl_first(&small->lst);

		sedge.p1.x = n->x; sedge.p1.y = n->y;
		sedge.p2.x = nn->x; sedge.p2.y = nn->y;

		for(i = rtrnd_rtree_first(&it, &big->tree, &n->bbox); i != NULL; i = rtrnd_rtree_next(&it)) {
			in = gdl_next(&big->lst, i);
			if (in == NULL) in = gdl_first(&big->lst);
			bedge.p1.x = i->x; bedge.p1.y = i->y;
			bedge.p2.x = in->x; bedge.p2.y = in->y;

			if (g2d_isc_cline_cline(&sedge, &bedge))
				return 1;
		}
	}

	/* lower probabilty: one polygon is fully within the other */
	n = gdl_first(&small->lst);
	if (rtpoly_isc_point(big, n->x, n->y)) return 1;
	n = gdl_first(&big->lst);
	if (rtpoly_isc_point(small, n->x, n->y)) return 1;

	return 0;
}
