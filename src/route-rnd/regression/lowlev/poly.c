#include <stdio.h>
#include <math.h>
#include "../rtpoly.h"

static inline int rtpoly_isc_cline_(const rtpoly_t *poly, double x1, double y1, double x2, double y2)
{
	g2d_cline_t cl;
	cl.p1.x = x1; cl.p1.y = y1;
	cl.p2.x = x2; cl.p2.y = y2;
	return rtpoly_isc_cline(poly, &cl);
}

static inline int rtpoly_isc_triangle(const rtpoly_t *poly, double cx, double cy, double r)
{
	int n, res;
	double a, step = 2.0*M_PI/3.0;
	rtpoly_t tri;
	rtpoly_new_begin(&tri);

	for(n = 0, a = 0; n < 3; n++, a+=step) {
/*		printf(" %f %f\n", cx + cos(a) * r, cy + sin(a) * r);*/
		rtpoly_new_append(&tri, cx + cos(a) * r, cy + sin(a) * r);
	}

	rtpoly_new_end(&tri);
	res = rtpoly_isc_rtpoly(poly, &tri);
	rtpoly_free(&tri);
	return res;
}

int main(int argc, char *argv[])
{
	int end;
	rtpoly_t poly;

	rtpoly_new_begin(&poly);

	rtpoly_new_append(&poly, 1,1);
	rtpoly_new_append(&poly, 5,1);
	rtpoly_new_append(&poly, 5,5);
	rtpoly_new_append(&poly, 1,5);

	end = rtpoly_new_end(&poly);
	printf("end=%d area=%f %f;%f - %f;%f\n", end, poly.area, poly.bbox.p1.x, poly.bbox.p1.y, poly.bbox.p2.x, poly.bbox.p2.y);

	printf("point isc:\n");
	printf(" in: %d\n", rtpoly_isc_point(&poly, 3, 3));
	printf(" out left: %d\n", rtpoly_isc_point(&poly, -3, 3));
	printf(" out right: %d\n", rtpoly_isc_point(&poly, 15, 3));
	printf(" on edge1:  %d\n", rtpoly_isc_point(&poly, 1, 3));
	printf(" on edge2:  %d\n", rtpoly_isc_point(&poly, 3, 1));
	printf(" on corner: %d\n", rtpoly_isc_point(&poly, 1, 1));

	printf("circ isc:\n");
	printf(" in: %d\n", rtpoly_isc_circle(&poly, 3, 3, 2));
	printf(" left1: %d\n", rtpoly_isc_circle(&poly, -1, 3, 2.2));
	printf(" left2: %d\n", rtpoly_isc_circle(&poly, -1, 3, 2));
	printf(" left3: %d\n", rtpoly_isc_circle(&poly, -1, -2, 2));
	printf(" corner: %d\n", rtpoly_isc_circle(&poly, 0, 0, 1.48));

	printf("cline isc:\n");
	printf(" in: %d\n", rtpoly_isc_cline_(&poly, 2, 2, 3, 3));
	printf(" side-yes: %d\n",   rtpoly_isc_cline_(&poly, 0, 1, 2, 1.5));
	printf(" side-nope: %d\n",   rtpoly_isc_cline_(&poly, 0, 1, 2, 0.9));
	printf(" corner-yes: %d\n", rtpoly_isc_cline_(&poly, 0, 0, 1, 1));


	printf("rtpoly isc:\n");
	printf(" in: %d\n", rtpoly_isc_triangle(&poly, 3, 3, 1));
	printf(" left-yes: %d\n", rtpoly_isc_triangle(&poly, 0, 3, 1));
	printf(" left-yes: %d\n", rtpoly_isc_triangle(&poly, 0, 3, 2));
	printf(" left-no:  %d\n", rtpoly_isc_triangle(&poly, -1, 3, 1));


	return 0;
}
