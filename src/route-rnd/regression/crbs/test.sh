#!/bin/sh

RTRND=../../route-rnd

while test $# -gt 0
do
	case "$1" in
		-r) keep_rats=1;;
	esac
	shift 1
done

rm -f *.res.tdx
for n in *.tdx
do
	b=${n%%.tdx}
	rm -f [123]*.svg
	$RTRND -v -v -v -m topo_crbs $n > $b.log
	mv 3routed.svg $b.svg || echo "file: $n" 2>&1
	if test ! -z "$keep_rats"
	then
	mv 2_1_laa.svg $b.rat.svg
	fi
done

rm -f [123]*.svg

