#!/bin/sh

mkdir -p web

./test.sh -r 

awk '
BEGIN {n = 0; q = "\""; }
($1 == "@") { basename=$2; FILES[n] = basename; n++; next }
{ TEXT[basename] = TEXT[basename] " " $0 }

END {
	for(n = 0; FILES[n] != ""; n++) {
		basename = FILES[n]
		prev_bn = FILES[n-1]
		next_bn = FILES[n+1]
		html = "web/" basename ".html"
		print "<html><body><h1>route-rnd/crbs test output</h1><h2>", basename, "</h2>" > html
		print "<p><table border=0 cellspacing=30><tr>" > html

		print "<tr><td colspan=3><p><center>" TEXT[basename] "</center>" > html

		print "<tr><td><img src=" q basename ".rat.svg" q " width=600px>" > html
		print "<td><b>&rarr;</b>" > html
		print "<td><img src=" q basename ".svg" q " width=600px>" > html
		print "<tr><td>" > html
		if (prev_bn != "")
			print "<a href=" q prev_bn ".html" q "> &larr; previous input </a>" > html
		else
			print "&nbsp;" > html
		print "<td>&nbsp;<td align=right>" > html
		if (next_bn != "")
			print "<a href=" q next_bn ".html" q "> &rarr; next input </a>" > html
		else
			print "&nbsp;" > html
		print "</table></body></html>" > html
		
		close(html)
		system("cp " basename ".*svg web")
	}
	system("cd web; ln -sf " FILES[0] ".html index.html")
}

' < index.in