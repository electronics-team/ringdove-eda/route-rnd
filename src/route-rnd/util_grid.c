/*
 *                            COPYRIGHT
 *
 *  route-rnd, modular printed circuit board autorouter
 *
 *  helper functions and data structures for grid based routing
 *  route-rnd Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 PET Fund in 2020)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "route-rnd.h"
#include "data.h"
#include <gengeo2d/box.h>
#include "htdi.h"
#include "util_grid.h"

static int dcmp(const void *A, const void *B)
{
	double a = *(double *)A, b = *(double *)B;
	return (a >= b) ? +1 : -1;
}

int rtrnd_grid_detect(vtd0_t *samples, double *origin, double *spacing)
{
	long n, i, best_spc_score = 0;
	double best_orig = 0, best_spc = 0;
	htdi_t spc, offs;

	if (samples->used < 2)
		return -1;

	htdi_init(&spc, htdi_keyhash, htdi_keyeq);

	qsort(samples->array, samples->used, sizeof(double), dcmp);

	for(i = 0; i < samples->used-1; i++) {
		double d = samples->array[i+1] - samples->array[i], best_offs;
		htdi_entry_t *e;
		long best_score;

		if (d == 0)
			continue;

		if (htdi_get(&spc, d) > 0)
			continue;
		htdi_set(&spc, d, 1);

		/* assume the next distance as grid spacing; calculate the offset for each
		   point and see what's the most common offset and how popular it is */
		htdi_init(&offs, htdi_keyhash, htdi_keyeq);
		for(n = 0; n < samples->used; n++) {
			long cnt;
			double o = fmod(samples->array[n], d);
			cnt = htdi_get(&offs, o);
			cnt++;
			htdi_set(&offs, o, cnt);
		}
		best_score = 0;
		for(e = htdi_first(&offs); e != NULL; e = htdi_next(&offs, e)) {
			if (e->value > best_score) {
				best_score = e->value;
				best_offs = e->key;
			}
		}
		htdi_uninit(&offs);
/*		printf(" cand: spc=%f offs=%f %ld/%ld\n", d, best_offs, best_score, samples->used);*/
		if (best_score > best_spc_score) {
			best_spc_score = best_score;
			best_orig = best_offs;
			best_spc = d;
		}
	}

	best_orig = fmod(best_orig, best_spc);
/*	printf(" best: spc=%f offs=%f %ld/%ld\n", best_spc, best_orig, best_spc_score, samples->used);*/
	htdi_uninit(&spc);

	*origin = best_orig;
	*spacing = best_spc;

	return 0;
}

#define APPEND_IF_TERM(term) \
do { \
	if (term->hdr.terminal) { \
		double cx, cy; \
		if (rtrnd_obj_center(term, &cx, &cy) == 0) { \
			vtd0_append(&tx, cx); \
			vtd0_append(&ty, cy); \
		} \
	} \
} while(0)

int rtrnd_grid_detect_terms(const rtrnd_board_t *brd, double *x_orig, double *x_spacing, double *y_orig, double *y_spacing)
{
	vtd0_t tx, ty;
	int ret, n;
	rtrnd_any_obj_t *obj;
	rtrnd_rtree_it_t it;

	vtd0_init(&tx);
	vtd0_init(&ty);


	for(obj = rtrnd_rtree_all_first(&it, &brd->vias); obj != NULL; obj = rtrnd_rtree_all_next(&it))
		APPEND_IF_TERM(obj);

	for(n = 0; n < brd->layers.used; n++) {
		rtrnd_layer_t *layer = brd->layers.array[n];
		for(obj = rtrnd_rtree_all_first(&it, &layer->objs); obj != NULL; obj = rtrnd_rtree_all_next(&it))
			APPEND_IF_TERM(obj);
	}

	ret =  rtrnd_grid_detect(&tx, x_orig, x_spacing);
	ret |= rtrnd_grid_detect(&ty, y_orig, y_spacing);
	vtd0_uninit(&tx);
	vtd0_uninit(&ty);
	return ret;
}


void rtrnd_ragrid_init(rtrnd_ragrid_t *grid, double min_coord, double max_coord, double origin, double spacing, double minor0, double minor1)
{
	double c, from;
	long n;

	assert(max_coord > min_coord);

	grid->len = floor((max_coord - min_coord) / spacing)+3;
	grid->raline = calloc(sizeof(rtrnd_raline_t), grid->len);
	grid->spacing = spacing;
	from = floor(min_coord / spacing) * spacing + fmod(origin, spacing);
	for(c = from - spacing, n = 0; n < grid->len; c += spacing, n++) {
		grid->raline[n].major = c;
		vtd0_append(&grid->raline[n].minor, minor0);
		vtd0_append(&grid->raline[n].minor, minor1);
	}
}

void rtrnd_ragrid_uninit(rtrnd_ragrid_t *grid)
{
	long n;
	for(n = 0; n < grid->len; n++)
		vtd0_uninit(&grid->raline[n].minor);
	free(grid->raline);
	grid->len = 0;
}

void rtrnd_ragrid_draw(rtrnd_ragrid_t *grid, rtrnd_layer_t *layer, int is_major_x)
{
	long n, i;
	double thick = 0.1;

	for(i = 0; i < grid->len; i++) {
		double major = grid->raline[i].major;
		rtrnd_raline_t *ra = &grid->raline[i];
		for(n = 0; n < ra->minor.used; n += 2) {
			if (is_major_x)
				rtrnd_line_new(layer, NULL, NULL, major, ra->minor.array[n], major, ra->minor.array[n+1], thick, 0);
			else
				rtrnd_line_new(layer, NULL, NULL, ra->minor.array[n], major, ra->minor.array[n+1], major, thick, 0);
		}
	}
}

int rtrnd_raline_block(rtrnd_raline_t *ra, double from, double to)
{
	long i1 = -1, i2 = -1, n, hole1, hole2, dfrom, dto;
	double *p;

	if (to <= from)
		return -1;

	if (from <= ra->minor.array[0]) /* from is before the first range */
		i1 = 0;

	if (to >= ra->minor.array[ra->minor.used-1]) /* to is after the last range */
		i2 = ra->minor.used-1;

	for(n = 0; n < ra->minor.used-1; n++) {
		if ((ra->minor.array[n] <= from) && (ra->minor.array[n+1] >= from))
			i1 = n;
		if ((ra->minor.array[n] <= to) && (ra->minor.array[n+1] >= to)) {
			i2 = n;
			break;
		}
	}

	if ((i1 < 0) || (i2 < 0))
		return -1;

	hole1 = ((i1 % 2) == 1);
	hole2 = ((i2 % 2) == 1);

	if (i1 == i2) {
		if (hole1) /* creating a hole within a hole -> nop */
			return 0;

		/* same range: have to add a new range */
		p = vtd0_alloc_insert(&ra->minor, i1+1, 2);
		p[0] = from;
		p[1] = to;
		return 0;
	}

	/* in two different ranges - tune the ends of the start/end range and
	   remove any range in between */
	if (hole1) {
		/* starting from a hole */
		dfrom = i1+1;
	}
	else if (ra->minor.array[i1] == from) {
		/* from-pos-range is removed from its start - remove the whole range */
		dfrom = i1;
	}
	else {
		ra->minor.array[i1+1] = from;
		dfrom = i1+2;
	}

	if (hole2) {
		/* ending in a hole */
		dto = i2;
	}
	else if (ra->minor.array[i2+1] == to) {
		/* to-pos-range is removed to its end - remove the whole range */
		dto = i2+1;
	}
	else {
		ra->minor.array[i2] = to;
		dto = i2-1;
	}
	vtd0_remove(&ra->minor, dfrom, (dto+1) - dfrom);
	return 0;
}

G2D_INLINE int g2d_box_clip2(g2d_box_t *dst, const g2d_box_t *box, const g2d_box_t *clipbox)
{
	if (!g2d_isc_box_box(box, clipbox))
		return 0;
	dst->p1.x =(  G2D_MAX ( box->p1.x , clipbox->p1.x ) );
	dst->p2.x =(  G2D_MIN ( box->p2.x , clipbox->p2.x ) );
	dst->p1.y =(  G2D_MAX ( box->p1.y , clipbox->p1.y ) );
	dst->p2.y =(  G2D_MIN ( box->p2.y , clipbox->p2.y ) );
	return 1;
}

int rtrnd_raline_obj_mask_size_at(rtrnd_raline_t *ra, int is_major_x, double lthick, double lclr, rtrnd_any_obj_t *obj, double *minor_from, double *minor_to)
{
	g2d_box_t rabox, clbox, obox;
	double width, clr;

	switch(obj->hdr.type) {
		case RTRND_LINE:
		case RTRND_POLY:
		case RTRND_VIA:
#warning TODO: lazy until gengeo2d is done
#warning TODO: if object's clearance is bigger than width, use that!
			clr = lclr;
			width = lthick + clr;
			if (is_major_x) {
				rabox.p1.y = ra->minor.array[0] - width;
				rabox.p2.y = ra->minor.array[ra->minor.used-1] + width;
				rabox.p1.x = ra->major - width;
				rabox.p2.x = ra->major + width;
			}
			else {
				rabox.p1.x = ra->minor.array[0] - width;
				rabox.p2.x = ra->minor.array[ra->minor.used-1] + width;
				rabox.p1.y = ra->major - width;
				rabox.p2.y = ra->major + width;
			}

			obox.p1.x = obj->hdr.bbox.x1; obox.p1.y = obj->hdr.bbox.y1;
			obox.p2.x = obj->hdr.bbox.x2; obox.p2.y = obj->hdr.bbox.y2;
			if (!g2d_box_clip2(&clbox, &rabox, &obox))
				break;  /* no intersection */

			if (is_major_x) {
				*minor_from = clbox.p1.y-clr;
				*minor_to = clbox.p2.y+clr;
			}
			else {
				*minor_from = clbox.p1.x-clr;
				*minor_to = clbox.p2.x+clr;
			}
			return 0;

		case RTRND_ARC:
#warning TODO
		case RTRND_TEXT:
		case RTRND_BOARD:
		case RTRND_LAYER:
		case RTRND_NET:
		case RTRND_NETSEG:
			return -1;
	}

	return -1;
}

int rtrnd_raline_mask_obj(rtrnd_raline_t *ra, int is_major_x, double lthick, double lclr, rtrnd_any_obj_t *obj)
{
	int res;
	double minor_from, minor_to;

	res = rtrnd_raline_obj_mask_size_at(ra, is_major_x, lthick, lclr, obj, &minor_from, &minor_to);
	if (res != 0)
		return res;

	return rtrnd_raline_block(ra, minor_from, minor_to);
}

int rtrnd_grid_mask_obj(rtrnd_ragrid_t *grid, int is_major_x, double lthick, double lclr, rtrnd_any_obj_t *obj)
{
	double from, to;
	long n;

	switch(obj->hdr.type) {
		case RTRND_LINE:
		case RTRND_POLY:
		case RTRND_VIA:
		case RTRND_ARC:
			if (is_major_x) {
				from = obj->hdr.bbox.x1;
				to = obj->hdr.bbox.x2;
			}
			else {
				from = obj->hdr.bbox.y1;
				to = obj->hdr.bbox.y2;
			}

			for(n = 0; n < grid->len; n++) {
				if (grid->raline[n].major < from)
					continue;
				if (grid->raline[n].major > to)
					break;
				rtrnd_raline_mask_obj(&grid->raline[n], is_major_x, lthick, lclr, obj);
			}
			return 0;
		case RTRND_TEXT:
		case RTRND_BOARD:
		case RTRND_LAYER:
		case RTRND_NET:
		case RTRND_NETSEG:
			return -1;
	}

	return 0;
}

void rtrnd_grid_mask_objs(rtrnd_ragrid_t *grid, int is_major_x, rtrnd_board_t *brd, rtrnd_layer_t *layer, double wire_thick, double wire_clr)
{
	rtrnd_any_obj_t *obj;
	rtrnd_rtree_it_t it;

	/* cut out layer objects if there's a layer */
	if (layer != NULL)
		for(obj = rtrnd_rtree_all_first(&it, &layer->objs); obj != NULL; obj = rtrnd_rtree_all_next(&it))
			rtrnd_grid_mask_obj(grid, is_major_x, wire_thick, wire_clr, obj);

	/* cut out vias if there's a board */
	if (brd != NULL)
		for(obj = rtrnd_rtree_all_first(&it, &brd->vias); obj != NULL; obj = rtrnd_rtree_all_next(&it))
			rtrnd_grid_mask_obj(grid, is_major_x, wire_thick, wire_clr, obj);
}

typedef struct {
	double major;
	const rtrnd_raline_t *last;
} major_bs_t;

static int major_cmp(const void *Major, const void *B)
{
	major_bs_t *ctx = (major_bs_t *)Major;
	const rtrnd_raline_t *raline = B;

	ctx->last = raline;
	if (ctx->major == raline->major ) return 0;
	if (ctx->major < raline->major ) return -1;
	return +1;
}

rtrnd_raline_t *rtrnd_grid_find_major_around(rtrnd_ragrid_t *grid, double major)
{
	major_bs_t ctx;
	ctx.major = major;
	ctx.last = NULL;
	bsearch(&ctx, grid->raline, grid->len, sizeof(rtrnd_raline_t), major_cmp);
	return (rtrnd_raline_t *)ctx.last;
}

rtrnd_raline_t *rtrnd_grid_find_major_before(rtrnd_ragrid_t *grid, double major)
{
	rtrnd_raline_t *ra = rtrnd_grid_find_major_around(grid, major);
	if (ra->major >= major)
		ra--;
	return ra;
}

int rtrnd_raline_range_avail(rtrnd_raline_t *ra, double from, double to)
{
	long n;

	for(n = 0; n < ra->minor.used; n+=2)
		if ((from >= ra->minor.array[n]) && (from <= ra->minor.array[n+1]))
			return (to <= ra->minor.array[n+1]);

	return 0;
}

long rtrnd_raline_pt_range(rtrnd_raline_t *ra, double minor_pt)
{
	long n;

	for(n = 0; n < ra->minor.used; n+=2)
		if ((minor_pt >= ra->minor.array[n]) && (minor_pt <= ra->minor.array[n+1]))
			return n;

	return -1;
}

long rtrnd_raline_pt_neg_range(rtrnd_raline_t *ra, double minor_pt)
{
	long n;

	for(n = 1; n < ra->minor.used; n+=2)
		if ((minor_pt >= ra->minor.array[n]) && (minor_pt <= ra->minor.array[n+1]))
			return n;

	return -1;
}


int rtrnd_raline_in_grid(rtrnd_ragrid_t *grid, rtrnd_raline_t *ra)
{
	return (ra >= grid->raline) && (ra < (grid->raline + grid->len));
}

long rtrnd_raline_dist(rtrnd_ragrid_t *grid, rtrnd_raline_t *ra, rtrnd_raline_t *rb)
{
	assert(rtrnd_raline_in_grid(grid, ra));
	assert(rtrnd_raline_in_grid(grid, rb));
	return ra - rb;
}


rtrnd_raline_t *rtrnd_raline_step(rtrnd_ragrid_t *grid, rtrnd_raline_t *ra, long delta)
{
	long idx = ra - grid->raline;

	if ((idx < 0) || (idx >= grid->len))
		return NULL;

	idx += delta;
	if ((idx < 0) || (idx >= grid->len))
		return NULL;

	return grid->raline + idx;
}
