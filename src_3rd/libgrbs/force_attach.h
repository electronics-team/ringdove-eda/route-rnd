/* for internal use only */
static int grbs_force_attach_line_to_pt(grbs_t *grbs, grbs_line_t *line, grbs_point_t *pt, double outer_copr, double clr, int segi);
static int grbs_force_detach(grbs_t *grbs, grbs_arc_t *arc);
