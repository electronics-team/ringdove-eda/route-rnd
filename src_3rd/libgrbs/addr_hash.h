#include "grbs.h"

/* hash key that encodes a mostly topological point on an orbit */
typedef struct grbs_addr_key1_s {
	unsigned short int ang;             /* in (radian * 10000), normalized between 0 and 2*PI */
	unsigned char orbit;                /* 0 means incident, 1 is the first (innermost non-sentinel) orbit in the segment */
	unsigned valid:1;
	unsigned is_vconcave:1;
	unsigned is_ccw:1;
	unsigned pt_uid:32;
} grbs_addr_key1_t;

typedef struct grbs_addr_key_s {
	grbs_addr_key1_t curr, last, last2;
} grbs_addr_key_t;

/* Convert address or detached address to key */
grbs_addr_key_t grbs_addr_new_to_key(const grbs_addr_t *addr);
grbs_addr_key_t grbs_det_addr_to_key(const grbs_detached_addr_t det[2]);
grbs_addr_key_t grbs_point_to_key(const grbs_point_t *pt);

/* Standard functions for genht */
int grbs_addr_hash_keyeq(grbs_addr_key_t a, grbs_addr_key_t b);

/* This macro creates a hash function for grbs_addr_key_t (depends on
   genht/hash.h */
#define GRBS_ADDR_HASH(func_name) \
	unsigned int func_name(grbs_addr_key_t key) { return murmurhash(&key, sizeof(key)); }
