int grbs_bicycle_angles(double cx1, double cy1, double r1, double cx2, double cy2, double r2, double a[4], int cross);

int grbs_angle_in_arc(double arc_sa, double arc_da, double ang, int inclusive);

double grbs_arc_get_delta(double sa, double ea, int dir);


/* A line from point from_x;from_y is connecting to an arc, radius to_r, at
   angle to_a. Assuming routing is coming from the point, through the line,
   calculate and return arc angle direction to continue that line (+1 or -1). */
int grbs_get_adir(double from_x, double from_y, double to_cx, double to_cy, double to_r, double to_a);

/* Return the next or prev arc with in_use==1 around a point */
grbs_arc_t *grbs_next_arc_in_use(grbs_arc_t *arc);
grbs_arc_t *grbs_prev_arc_in_use(grbs_arc_t *arc);


#define GRBS_MIN(a,b) ((a) < (b) ? (a) : (b))
#define GRBS_MAX(a,b) ((a) > (b) ? (a) : (b))
#define GRBS_PI 3.14159265358979323846

#define grbs_arc_overlaps_arc(arc1, arc2) \
	(grbs_angle_in_arc((arc1)->sa, (arc1)->da, (arc2)->sa, 1) || grbs_angle_in_arc((arc1)->sa, (arc1)->da, ((arc2)->sa) + (arc2)->da, 1))

#define grbs_arc_overlaps_arc_new(arc1, arc2) \
	(grbs_angle_in_arc((arc1)->sa, (arc1)->da, (arc2)->new_sa, 1) || grbs_angle_in_arc((arc1)->sa, (arc1)->da, ((arc2)->new_sa) + (arc2)->new_da, 1))
