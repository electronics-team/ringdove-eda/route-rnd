#define addr_is_incident(addr) (((addr)->type & 0x0F) == ADDR_POINT)

static grbs_point_t *addr_point(const grbs_addr_t *addr)
{
	switch(addr->type & 0x0F) {
		case ADDR_ARC_CONVEX:
		case ADDR_ARC_VCONCAVE:
			return addr->obj.arc->parent_pt;
		case ADDR_POINT:
			return addr->obj.pt;
	}
	abort();
	return 0;
}

