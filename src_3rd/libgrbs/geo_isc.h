#ifndef GRBS_GEO_ISC_H
#define GRBS_GEO_ISC_H

#include "config.h"

#define G2D_INLINE GRBS_INLINE
#include "gengeo2d/typecfg_double_double.h"
#include <gengeo2d/prim.h>

#endif
