#ifndef USRCH_COMMON_H
#define USRCH_COMMON_H

#ifndef USRCH_INLINE
#	define USRCH_INLINE
#endif

typedef enum {
	USRCH_RES_ERROR = -1, /* unrecoverable error, stop (path not found included) */
	USRCH_RES_CONTINUE,   /* continue the search */
	USRCH_RES_FOUND,      /* found a path, stop */

	/* aliases */
	USRCH_RES_SUCCESS = USRCH_RES_CONTINUE
} usrch_res_t;

#define usrch_malloc(ctx, size) \
	(((ctx)->malloc == NULL) ? (malloc(size)) : ((ctx)->malloc(ctx, (size))))

#define usrch_free(ctx, ptr) \
	(((ctx)->free == NULL) ? (free(ptr)) : ((ctx)->free(ctx, (ptr))))

#endif
