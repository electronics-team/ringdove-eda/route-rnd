/*  libusteiner - simple approximation to minimal 2D Euclidean Steiner Tree
    Copyright (C) 2020  Tibor 'Igor2' Palinkas

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

    Source code: svn://repo.hu/libusteiner/trunk
    Author: Tibor 'Igor2' Palinkas
    Contact: http://igor2.repo.hu/contact.html
*/

#ifndef LIBUSTEINER_H
#define LIBUSTEINER_H

#include <genvector/vtp0.h>

#ifndef USTN_INLINE
# define USTN_INLINE static inline
#endif
#ifndef USTN_COORD_T
# define USTN_COORD_T double
# define USTN_COORD_FMT "%.3f"
#endif

typedef USTN_COORD_T ustn_coord_t;

/* steiner point - shadow of an input poin */

typedef struct ustn_node_s ustn_node_t;

struct ustn_node_s {
	ustn_coord_t ix, iy;  /* input point coorsd we are shadowing */
	ustn_coord_t x, y;    /* current location of the steiner point */
	ustn_coord_t nx, ny;  /* new coordinates calculated for the next iteration */
	unsigned cloned:1;    /* should not be tied to its input point when calculating total length as it's already cloned into another steiner point placed on the input */
	unsigned fixed:1;     /* do not move */

	vtp0_t edges;         /* edges currently connected to this point */

	/* disjoint set */
	ustn_node_t *parent;
	int rank;

	long user_long;
	void *user_ptr;

	/* temp storage for sort-by-angle */
	double ang;

	/* temp for spring force optimization */
	double stepping;
};

typedef struct {
	ustn_node_t *p1, *p2;

	/* cache: components and length */
	double len, len2;
	double dx, dy;
	double ang; /* temporary storage for sorting */

	long user_long;
	void *user_ptr;

	unsigned fixed:1;     /* used by the caller to remember an edge was edded as static */
} ustn_edge_t;

typedef struct {
	vtp0_t nodes, edges;

	/* for simulation iterations */
	long itc; /* iteration counter */
	int hit;
	double last, avg, totlen;

	long user_long;
	void *user_ptr;

	/* configuration */
	double pt_too_close2; /* if a newly added point (steiner point) is too close to a fixed input, merge it with the fixed input point; square value */

	/* allocation cache */
	vtp0_t free_edges;
} ustn_tree_t;

USTN_INLINE ustn_node_t *ustn_add_node(ustn_tree_t *tree, ustn_coord_t x, ustn_coord_t y)
{
	ustn_node_t *n = calloc(sizeof(ustn_node_t), 1);
	n->ix = n->x = x;
	n->iy = n->y = y;
	n->stepping = 0.1;
	vtp0_append(&tree->nodes, n);
	return n;
}

/* add a fixed edge between two points */
ustn_edge_t *ustn_add_edge(ustn_tree_t *mst, ustn_node_t *p1, ustn_node_t *p2);

/* Slow linear search for a point already in the tree, by coords; if require_fixed
   is 0, return any point; if require_fixed is 1, return only fixed points,
   if it is -1, return only non-fixed points */
ustn_node_t *ustn_find_node(ustn_tree_t *tree, ustn_coord_t x, ustn_coord_t y, int require_fixed);

/* Call this before running the iterations */
void ustn_solve_iterate_pre(ustn_tree_t *mst);

/* Run an iteration. Returns whether more iterations are needed. Result:
   mst->edges contains the 2-nets, connecting mst->nodes */
int ustn_solve_iterate(ustn_tree_t *mst);

/* Run this after the iterations to tidy up. */
void ustn_optimize(ustn_tree_t *mst);

double ustn_tree_length(ustn_tree_t *mst);



#endif
