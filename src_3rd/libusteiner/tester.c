#include <math.h>
#include "libusteiner.h"
#include "debug.h"

int main(int argc, char *argv[])
{
	ustn_tree_t tree = {0};
	long err;
	double initlen;

	if (argc > 1)
		err = ustn_load_pts(argv[1], &tree);
	else
		err = ustn_fload_pts(stdin, &tree);
	if (err != 0) {
		fprintf(stderr, "Syntax error on stdin in line %ld\n", err);
		return 1;
	}

	ustn_save_svg("0_load.svg", &tree);
	ustn_solve_iterate_pre(&tree);
	initlen = ustn_tree_length(&tree);
	fprintf(stderr, "Initial length (raw spanning tree): %f\n", initlen);
	ustn_save_svg("1_span.svg", &tree);

/*	last = HUGE_VAL;*/
	while(tree.itc < 9000) {
		char fn[256];

		sprintf(fn, "2_it_%04ld.svg", tree.itc);
		if (!ustn_solve_iterate(&tree))
			break;

		fprintf(stderr, " --- %s len=%f %f avg=%f gain=%f%%\n", fn, tree.totlen, tree.last - tree.totlen, tree.avg, 100-tree.totlen/initlen*100.0);
		printf("%f\n", (tree.avg - tree.totlen));

		if ((tree.itc % 10) == 0)
			ustn_save_svg(fn, &tree);
	}

	fprintf(stderr, "spanning tree len = %f\n", initlen);
	ustn_save_svg("3_equ.svg", &tree);
	fprintf(stderr, "steiner tree  len = %f\n", ustn_tree_length(&tree));
	ustn_optimize(&tree);
	fprintf(stderr, "optimized     len = %f\n", ustn_tree_length(&tree));
	ustn_save_svg("4_result.svg", &tree);
}
