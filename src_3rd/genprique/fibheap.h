/*

genprique - microlib for priority queues - fibonacci heap

Copyright (c) 2020 Tibor 'Igor2' Palinkas
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the Author nor the names of contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

Source code: svn://svn.repo.hu/genprique/trunk
Contact the author: http://igor2.repo.hu/contact.html

*/

#ifndef GENPRIQUE_FIBHEAP_H
#define GENPRIQUE_FIBHEAP_H

/* Type of the priority field; use long by default */
#ifndef GENPRIQUE_PRI_TYPE
#define GENPRIQUE_PRI_TYPE long
#endif

/* Prefix API functions; can be static to make the whole lib private to a
   source file #including both the .h and the .c */
#ifndef GENPRIQUE_API
#define GENPRIQUE_API
#endif

typedef struct fbhn_s fbhn_t;

typedef struct fbh_s {
	unsigned long offs;

	unsigned long num_nodes;
	fbhn_t *min;
} fbh_t;

struct fbhn_s {
	fbhn_t *parent, *left, *right, *child;
	GENPRIQUE_PRI_TYPE pri;
	unsigned short deg;
	unsigned mark:1;
};


GENPRIQUE_API int fbh_init(fbh_t *fbh, unsigned long offs);
GENPRIQUE_API int fbh_insert(fbh_t *fbh, void *data, GENPRIQUE_PRI_TYPE pri);
GENPRIQUE_API void *fbh_pop_min(fbh_t *fbh);

GENPRIQUE_API int fbh_concat_heap(fbh_t *dst, fbh_t *src);

/* return the data pointer of the minimum node */
#define fbh_min(fbh) ((void *)((fbh)->min == NULL ? NULL : fbh_n2o((fbh), (fbh)->min)))

/*** internal ***/

/* calculate the node pointer from data knowing the address */
#define fbh_o2n(fbh, data)  ((fbhn_t *)(((char *)(data)) + fbh->offs))

/* calculate the user data pointer from node */
#define fbh_n2o(fbh, node)  ((void *)(((char *)(node)) - fbh->offs))

#endif
