/* libsprand - simple/portable pseudorandom generators.
   Public domain.

   Project page: http://www.repo.hu/projects/libpsrand
   Project VCS: svn://svn.repo.hu/libpsrand/trunk
   Contact: http://igor2.repo.hu/contact.html
*/

#ifndef PSR_MINSTD_H
#define PSR_MINSTD_H

#include "libpsrand/psr.h"

#define PSR_MINSTD_MAX (0x7fffffffUL)

typedef psr_uint32_t psr_minstd_t;

/* NOTE: seed shall be coprime to PSR_MINSTD_MAX */
void psr_minstd_init(psr_minstd_t *state, psr_uint32_t seed);
psr_uint32_t psr_minstd_rand(psr_minstd_t *state);

#define psr_minstd_rand01(state) \
	((double)psr_minstd_rand(state) / (double)PSR_MINSTD_MAX)

#endif /* #ifndef PSR_MINSTD_H */
