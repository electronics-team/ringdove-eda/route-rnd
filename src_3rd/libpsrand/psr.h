/* libsprand - simple/portable pseudorandom generators.
   Public domain.

   Project page: http://www.repo.hu/projects/libpsrand
   Project VCS: svn://svn.repo.hu/libpsrand/trunk
   Contact: http://igor2.repo.hu/contact.html
*/

#ifndef PSR_H
#define PSR_H

#include <limits.h>

#if INT_MAX == 2147483647
	typedef int psr_int32_t;
	typedef unsigned int psr_uint32_t;
#elif LONG_MAX == 2147483647
	typedef long psr_int32_t;
	typedef unsigned long psr_uint32_t;
#else
#	error Can not find 32 bit integer type
#endif

#endif
