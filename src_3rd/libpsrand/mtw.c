/* An implementation of the MT19937 Algorithm for the Mersenne Twister
   by Evan Sultanik.  Based upon the pseudocode in: M. Matsumoto and
   T. Nishimura, "Mersenne Twister: A 623-dimensionally
   equidistributed uniform pseudorandom number generator," ACM
   Transactions on Modeling and Computer Simulation Vol. 8, No. 1,
   January pp.3-30 1998.

   http://www.sultanik.com/Mersenne_twister
   Original source: github/mtwister

   Placed in the public domain by the original author, Evan Sultanik.

   Adapted to libpsrand by Tibor 'Igor2' Palinkas in 2020.

   Project page: http://www.repo.hu/projects/libpsrand
   Project VCS: svn://svn.repo.hu/libpsrand/trunk
   Contact: http://igor2.repo.hu/contact.html
*/

/*
The 'Mersenne Twister' is a pseudo-random number generation
algorithm that was developed in 1997 by Makoto Matsumoto and
Takuji Nishimura (http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html).

Although improvements on the original algorithm have since been
made (http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/SFMT/index.html),
the original algorithm is still both faster and "more random" than the
built-in generators in many common programming languages (C and Java
included).
*/


#define UPPER_MASK        0x80000000
#define LOWER_MASK        0x7fffffff
#define TEMPERING_MASK_B  0x9d2c5680
#define TEMPERING_MASK_C  0xefc60000

#include "mtw.h"

void psr_mtw_init(psr_mtw_t *rand, psr_uint32_t seed)
{
	/* set initial seeds to mt[PSR_MTW_STVECT_LENGTH] using the generator
	   from Line 25 of Table 1 in: Donald Knuth, "The Art of Computer
	   Programming," Vol. 2 (2nd Ed.) pp.102. */
	rand->mt[0] = seed & 0xffffffff;
	for(rand->index = 1; rand->index < PSR_MTW_STVECT_LENGTH; rand->index++)
		rand->mt[rand->index] = (6069 * rand->mt[rand->index - 1]) & 0xffffffff;
}

psr_uint32_t psr_mtw_rand(psr_mtw_t *rand)
{
	psr_uint32_t y;
	static psr_uint32_t mag[2] = { 0x0, 0x9908b0df }; /* mag[x] = x * 0x9908b0df for x = 0,1 */
	if ((rand->index >= PSR_MTW_STVECT_LENGTH) || (rand->index < 0)) {
		/* generate PSR_MTW_STVECT_LENGTH words at a time */
		int kk;
		if ((rand->index >= PSR_MTW_STVECT_LENGTH + 1) || (rand->index < 0))
			psr_mtw_init(rand, 4357);
		for(kk = 0; kk < PSR_MTW_STVECT_LENGTH - PSR_MTW_STVECT_M; kk++) {
			y = (rand->mt[kk] & UPPER_MASK) | (rand->mt[kk + 1] & LOWER_MASK);
			rand->mt[kk] = rand->mt[kk + PSR_MTW_STVECT_M] ^ (y >> 1) ^ mag[y & 0x1];
		}
		for(; kk < PSR_MTW_STVECT_LENGTH - 1; kk++) {
			y = (rand->mt[kk] & UPPER_MASK) | (rand->mt[kk + 1] & LOWER_MASK);
			rand->mt[kk] = rand->mt[kk + (PSR_MTW_STVECT_M - PSR_MTW_STVECT_LENGTH)] ^ (y >> 1) ^ mag[y & 0x1];
		}
		y = (rand->mt[PSR_MTW_STVECT_LENGTH - 1] & UPPER_MASK) | (rand->mt[0] & LOWER_MASK);
		rand->mt[PSR_MTW_STVECT_LENGTH - 1] = rand->mt[PSR_MTW_STVECT_M - 1] ^ (y >> 1) ^ mag[y & 0x1];
		rand->index = 0;
	}
	y = rand->mt[rand->index++];
	y ^= (y >> 11);
	y ^= (y << 7) & TEMPERING_MASK_B;
	y ^= (y << 15) & TEMPERING_MASK_C;
	y ^= (y >> 18);
	return y;
}
