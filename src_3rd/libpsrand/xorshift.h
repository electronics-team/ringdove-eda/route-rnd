/* libsprand - simple/portable pseudorandom generators.
   Public domain.

   Project page: http://www.repo.hu/projects/libpsrand
   Project VCS: svn://svn.repo.hu/libpsrand/trunk
   Contact: http://igor2.repo.hu/contact.html
*/

#ifndef PSR_XORSHIFT_H
#define PSR_XORSHIFT_H

#include "libpsrand/psr.h"

#define PSR_XORSHIFT_MAX (0xffffffffUL)

typedef struct psr_xorshift_s {
	psr_uint32_t x, y, z, w;
} psr_xorshift_t;

void psr_xorshift_init(psr_xorshift_t *state, psr_uint32_t seed);
psr_uint32_t psr_xorshift_rand(psr_xorshift_t *state);


#define psr_xorshift_rand01(state) \
	((double)psr_xorshift_rand(state) / (double)PSR_XORSHIFT_MAX)

#endif /* #ifndef PSR_XORSHIFT_H */
