/*

Buddy allocator

Copyright (c) 2021 Aron Barath
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the Author nor the names of contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

Source code: svn://svn.repo.hu/libualloc/trunk
Contact the author: aron-dev@mailbox.org

*/

#include "buddy_api.h"
#include <string.h>
#include <limits.h>

#include <stddef.h>

/* minimal allocation size in "uall" mode buddy allocator */
#define UALL_BUDDY_MIN_SIZE    0

#define ADDR_TO_OFFS(_addr_)   ((((_addr_)) - buddy->memory) >> buddy->min_bits)
#define OFFS_TO_ADDR(_offs_)   (buddy->memory + ((_offs_) << buddy->min_bits))

/* set to non-zero to check the bounds of the block in uall_buddy_free_() */
/* (by default this is set to 1) */
#define CHECK_BOUNDS_IN_FREE 1

/* set to non-zero to uall_buddy_free_() check whether the block is actually */
/* an allocated memory block or not */
/* (by default this is set to 1) */
#define CHECK_RESERVED_BIT_IN_FREE 1

/****************************************************************************/
/* list handling stuff */

static UALL_INLINE void uall_buddy_list_init(uall_buddy_byte_t* const base, uall_bchunk_t* const list)
{
	list->next = list->prev = uall_buddy_gp2la(base, list);
}

static UALL_INLINE int uall_buddy_list_is_empty(uall_buddy_byte_t* const base, uall_bchunk_t* const list)
{
	return (uall_buddy_gp2la(base, list)==list->next);
}

static UALL_INLINE uall_bchunk_t* uall_buddy_list_next(uall_buddy_byte_t* const base, uall_bchunk_t* const list)
{
	return (uall_bchunk_t*)uall_buddy_la2gp(base, list->next);
}

static UALL_INLINE uall_bchunk_t* uall_buddy_list_prev(uall_buddy_byte_t* const base, uall_bchunk_t* const list)
{
	return (uall_bchunk_t*)uall_buddy_la2gp(base, list->prev);
}

static UALL_INLINE uall_bchunk_t* uall_buddy_list_first(uall_buddy_byte_t* const base, uall_bchunk_t* const list)
{
	return uall_buddy_list_next(base, list);
}

static UALL_INLINE void uall_buddy_list_remove(uall_buddy_byte_t* const base, uall_bchunk_t* const node)
{
	uall_buddy_list_prev(base, node)->next = node->next;
	uall_buddy_list_next(base, node)->prev = node->prev;
}

static UALL_INLINE void uall_buddy_list_insert_head(uall_buddy_byte_t* const base, uall_bchunk_t* const list, uall_bchunk_t* const node)
{
	const uall_buddy_addr_t node_la = uall_buddy_gp2la(base, node);

	node->prev = uall_buddy_gp2la(base, list);
	node->next = list->next;
	uall_buddy_list_next(base, list)->prev = node_la;
	list->next = node_la;
}

static UALL_INLINE void uall_buddy_list_insert_tail(uall_buddy_byte_t* const base, uall_bchunk_t* const list, uall_bchunk_t* const node)
{
	const uall_buddy_addr_t node_la = uall_buddy_gp2la(base, node);

	node->next = uall_buddy_gp2la(base, list);
	node->prev = list->prev;
	uall_buddy_list_prev(base, list)->next = node_la;
	list->prev = node_la;
}

/****************************************************************************/
/* auxiliary functions */

#define BUDDY_ADDR_BITS         (CHAR_BIT * sizeof(uall_buddy_addr_t))

static UALL_INLINE uall_buddy_size_t uall_buddy_clz(uall_buddy_addr_t x)
{
	uall_buddy_size_t n = 1;

	if(0u==x) return BUDDY_ADDR_BITS;

	/* you can #ifdef this if you like, but I trust in the compiler */

	if(64==BUDDY_ADDR_BITS)
	{
		if(0u==(x >> 32)) { n += 32; x <<= 32; }
		if(0u==(x >> 48)) { n += 16; x <<= 16; }
		if(0u==(x >> 56)) { n +=  8; x <<=  8; }
		if(0u==(x >> 60)) { n +=  4; x <<=  4; }
		if(0u==(x >> 62)) { n +=  2; x <<=  2; }
	}
	else
	if(32==BUDDY_ADDR_BITS)
	{
		if(0u==(x >> 16)) { n += 16; x <<= 16; }
		if(0u==(x >> 24)) { n +=  8; x <<=  8; }
		if(0u==(x >> 28)) { n +=  4; x <<=  4; }
		if(0u==(x >> 30)) { n +=  2; x <<=  2; }
	}
	else
	if(16==BUDDY_ADDR_BITS)
	{
		if(0u==(x >>  8)) { n += 8; x <<= 8; }
		if(0u==(x >> 12)) { n += 4; x <<= 4; }
		if(0u==(x >> 14)) { n += 2; x <<= 2; }
	}
	else
	{
		if(0u==(x >> 4)) { n += 4; x <<= 4; }
		if(0u==(x >> 6)) { n += 2; x <<= 2; }
	}

	n -= (x>>(BUDDY_ADDR_BITS-1));

	return n;
}

static UALL_INLINE uall_buddy_size_t uall_buddy_ctz(uall_buddy_addr_t x)
{
	uall_buddy_size_t n = 1;

	if(0u==x) return BUDDY_ADDR_BITS;

	/* you can #ifdef this if you like, but I trust in the compiler */

	if(64<=BUDDY_ADDR_BITS)
	{
		if(0u==(x & 0xFFFFFFFFul)) { n += 32; x >>= 32;}
	}

	if(32<=BUDDY_ADDR_BITS)
	{
		if(0u==(x & 0x0000FFFFul)) { n += 16; x >>= 16;}
	}

	if(16<=BUDDY_ADDR_BITS)
	{
		if(0u==(x & 0x000000FFul)) { n += 8; x >>= 8;}
	}

	if(0u==(x & 0x0000000Ful)) { n += 4; x >>= 4;}
	if(0u==(x & 0x00000003ul)) { n += 2; x >>= 2;}

	return n - (x & 1ul);
}

/* compute the integer log2 of a number */
/* 1024 -> 10 */
/* 1020 -> 9 */
static UALL_INLINE uall_buddy_size_t uall_buddy_ilog2(uall_buddy_size_t n)
{
	if(0!=(n & (n-1))) { --n; }
	return BUDDY_ADDR_BITS - 1 - uall_buddy_clz(n);
}

/* compute the bounding integer log2 of a number */
/* 1025 -> 11 */
/* 1024 -> 10 */
/* 1023 -> 10 */
static UALL_INLINE uall_buddy_size_t uall_buddy_ilog2_bound(uall_buddy_size_t n)
{
	if(0==(n & (n-1))) { --n; }
	return 1 + (BUDDY_ADDR_BITS - 1 - uall_buddy_clz(n));
}

/****************************************************************************/
/* core buddy implementation */

/* size of the core buddy allocator state */
/* note that 'chunk_lists' has one element, so we need to subtract it */
#define sizeof_struct_buddy (sizeof(uall_buddy1_t) - sizeof(uall_bchunk_t))

uall_buddy1_t* uall_buddy_init_(uall_buddy1_t* const buddy, uall_buddy_size_t size, uall_buddy_size_t minsize)
{
	uall_buddy_size_t  min_log2;
	uall_buddy_size_t  max_log2;
	uall_buddy_size_t  num_lists;
	uall_buddy_size_t  remaining;
	uall_buddy_size_t  num_min;
	uall_buddy_byte_t* info;

	/* more sanity check */
	if(NULL==buddy || size<=sizeof_struct_buddy)
	{
		return NULL;
	}

	/* apply minsize hard-limit */
	if(sizeof(uall_bchunk_t)>minsize)
	{
		minsize = sizeof(uall_bchunk_t);
	}

	/* minsize must be power of two */
	if(0!=(minsize & (minsize-1)))
	{
		return NULL;
	}

	/* do some magic to determine the state size */
	/* first, count the number of lists to be used */
	/* (the potentional loss here is about 1 list head) */
	min_log2  = uall_buddy_ilog2(minsize);
	max_log2  = uall_buddy_ilog2(size - sizeof_struct_buddy);
	num_lists = 1 + (max_log2 - min_log2);
	/* second, split the remaining space into 'minsize' chunks */
	/* and 'minsize' info (1 byte) */
	remaining = size - sizeof_struct_buddy - num_lists*sizeof(uall_bchunk_t);
	num_min   = remaining / minsize;
	{
		/* search for the optimal num_min value */
		size_t tmp = (remaining - num_min) / minsize;
		while(num_min!=tmp && (num_min+1)!=tmp)
		{
			num_min = tmp;
			tmp     = (remaining - num_min) / minsize;
		}
	}
	/* finally, recompute the total size */
	/* (the lost space here will turn to padding later) */
	remaining = num_min * minsize;

	/* compute pointers */
	info = ((uall_buddy_byte_t*)buddy) + sizeof_struct_buddy + num_lists*sizeof(uall_bchunk_t);

	buddy->next          = NULL;
	buddy->memory        = (uall_buddy_gp2la(buddy, info) + ((num_min + minsize - 1))) & (~(minsize - 1));
	buddy->total_size    = remaining;
	buddy->act_max_size  = 0;
	buddy->min_size      = ((uall_buddy_size_t)1) << min_log2;
	buddy->min_bits      = min_log2;
	buddy->num_levels    = num_lists;
	buddy->num_minchunks = num_min;
	buddy->chunks_info   = uall_buddy_gp2la(buddy, info);

	memset(info, 0, num_min);

	{
		uall_buddy_size_t idx;

		for(idx=0;idx<num_lists;++idx)
		{
			uall_buddy_list_init((uall_buddy_byte_t*)buddy, buddy->chunk_lists + idx);
		}
	}

	{
		uall_bchunk_t* const lists = buddy->chunk_lists;
		uall_buddy_addr_t         addr  = buddy->memory;

		while(0<remaining)
		{
			uall_buddy_size_t level = uall_buddy_ctz(addr) - min_log2; /* the level this chunk goes to */
			uall_buddy_size_t csize = ((uall_buddy_size_t)1) << (level + min_log2); /* chunk size */

			if(csize>remaining)
			{
				level = BUDDY_ADDR_BITS - 1 - uall_buddy_clz(remaining) - min_log2;
				csize = ((uall_buddy_size_t)1) << (level + min_log2);
			}

			*info = (uall_buddy_byte_t)level;

			uall_buddy_list_insert_tail((uall_buddy_byte_t*)buddy, lists + level,
				(uall_bchunk_t*)uall_buddy_la2gp(buddy, addr));

			addr      += csize;
			info      += ((uall_buddy_size_t)1) << level;
			remaining -= csize;

			if(csize>buddy->act_max_size)
			{
				buddy->act_max_size = csize;
				buddy->max_bits = level + min_log2;
			}
		}
	}

	return buddy;
}

static UALL_INLINE void uall_recompute_act_max_size(uall_buddy1_t* const buddy)
{
	uall_bchunk_t* const chunk_lists = buddy->chunk_lists;
	uall_bchunk_t*       chunk_list  = chunk_lists + buddy->num_levels - 1;

	while(chunk_lists<=chunk_list)
	{
		if(!uall_buddy_list_is_empty((uall_buddy_byte_t*)buddy, chunk_list))
		{
			buddy->act_max_size =
				buddy->min_size << (chunk_list - chunk_lists);
			return;
		}

		--chunk_list;
	}

	buddy->act_max_size = 0;
}

UALL_INLINE uall_buddy_addr_t uall_buddy_alloc_(uall_buddy1_t* const buddy, uall_buddy_size_t size)
{
	uall_buddy_addr_t   addr;  /* logical address of the allocated block */
	uall_buddy_size_t   level; /* temporary level, we do calculations in it */
	uall_buddy_size_t   lreq;  /* level of the required size */
	uall_buddy_byte_t*  info;  /* pointer to the chunk info of the allocated block */
	uall_bchunk_t* chunk; /* address of the allocated memory block */

	if(size<buddy->min_size)
	{
		size = buddy->min_size;
	}

	if(size>buddy->act_max_size)
	{
		return 0;
	}

	level = uall_buddy_ilog2_bound(size) - buddy->min_bits;
	lreq  = level;

	/* find the smallest block that is large enough */

	while(level<buddy->num_levels &&
		uall_buddy_list_is_empty((uall_buddy_byte_t*)buddy, buddy->chunk_lists+level))
	{
		++level;
	}

	/* did we ran out of memory? */

	if(level>=buddy->num_levels ||
		uall_buddy_list_is_empty((uall_buddy_byte_t*)buddy, buddy->chunk_lists+level))
	{
		return 0;
	}

	/* we have a block, unlink it */

	chunk = uall_buddy_list_first((uall_buddy_byte_t*)buddy, buddy->chunk_lists+level);
	addr = uall_buddy_gp2la(buddy, chunk);
	info = (uall_buddy_byte_t*)uall_buddy_la2gp(buddy, buddy->chunks_info) +
		ADDR_TO_OFFS(addr);

	uall_buddy_list_remove((uall_buddy_byte_t*)buddy, chunk);

	if(buddy->act_max_size==(buddy->min_size << level) &&
		uall_buddy_list_is_empty((uall_buddy_byte_t*)buddy, buddy->chunk_lists+level))
	{
		buddy->act_max_size = 0;
	}

	/* 'level' can be larger than 'lreq' */
	/* in this case, we need to split the found memory block */
	/* put all fragment blocks back to the empty lists */

	while(lreq<level)
	{
		/* size of the fragment from the oversized memory block */
		/* (size expressed in minimal-sized chunks) */
		const uall_buddy_size_t fragment_nchunks = ((uall_buddy_size_t)1) << (--level);

		/* locate the info of the fragment chunk */
		uall_buddy_byte_t* const fragment_info = info + fragment_nchunks;

		/* compute the address of the fragment chunk (half of the */
		/* current chunk, see 'level') */
		uall_bchunk_t* const frag_chunk = (uall_bchunk_t*)
			(((uall_buddy_byte_t*)chunk) + (fragment_nchunks << buddy->min_bits));

		/* set the chunk info */
		*fragment_info = level;

		/* put the free memory chunk to the list */
		uall_buddy_list_insert_head((uall_buddy_byte_t*)buddy, buddy->chunk_lists+level,
			frag_chunk);

		/* update act_max_size */
		{
			const uall_buddy_size_t size = buddy->min_size << level;

			if(size>buddy->act_max_size)
			{
				buddy->act_max_size = size;
			}
		}
	}

	/* set info for the whole block, thus misaligned requests */
	/* can be resolved */

	memset(info, UALL_BUDDY_RESERVED_BIT|lreq, ((uall_buddy_size_t)1) << lreq);

	/* update the act_max_size the hard way */

	if(0==buddy->act_max_size)
	{
		uall_recompute_act_max_size(buddy);
	}

	/* we're done */

	return addr;
}

UALL_INLINE void uall_buddy_free_(uall_buddy1_t* const buddy, uall_buddy_addr_t block)
{
	const uall_buddy_addr_t  addr = block;
	uall_buddy_byte_t* const INFO = (uall_buddy_byte_t*)uall_buddy_la2gp(buddy, buddy->chunks_info);
	uall_buddy_byte_t* const last = INFO + buddy->num_minchunks;

	uall_buddy_size_t  offset;   /* offset in chunks_info */
	uall_buddy_size_t  level;    /* current level (index list heads with) */
	uall_buddy_addr_t  sib_mask; /* mask to determine on which side the sibling is */
	uall_buddy_size_t  distance; /* distance of the sibling (buddy) in bytes */
	uall_buddy_byte_t* info;     /* chunk info byte of the block to be freed */

#if CHECK_BOUNDS_IN_FREE

	/* check the valid memory interval */

	if(block<buddy->memory ||
		block>=(buddy->memory + buddy->total_size))
	{
		return;
	}

#endif /* CHECK_BOUNDS_IN_FREE */

	offset = ADDR_TO_OFFS(addr);

#if CHECK_RESERVED_BIT_IN_FREE

	/* is the given address represent an allocated block? */

	if(0==(UALL_BUDDY_RESERVED_BIT & INFO[offset]))
	{
		return;
	}

#endif /* CHECK_RESERVED_BIT_IN_FREE */

	/* as far as we can tell, the given address is a reserved */
	/* block, so it is possible to free it up */

	level    = INFO[offset] & UALL_BUDDY_LEVELS_MASK;
	sib_mask = buddy->min_size << level;
	distance = ((uall_buddy_size_t)1) << level;
	info     = INFO + offset;

	for(;;)
	{
		uall_buddy_byte_t* sibling_info;

		if(0!=(addr & sib_mask))
		{
			sibling_info = info - distance;
		}
		else
		{
			sibling_info = info + distance;
		}

		/* check whether the sibling is in bounds and whether it can */
		/* be merged (has no UALL_BUDDY_RESERVED_BIT and both on the same level) */

		if(sibling_info>=INFO && /* lower bound */
			sibling_info<last && /* upper bound */
			level<buddy->num_levels && /* not on top level */
			0==(UALL_BUDDY_RESERVED_BIT & (*sibling_info)) && /* sibling is free */
			(UALL_BUDDY_LEVELS_MASK & (*sibling_info))==level) /* both on same level */
		{
			uall_bchunk_t* const sib_node =
				(uall_bchunk_t*)uall_buddy_la2gp(buddy,
				OFFS_TO_ADDR(sibling_info - INFO));

			if(0!=(addr & sib_mask))
			{
				/* sibling is on a lower address, we must use that lower */
				/* address in the next iteration */
				info = sibling_info;
			}

			/* remove the sibling from the empty list */
			uall_buddy_list_remove((uall_buddy_byte_t*)buddy, sib_node);

			/* "merge" current node with its sibling -- in practice: */
			/* set current level one higher, so distance to sibling */
			/* is doubled, and we must use the next bit in the sibling mask */
			++level;
			distance <<= 1;
			sib_mask <<= 1;
		}
		else
		{
			/* no further merge is possible */

			uall_bchunk_t* const node =
				(uall_bchunk_t*)uall_buddy_la2gp(buddy,
				OFFS_TO_ADDR(info - INFO));

			const uall_buddy_size_t size = buddy->min_size << level;

			if(size>buddy->act_max_size)
			{
				buddy->act_max_size = size;
			}

			/* link into the proper list */
			uall_buddy_list_insert_head((uall_buddy_byte_t*)buddy, buddy->chunk_lists + level,
				node);

			/* set chunk info */
			*info = (uall_buddy_byte_t)level;

			/* and leave */
			return;
		}
	}
}

UALL_INLINE uall_buddy_size_t uall_buddy_msize(uall_buddy1_t* const buddy, uall_buddy_addr_t block)
{
	uall_buddy_size_t offs;
	uall_buddy_size_t level;
	uall_buddy_byte_t info;

	/* first, check the valid memory interval */

	if(block<buddy->memory ||
		block>=(buddy->memory + buddy->total_size))
	{
		return 0;
	}

	/* is the given address represent an allocated block? */

	offs = ADDR_TO_OFFS(block);
	info = ((uall_buddy_byte_t*)uall_buddy_la2gp(buddy, buddy->chunks_info))[offs];

	if(0==(UALL_BUDDY_RESERVED_BIT & info))
	{
		return 0;
	}

	/* everything matched, return the size of the memory block */

	level = info & UALL_BUDDY_LEVELS_MASK;

	return ((uall_buddy_size_t)1) << (level + buddy->min_bits);
}

/****************************************************************************/
/* "uall" mode buddy allocator (extendable) */

UALL_INLINE void uall_buddy_init(uall_buddy_t* ctx)
{
	memset(ctx, 0, sizeof(*ctx));
}

UALL_INLINE void uall_buddy_restore_segment(uall_buddy_t* ctx, void* base)
{
	((uall_buddy1_t*)base)->next = NULL;
	++ctx->_nsegments;

	if(NULL!=ctx->_last)
	{
		ctx->_last->next = (uall_buddy1_t*)base;
		ctx->_last = (uall_buddy1_t*)base;
	}
	else
	{
		ctx->_first = ctx->_last = (uall_buddy1_t*)base;
		ctx->_maxsize = ((uall_buddy_size_t)1) << ((uall_buddy1_t*)base)->max_bits;
	}
}

static UALL_INLINE uall_buddy1_t* uall_locate_buddy_by_ptr(uall_buddy_t* ctx, void* ptr,
	buddy_segm_t* const p_segm)
{
	uall_buddy1_t* buddy = ctx->_first;
	buddy_segm_t segm = 0;

	while(NULL!=buddy)
	{
		if(((uall_buddy_byte_t*)buddy)<((uall_buddy_byte_t*)ptr) &&
			((uall_buddy_byte_t*)ptr)<(((uall_buddy_byte_t*)buddy)+ctx->sys->page_size))
		{
			*p_segm = segm;
			return buddy;
		}

		++segm;
		buddy = buddy->next;
	}

	return NULL;
}

static UALL_INLINE uall_buddy1_t* uall_locate_buddy_by_va(uall_buddy_t* ctx, const uall_buddy_va_t* const va)
{
	uall_buddy1_t* buddy = ctx->_first;
	buddy_segm_t segm = va->segm;

	if(segm>ctx->_nsegments)
	{
		return NULL;
	}

	while(0<segm)
	{
		buddy = buddy->next;
		--segm;
	}

	return buddy;
}

static UALL_INLINE uall_buddy1_t* uall_buddy_alloc_impl(uall_buddy_t* ctx, size_t size, uall_buddy_va_t* const va)
{
	uall_buddy1_t* buddy = ctx->_first;

	if(0<ctx->_maxsize && size>ctx->_maxsize)
	{
		return NULL;
	}

	va->segm = 0;

	while(NULL!=buddy)
	{
		if(0!=(va->addr=uall_buddy_alloc_(buddy, size)))
		{
			return buddy;
		}

		++va->segm;

		buddy = buddy->next;
	}

	if(NULL==(buddy=(uall_buddy1_t*)ctx->sys->alloc(ctx->sys, ctx->sys->page_size)))
	{
		return NULL;
	}

	if(NULL==uall_buddy_init_(buddy, ctx->sys->page_size, UALL_BUDDY_MIN_SIZE))
	{
		/* I would put an assert here, but it is unlikely to reach this */
		return NULL;
	}

	if(NULL!=ctx->_last)
	{
		ctx->_last->next = buddy;
	}
	else
	{
		ctx->_first = buddy;
	}

	ctx->_last = buddy;
	++ctx->_nsegments;

	if(0==ctx->_maxsize)
	{
		ctx->_maxsize = ((uall_buddy_size_t)1) << buddy->max_bits;
	}

	if(0!=(va->addr=uall_buddy_alloc_(buddy, size)))
	{
		return buddy;
	}

	return NULL;
}

UALL_INLINE void* uall_buddy_alloc(uall_buddy_t* ctx, size_t size)
{
	uall_buddy_va_t va;
	uall_buddy1_t* const buddy = uall_buddy_alloc_impl(ctx, size, &va);

	if(NULL!=buddy)
	{
		return uall_buddy_la2gp(buddy, va.addr);
	}

	return NULL;
}

UALL_INLINE void uall_buddy_free(uall_buddy_t* ctx, void* ptr)
{
	buddy_segm_t segm = 0;
	uall_buddy1_t* const buddy = uall_locate_buddy_by_ptr(ctx, ptr, &segm);

	if(NULL!=buddy)
	{
		const uall_buddy_addr_t addr = uall_buddy_gp2la(buddy, ptr);
		uall_buddy_free_(buddy, addr);
	}
}

UALL_INLINE int uall_buddy_alloc_va(uall_buddy_t* ctx, size_t size, uall_buddy_va_t* const va)
{
	if(NULL!=uall_buddy_alloc_impl(ctx, size, va))
	{
		return 0;
	}

	return -1;
}

UALL_INLINE int uall_buddy_free_va(uall_buddy_t* ctx, uall_buddy_va_t* const va)
{
	uall_buddy1_t* const buddy = uall_locate_buddy_by_va(ctx, va);

	if(NULL!=buddy)
	{
		uall_buddy_free_(buddy, va->addr);
		return 0;
	}

	return -1;
}

UALL_INLINE void* uall_buddy_va2ptr(uall_buddy_t* ctx, const uall_buddy_va_t* const va)
{
	uall_buddy1_t* const buddy = uall_locate_buddy_by_va(ctx, va);

	if(NULL!=buddy)
	{
		return uall_buddy_la2gp(buddy, va->addr);
	}

	return NULL;
}

UALL_INLINE int uall_buddy_ptr2va(uall_buddy_t* ctx, uall_buddy_va_t* const va, void* ptr)
{
	buddy_segm_t segm = 0;
	uall_buddy1_t* const buddy = uall_locate_buddy_by_ptr(ctx, ptr, &segm);

	if(NULL!=buddy)
	{
		va->segm = segm;
		va->addr = uall_buddy_gp2la(buddy, ptr);
		return 0;
	}

	return -1;
}
