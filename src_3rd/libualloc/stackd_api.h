#ifndef LIBUALLOC_STACKD_API_H
#define LIBUALLOC_STACKD_API_H

#include <stddef.h>
#include <libualloc/libualloc.h>

/*
	Allocator: stackd - stack allocation with dynamic sized data

	Allocate arbitrary sized elements in order, packing them in a linked list
	of pages. In other words, allocations are pushed on a stack. Free is
	possible only in reverse order of allocation (always popping and
	discarding the topmost element of the stack).

	Allocation size:      arbitrary
	Standard calls:       alloc, free*, clean
	Per allocation cost:  1 size_t + alignment
	Per page cost:        1 pointers + 1 size_t + arbitrary fitting losses
*/

typedef struct uall_stackd_page_s uall_stackd_page_t;

typedef struct {
	/* configuration */
	uall_sysalloc_t *sys;

	void *user_data;

	/* internal states - init all bytes to 0 */
	uall_stackd_page_t *pages;
} uall_stackd_t;


/* Push data: allocate a block of size on top of the stack and return
   its pointer */
UALL_INLINE void *uall_stackd_alloc(uall_stackd_t *ctx, size_t size);

/* Pop data: remove and free the top item; returns 1 if an item could be
   removed, 0 if the stack was already empty */
UALL_INLINE int uall_stackd_free(uall_stackd_t *ctx);

/* Return pointer to the top element of the stack */
UALL_INLINE void *uall_stackd_top(uall_stackd_t *ctx);

/* Free all data and empty ctx, which will be ready to accept new allocation;
   cheaper than calling uall_stackd_free() multiple times */
UALL_INLINE void uall_stackd_clean(uall_stackd_t *ctx);

#endif
