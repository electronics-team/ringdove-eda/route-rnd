/*

libualloc - microlib for memory allocation with various strategies

Copyright (c) 2020 Tibor 'Igor2' Palinkas
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the Author nor the names of contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

Source code: svn://svn.repo.hu/libualloc/trunk
Contact the author: http://igor2.repo.hu/contact.html

*/

#include <stdlib.h>
#include <stddef.h>
#include <assert.h>
#include <libualloc/libualloc.h>

struct uall_stacks_page_s {
	size_t alloced, used;
	uall_stacks_page_t *next;
	char data[1];
};

#define UALL_STACKS_PAGE_T_SIZE         offsetof(uall_stacks_page_t, data)

UALL_INLINE void *uall_stacks_alloc(uall_stacks_t *ctx)
{
	uall_stacks_page_t *page = ctx->pages;
	size_t avail = (page == NULL) ? 0 : ctx->pages->alloced - ctx->pages->used;
	void *elem;

	if (ctx->pages == NULL) {
		if (ctx->elem_size == 0)
			return NULL;
		ctx->esize = UALL_ALIGN(ctx->elem_size);
		if (ctx->esize > (ctx->sys->page_size - UALL_STACKS_PAGE_T_SIZE))
			return NULL;
	}

	/* if not enough room in the current page, go on allocating the next page */
	if (avail < ctx->esize) {
		page = ctx->sys->alloc(ctx->sys, ctx->sys->page_size);
		if (page == NULL)
			return NULL;
		page->used = 0;
		page->alloced = ctx->sys->page_size - UALL_STACKS_PAGE_T_SIZE;
		page->next = ctx->pages;
		ctx->pages = page;
	}

	/* current page has enough room, pack our data at the end */
	elem = (void *)((char *)page->data + page->used);
	page->used += ctx->esize;
	return elem;
}

UALL_INLINE int uall_stacks_free(uall_stacks_t *ctx)
{
	uall_stacks_page_t *page = ctx->pages;

	if (page == NULL)
		return 0;

	page->used -= ctx->esize;

	/* check if the current page got empty and get rid of it */
	if (page->used == 0) {
		ctx->pages = page->next;
		ctx->sys->free(ctx->sys, page);
	}
	return 1;
}

void *uall_stacks_top(uall_stacks_t *ctx)
{
	uall_stacks_page_t *page = ctx->pages;

	if (page == NULL)
		return NULL;

	return ((char *)page->data + page->used - ctx->esize);
}

UALL_INLINE void uall_stacks_clean(uall_stacks_t *ctx)
{
	uall_stacks_page_t *page, *next;
	for(page = ctx->pages; page != NULL; page = next) {
		next = page->next;
		ctx->sys->free(ctx->sys, page);
	}
	ctx->pages = NULL;
}
