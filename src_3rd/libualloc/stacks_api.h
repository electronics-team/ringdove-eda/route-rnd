#ifndef LIBUALLOC_STACKS_API_H
#define LIBUALLOC_STACKS_API_H

#include <stddef.h>
#include <libualloc/libualloc.h>

/*
	Allocator: stacks - stack allocation with fixed sized data

	Allocate fixed sized elements in order, packing them in a linked list
	of pages. In other words, allocations are pushed on a stack. Free is
	possible only in reverse order of allocation (always popping and
	discarding the topmost element of the stack).

	Allocation size:      fixed
	Standard calls:       alloc, free*, clean
	Per allocation cost:  alignment
	Per page cost:        1 pointers + 2 size_t + at most 1 elem_size for fitting
*/

typedef struct uall_stacks_page_s uall_stacks_page_t;

typedef struct {
	/* configuration */
	uall_sysalloc_t *sys;
	size_t elem_size;

	void *user_data;

	/* internal states - init all bytes to 0 */
	size_t esize;                 /* elem_size rounded up */
	uall_stacks_page_t *pages;
} uall_stacks_t;


/* Push data: allocate an element on top of the stack and return
   its pointer */
UALL_INLINE void *uall_stacks_alloc(uall_stacks_t *ctx);

/* Pop data: remove and free the top element; returns 1 if an item could be
   removed, 0 if the stack was already empty */
UALL_INLINE int uall_stacks_free(uall_stacks_t *ctx);

/* Return pointer to the top element of the stack */
UALL_INLINE void *uall_stacks_top(uall_stacks_t *ctx);

/* Free all data and empty ctx, which will be ready to accept new allocations;
   cheaper than calling uall_stacks_free() multiple times */
UALL_INLINE void uall_stacks_clean(uall_stacks_t *ctx);

#endif
