/*

Buddy allocator - debug helpers

Copyright (c) 2021 Aron Barath
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the Author nor the names of contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

Source code: svn://svn.repo.hu/libualloc/trunk
Contact the author: aron-dev@mailbox.org

*/

#include <stdio.h>
#include "buddy_debug.h"

void uall_buddy_visualize(uall_buddy1_t* const buddy, FILE* const file)
{
	uall_buddy_byte_t*       info = (uall_buddy_byte_t*)uall_buddy_la2gp(buddy, buddy->chunks_info);
	uall_buddy_byte_t* const end  = info + buddy->num_minchunks;
	int                      mod  = 0;

	while(info<end)
	{
		const char disp = (((*info) & UALL_BUDDY_RESERVED_BIT) ? '#' : ',') + mod;
		size_t     size = ((size_t)1) << ((*info) & UALL_BUDDY_LEVELS_MASK);

		while(0<size)
		{
			fprintf(file, "%c", disp);
			--size;
			++info;
		}

		mod ^= 1;
	}

	fprintf(file, "\n");
}

