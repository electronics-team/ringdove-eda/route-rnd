#ifndef LIBUALLOC_ACACHE_API_H
#define LIBUALLOC_ACACHE_API_H

#include <libualloc/libualloc.h>

/*
	Allocator: acache - simple allocation cache

	Remember all allocations free'd in a doubly linked list (cache). Serve new
	allocations from the cache. Same as mcache, but is cheaper and has no clean().

	Allocation size:      fixed
	Standard calls:       alloc, free
	Per allocation cost:  1 pointer
	Per page cost:        n/a (not paged)
*/

typedef struct uall_acache_elem_s uall_acache_elem_t;
typedef struct uall_acache_page_s uall_acache_page_t;

typedef struct {
	/* configuration */
	uall_sysalloc_t *sys;
	long elem_size;

	void *user_data;

	/* internal states - init all bytes to 0 */
	uall_acache_elem_t *free_elems; /* singly linked list cache */
	uall_acache_elem_t *used_elems; /* singly linked list cache */
} uall_acache_t;


/* Return a new allocation of ctx->elem_size */
UALL_INLINE void *uall_acache_alloc(uall_acache_t *ctx);

/* Free a previous allocation (really just cache it for later reuse) */
UALL_INLINE void uall_acache_free(uall_acache_t *ctx, void *ptr);

/* Free all unused (cached free) elements (but keep elements that are in use) */
UALL_INLINE void uall_acache_flush(uall_acache_t *ctx);

#endif
