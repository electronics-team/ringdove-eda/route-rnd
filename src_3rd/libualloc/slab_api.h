#ifndef LIBUALLOC_SLAB_API_H
#define LIBUALLOC_SLAB_API_H

#include <libualloc/libualloc.h>

/*
	Allocator: slab - fixed size slabs over unaligned pages

	Serve fixed size slabs from a list of pages allocated in batch.
	Similar to slabap, but is a bit more expensive while not assuming
	2^n aligned page allocation.

	Allocation size:      fixed
	Standard calls:       alloc, free, clean
	Per allocation cost:  2 pointers + alignment
	Per page cost:        2 pointers + 1 long + at most one slab_size
*/

typedef struct uall_slab_s uall_slab_t;
typedef struct uall_slab_page_s uall_slab_page_t;

typedef struct {
	/* configuration */
	uall_sysalloc_t *sys;
	long slab_size;

	void *user_data;

	/* internal states - init all bytes to 0 */
	uall_slab_page_t *pages; /* singly linked list of slab pages */
	uall_slab_t *free_slabs; /* singly linked list cache */
} uall_slabs_t;


/* Return a new allocation of ctx->slab_size */
UALL_INLINE void *uall_slab_alloc(uall_slabs_t *ctx);

/* Free a previously allocated slab (may trigger a page free) */
UALL_INLINE void uall_slab_free(uall_slabs_t *ctx, void *ptr);

/* Free all data and empty ctx, which will be ready to accept new allocations
   again; cheaper than calling uall_slab_free() multiple times */
UALL_INLINE void uall_slab_clean(uall_slabs_t *ctx);

#endif
